const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const PnpWebpackPlugin = require('pnp-webpack-plugin');
const webpack = require('webpack'); 
const path = require('path');

module.exports = {
  mode: "development",
  devtool: "cheap-module-source-map",
  entry: {
    main: "./src/public/js/main.js",
    form: "./src/public/js/form/index.js",
    bankhome: "./src/public/js/bank-home/index.js",
    partners: "./src/public/js/partners/index.js",
    thankyou: "./src/public/js/thankyou/index.js",
    "response-canvas": "./src/public/js/response-canvas/index.js"
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist/public/js')
  },
  resolve: {
    modules: ['node_modules', path.resolve(__dirname, 'src/public')],
    plugins: [
      // Adds support for installing with Plug'n'Play, leading to faster installs and adding
      // guards against forgotten dependencies and such.
      PnpWebpackPlugin,
    ],
  },
  resolveLoader: {
    plugins: [
      // Also related to Plug'n'Play, but this time it tells Webpack to load its loaders
      // from the current package.
      PnpWebpackPlugin.moduleLoader(module),
    ],
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.hbs$/,
        use: {
          loader: "handlebars-loader",
          query: {
            helperDirs: [
              __dirname + "/src/public/js/templates/helpers"
            ]
          }
        }
      },
      {
        test: /\.(png|jp(e*)g|svg)$/,  
        use: [{
            loader: 'url-loader',
            options: { 
                limit: 20, // Convert images < 20b to base64 strings
                name: '../img/[hash]-[name].[ext]'
            } 
        }]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: '../css/[name].css',
    })
  ],
}