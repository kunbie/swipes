/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/public/js/response-canvas/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/handlebars/dist/cjs/handlebars.runtime.js":
/*!****************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars.runtime.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
} // istanbul ignore next


function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  } else {
    var newObj = {};

    if (obj != null) {
      for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
      }
    }

    newObj['default'] = obj;
    return newObj;
  }
}

var _handlebarsBase = __webpack_require__(/*! ./handlebars/base */ "./node_modules/handlebars/dist/cjs/handlebars/base.js");

var base = _interopRequireWildcard(_handlebarsBase); // Each of these augment the Handlebars object. No need to setup here.
// (This is done to easily share code between commonjs and browse envs)


var _handlebarsSafeString = __webpack_require__(/*! ./handlebars/safe-string */ "./node_modules/handlebars/dist/cjs/handlebars/safe-string.js");

var _handlebarsSafeString2 = _interopRequireDefault(_handlebarsSafeString);

var _handlebarsException = __webpack_require__(/*! ./handlebars/exception */ "./node_modules/handlebars/dist/cjs/handlebars/exception.js");

var _handlebarsException2 = _interopRequireDefault(_handlebarsException);

var _handlebarsUtils = __webpack_require__(/*! ./handlebars/utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

var Utils = _interopRequireWildcard(_handlebarsUtils);

var _handlebarsRuntime = __webpack_require__(/*! ./handlebars/runtime */ "./node_modules/handlebars/dist/cjs/handlebars/runtime.js");

var runtime = _interopRequireWildcard(_handlebarsRuntime);

var _handlebarsNoConflict = __webpack_require__(/*! ./handlebars/no-conflict */ "./node_modules/handlebars/dist/cjs/handlebars/no-conflict.js");

var _handlebarsNoConflict2 = _interopRequireDefault(_handlebarsNoConflict); // For compatibility and usage outside of module systems, make the Handlebars object a namespace


function create() {
  var hb = new base.HandlebarsEnvironment();
  Utils.extend(hb, base);
  hb.SafeString = _handlebarsSafeString2['default'];
  hb.Exception = _handlebarsException2['default'];
  hb.Utils = Utils;
  hb.escapeExpression = Utils.escapeExpression;
  hb.VM = runtime;

  hb.template = function (spec) {
    return runtime.template(spec, hb);
  };

  return hb;
}

var inst = create();
inst.create = create;

_handlebarsNoConflict2['default'](inst);

inst['default'] = inst;
exports['default'] = inst;
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/base.js":
/*!*************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/base.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.HandlebarsEnvironment = HandlebarsEnvironment; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
}

var _utils = __webpack_require__(/*! ./utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

var _exception = __webpack_require__(/*! ./exception */ "./node_modules/handlebars/dist/cjs/handlebars/exception.js");

var _exception2 = _interopRequireDefault(_exception);

var _helpers = __webpack_require__(/*! ./helpers */ "./node_modules/handlebars/dist/cjs/handlebars/helpers.js");

var _decorators = __webpack_require__(/*! ./decorators */ "./node_modules/handlebars/dist/cjs/handlebars/decorators.js");

var _logger = __webpack_require__(/*! ./logger */ "./node_modules/handlebars/dist/cjs/handlebars/logger.js");

var _logger2 = _interopRequireDefault(_logger);

var VERSION = '4.1.2';
exports.VERSION = VERSION;
var COMPILER_REVISION = 7;
exports.COMPILER_REVISION = COMPILER_REVISION;
var REVISION_CHANGES = {
  1: '<= 1.0.rc.2',
  // 1.0.rc.2 is actually rev2 but doesn't report it
  2: '== 1.0.0-rc.3',
  3: '== 1.0.0-rc.4',
  4: '== 1.x.x',
  5: '== 2.0.0-alpha.x',
  6: '>= 2.0.0-beta.1',
  7: '>= 4.0.0'
};
exports.REVISION_CHANGES = REVISION_CHANGES;
var objectType = '[object Object]';

function HandlebarsEnvironment(helpers, partials, decorators) {
  this.helpers = helpers || {};
  this.partials = partials || {};
  this.decorators = decorators || {};

  _helpers.registerDefaultHelpers(this);

  _decorators.registerDefaultDecorators(this);
}

HandlebarsEnvironment.prototype = {
  constructor: HandlebarsEnvironment,
  logger: _logger2['default'],
  log: _logger2['default'].log,
  registerHelper: function registerHelper(name, fn) {
    if (_utils.toString.call(name) === objectType) {
      if (fn) {
        throw new _exception2['default']('Arg not supported with multiple helpers');
      }

      _utils.extend(this.helpers, name);
    } else {
      this.helpers[name] = fn;
    }
  },
  unregisterHelper: function unregisterHelper(name) {
    delete this.helpers[name];
  },
  registerPartial: function registerPartial(name, partial) {
    if (_utils.toString.call(name) === objectType) {
      _utils.extend(this.partials, name);
    } else {
      if (typeof partial === 'undefined') {
        throw new _exception2['default']('Attempting to register a partial called "' + name + '" as undefined');
      }

      this.partials[name] = partial;
    }
  },
  unregisterPartial: function unregisterPartial(name) {
    delete this.partials[name];
  },
  registerDecorator: function registerDecorator(name, fn) {
    if (_utils.toString.call(name) === objectType) {
      if (fn) {
        throw new _exception2['default']('Arg not supported with multiple decorators');
      }

      _utils.extend(this.decorators, name);
    } else {
      this.decorators[name] = fn;
    }
  },
  unregisterDecorator: function unregisterDecorator(name) {
    delete this.decorators[name];
  }
};
var log = _logger2['default'].log;
exports.log = log;
exports.createFrame = _utils.createFrame;
exports.logger = _logger2['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/decorators.js":
/*!*******************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/decorators.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.registerDefaultDecorators = registerDefaultDecorators; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
}

var _decoratorsInline = __webpack_require__(/*! ./decorators/inline */ "./node_modules/handlebars/dist/cjs/handlebars/decorators/inline.js");

var _decoratorsInline2 = _interopRequireDefault(_decoratorsInline);

function registerDefaultDecorators(instance) {
  _decoratorsInline2['default'](instance);
}

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/decorators/inline.js":
/*!**************************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/decorators/inline.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _utils = __webpack_require__(/*! ../utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

exports['default'] = function (instance) {
  instance.registerDecorator('inline', function (fn, props, container, options) {
    var ret = fn;

    if (!props.partials) {
      props.partials = {};

      ret = function (context, options) {
        // Create a new partials stack frame prior to exec.
        var original = container.partials;
        container.partials = _utils.extend({}, original, props.partials);
        var ret = fn(context, options);
        container.partials = original;
        return ret;
      };
    }

    props.partials[options.args[0]] = options.fn;
    return ret;
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/exception.js":
/*!******************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/exception.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var errorProps = ['description', 'fileName', 'lineNumber', 'message', 'name', 'number', 'stack'];

function Exception(message, node) {
  var loc = node && node.loc,
      line = undefined,
      column = undefined;

  if (loc) {
    line = loc.start.line;
    column = loc.start.column;
    message += ' - ' + line + ':' + column;
  }

  var tmp = Error.prototype.constructor.call(this, message); // Unfortunately errors are not enumerable in Chrome (at least), so `for prop in tmp` doesn't work.

  for (var idx = 0; idx < errorProps.length; idx++) {
    this[errorProps[idx]] = tmp[errorProps[idx]];
  }
  /* istanbul ignore else */


  if (Error.captureStackTrace) {
    Error.captureStackTrace(this, Exception);
  }

  try {
    if (loc) {
      this.lineNumber = line; // Work around issue under safari where we can't directly set the column value

      /* istanbul ignore next */

      if (Object.defineProperty) {
        Object.defineProperty(this, 'column', {
          value: column,
          enumerable: true
        });
      } else {
        this.column = column;
      }
    }
  } catch (nop) {
    /* Ignore if the browser is very particular */
  }
}

Exception.prototype = new Error();
exports['default'] = Exception;
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers.js":
/*!****************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.registerDefaultHelpers = registerDefaultHelpers; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
}

var _helpersBlockHelperMissing = __webpack_require__(/*! ./helpers/block-helper-missing */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/block-helper-missing.js");

var _helpersBlockHelperMissing2 = _interopRequireDefault(_helpersBlockHelperMissing);

var _helpersEach = __webpack_require__(/*! ./helpers/each */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/each.js");

var _helpersEach2 = _interopRequireDefault(_helpersEach);

var _helpersHelperMissing = __webpack_require__(/*! ./helpers/helper-missing */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/helper-missing.js");

var _helpersHelperMissing2 = _interopRequireDefault(_helpersHelperMissing);

var _helpersIf = __webpack_require__(/*! ./helpers/if */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/if.js");

var _helpersIf2 = _interopRequireDefault(_helpersIf);

var _helpersLog = __webpack_require__(/*! ./helpers/log */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/log.js");

var _helpersLog2 = _interopRequireDefault(_helpersLog);

var _helpersLookup = __webpack_require__(/*! ./helpers/lookup */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/lookup.js");

var _helpersLookup2 = _interopRequireDefault(_helpersLookup);

var _helpersWith = __webpack_require__(/*! ./helpers/with */ "./node_modules/handlebars/dist/cjs/handlebars/helpers/with.js");

var _helpersWith2 = _interopRequireDefault(_helpersWith);

function registerDefaultHelpers(instance) {
  _helpersBlockHelperMissing2['default'](instance);

  _helpersEach2['default'](instance);

  _helpersHelperMissing2['default'](instance);

  _helpersIf2['default'](instance);

  _helpersLog2['default'](instance);

  _helpersLookup2['default'](instance);

  _helpersWith2['default'](instance);
}

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/block-helper-missing.js":
/*!*************************************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/block-helper-missing.js ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _utils = __webpack_require__(/*! ../utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

exports['default'] = function (instance) {
  instance.registerHelper('blockHelperMissing', function (context, options) {
    var inverse = options.inverse,
        fn = options.fn;

    if (context === true) {
      return fn(this);
    } else if (context === false || context == null) {
      return inverse(this);
    } else if (_utils.isArray(context)) {
      if (context.length > 0) {
        if (options.ids) {
          options.ids = [options.name];
        }

        return instance.helpers.each(context, options);
      } else {
        return inverse(this);
      }
    } else {
      if (options.data && options.ids) {
        var data = _utils.createFrame(options.data);

        data.contextPath = _utils.appendContextPath(options.data.contextPath, options.name);
        options = {
          data: data
        };
      }

      return fn(context, options);
    }
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/each.js":
/*!*********************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/each.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
}

var _utils = __webpack_require__(/*! ../utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

var _exception = __webpack_require__(/*! ../exception */ "./node_modules/handlebars/dist/cjs/handlebars/exception.js");

var _exception2 = _interopRequireDefault(_exception);

exports['default'] = function (instance) {
  instance.registerHelper('each', function (context, options) {
    if (!options) {
      throw new _exception2['default']('Must pass iterator to #each');
    }

    var fn = options.fn,
        inverse = options.inverse,
        i = 0,
        ret = '',
        data = undefined,
        contextPath = undefined;

    if (options.data && options.ids) {
      contextPath = _utils.appendContextPath(options.data.contextPath, options.ids[0]) + '.';
    }

    if (_utils.isFunction(context)) {
      context = context.call(this);
    }

    if (options.data) {
      data = _utils.createFrame(options.data);
    }

    function execIteration(field, index, last) {
      if (data) {
        data.key = field;
        data.index = index;
        data.first = index === 0;
        data.last = !!last;

        if (contextPath) {
          data.contextPath = contextPath + field;
        }
      }

      ret = ret + fn(context[field], {
        data: data,
        blockParams: _utils.blockParams([context[field], field], [contextPath + field, null])
      });
    }

    if (context && typeof context === 'object') {
      if (_utils.isArray(context)) {
        for (var j = context.length; i < j; i++) {
          if (i in context) {
            execIteration(i, i, i === context.length - 1);
          }
        }
      } else {
        var priorKey = undefined;

        for (var key in context) {
          if (context.hasOwnProperty(key)) {
            // We're running the iterations one step out of sync so we can detect
            // the last iteration without have to scan the object twice and create
            // an itermediate keys array.
            if (priorKey !== undefined) {
              execIteration(priorKey, i - 1);
            }

            priorKey = key;
            i++;
          }
        }

        if (priorKey !== undefined) {
          execIteration(priorKey, i - 1, true);
        }
      }
    }

    if (i === 0) {
      ret = inverse(this);
    }

    return ret;
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/helper-missing.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/helper-missing.js ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
}

var _exception = __webpack_require__(/*! ../exception */ "./node_modules/handlebars/dist/cjs/handlebars/exception.js");

var _exception2 = _interopRequireDefault(_exception);

exports['default'] = function (instance) {
  instance.registerHelper('helperMissing', function ()
  /* [args, ]options */
  {
    if (arguments.length === 1) {
      // A missing field in a {{foo}} construct.
      return undefined;
    } else {
      // Someone is actually trying to call something, blow up.
      throw new _exception2['default']('Missing helper: "' + arguments[arguments.length - 1].name + '"');
    }
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/if.js":
/*!*******************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/if.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _utils = __webpack_require__(/*! ../utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

exports['default'] = function (instance) {
  instance.registerHelper('if', function (conditional, options) {
    if (_utils.isFunction(conditional)) {
      conditional = conditional.call(this);
    } // Default behavior is to render the positive path if the value is truthy and not empty.
    // The `includeZero` option may be set to treat the condtional as purely not empty based on the
    // behavior of isEmpty. Effectively this determines if 0 is handled by the positive path or negative.


    if (!options.hash.includeZero && !conditional || _utils.isEmpty(conditional)) {
      return options.inverse(this);
    } else {
      return options.fn(this);
    }
  });
  instance.registerHelper('unless', function (conditional, options) {
    return instance.helpers['if'].call(this, conditional, {
      fn: options.inverse,
      inverse: options.fn,
      hash: options.hash
    });
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/log.js":
/*!********************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/log.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports['default'] = function (instance) {
  instance.registerHelper('log', function ()
  /* message, options */
  {
    var args = [undefined],
        options = arguments[arguments.length - 1];

    for (var i = 0; i < arguments.length - 1; i++) {
      args.push(arguments[i]);
    }

    var level = 1;

    if (options.hash.level != null) {
      level = options.hash.level;
    } else if (options.data && options.data.level != null) {
      level = options.data.level;
    }

    args[0] = level;
    instance.log.apply(instance, args);
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/lookup.js":
/*!***********************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/lookup.js ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

exports['default'] = function (instance) {
  instance.registerHelper('lookup', function (obj, field) {
    if (!obj) {
      return obj;
    }

    if (field === 'constructor' && !obj.propertyIsEnumerable(field)) {
      return undefined;
    }

    return obj[field];
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/helpers/with.js":
/*!*********************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/helpers/with.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _utils = __webpack_require__(/*! ../utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

exports['default'] = function (instance) {
  instance.registerHelper('with', function (context, options) {
    if (_utils.isFunction(context)) {
      context = context.call(this);
    }

    var fn = options.fn;

    if (!_utils.isEmpty(context)) {
      var data = options.data;

      if (options.data && options.ids) {
        data = _utils.createFrame(options.data);
        data.contextPath = _utils.appendContextPath(options.data.contextPath, options.ids[0]);
      }

      return fn(context, {
        data: data,
        blockParams: _utils.blockParams([context], [data && data.contextPath])
      });
    } else {
      return options.inverse(this);
    }
  });
};

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/logger.js":
/*!***************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/logger.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _utils = __webpack_require__(/*! ./utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

var logger = {
  methodMap: ['debug', 'info', 'warn', 'error'],
  level: 'info',
  // Maps a given level value to the `methodMap` indexes above.
  lookupLevel: function lookupLevel(level) {
    if (typeof level === 'string') {
      var levelMap = _utils.indexOf(logger.methodMap, level.toLowerCase());

      if (levelMap >= 0) {
        level = levelMap;
      } else {
        level = parseInt(level, 10);
      }
    }

    return level;
  },
  // Can be overridden in the host environment
  log: function log(level) {
    level = logger.lookupLevel(level);

    if (typeof console !== 'undefined' && logger.lookupLevel(logger.level) <= level) {
      var method = logger.methodMap[level];

      if (!console[method]) {
        // eslint-disable-line no-console
        method = 'log';
      }

      for (var _len = arguments.length, message = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        message[_key - 1] = arguments[_key];
      }

      console[method].apply(console, message); // eslint-disable-line no-console
    }
  }
};
exports['default'] = logger;
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/no-conflict.js":
/*!********************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/no-conflict.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {/* global window */


exports.__esModule = true;

exports['default'] = function (Handlebars) {
  /* istanbul ignore next */
  var root = typeof global !== 'undefined' ? global : window,
      $Handlebars = root.Handlebars;
  /* istanbul ignore next */

  Handlebars.noConflict = function () {
    if (root.Handlebars === Handlebars) {
      root.Handlebars = $Handlebars;
    }

    return Handlebars;
  };
};

module.exports = exports['default'];
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/runtime.js":
/*!****************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/runtime.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.checkRevision = checkRevision;
exports.template = template;
exports.wrapProgram = wrapProgram;
exports.resolvePartial = resolvePartial;
exports.invokePartial = invokePartial;
exports.noop = noop; // istanbul ignore next

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    'default': obj
  };
} // istanbul ignore next


function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  } else {
    var newObj = {};

    if (obj != null) {
      for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
      }
    }

    newObj['default'] = obj;
    return newObj;
  }
}

var _utils = __webpack_require__(/*! ./utils */ "./node_modules/handlebars/dist/cjs/handlebars/utils.js");

var Utils = _interopRequireWildcard(_utils);

var _exception = __webpack_require__(/*! ./exception */ "./node_modules/handlebars/dist/cjs/handlebars/exception.js");

var _exception2 = _interopRequireDefault(_exception);

var _base = __webpack_require__(/*! ./base */ "./node_modules/handlebars/dist/cjs/handlebars/base.js");

function checkRevision(compilerInfo) {
  var compilerRevision = compilerInfo && compilerInfo[0] || 1,
      currentRevision = _base.COMPILER_REVISION;

  if (compilerRevision !== currentRevision) {
    if (compilerRevision < currentRevision) {
      var runtimeVersions = _base.REVISION_CHANGES[currentRevision],
          compilerVersions = _base.REVISION_CHANGES[compilerRevision];
      throw new _exception2['default']('Template was precompiled with an older version of Handlebars than the current runtime. ' + 'Please update your precompiler to a newer version (' + runtimeVersions + ') or downgrade your runtime to an older version (' + compilerVersions + ').');
    } else {
      // Use the embedded version info since the runtime doesn't know about this revision yet
      throw new _exception2['default']('Template was precompiled with a newer version of Handlebars than the current runtime. ' + 'Please update your runtime to a newer version (' + compilerInfo[1] + ').');
    }
  }
}

function template(templateSpec, env) {
  /* istanbul ignore next */
  if (!env) {
    throw new _exception2['default']('No environment passed to template');
  }

  if (!templateSpec || !templateSpec.main) {
    throw new _exception2['default']('Unknown template object: ' + typeof templateSpec);
  }

  templateSpec.main.decorator = templateSpec.main_d; // Note: Using env.VM references rather than local var references throughout this section to allow
  // for external users to override these as psuedo-supported APIs.

  env.VM.checkRevision(templateSpec.compiler);

  function invokePartialWrapper(partial, context, options) {
    if (options.hash) {
      context = Utils.extend({}, context, options.hash);

      if (options.ids) {
        options.ids[0] = true;
      }
    }

    partial = env.VM.resolvePartial.call(this, partial, context, options);
    var result = env.VM.invokePartial.call(this, partial, context, options);

    if (result == null && env.compile) {
      options.partials[options.name] = env.compile(partial, templateSpec.compilerOptions, env);
      result = options.partials[options.name](context, options);
    }

    if (result != null) {
      if (options.indent) {
        var lines = result.split('\n');

        for (var i = 0, l = lines.length; i < l; i++) {
          if (!lines[i] && i + 1 === l) {
            break;
          }

          lines[i] = options.indent + lines[i];
        }

        result = lines.join('\n');
      }

      return result;
    } else {
      throw new _exception2['default']('The partial ' + options.name + ' could not be compiled when running in runtime-only mode');
    }
  } // Just add water


  var container = {
    strict: function strict(obj, name) {
      if (!(name in obj)) {
        throw new _exception2['default']('"' + name + '" not defined in ' + obj);
      }

      return obj[name];
    },
    lookup: function lookup(depths, name) {
      var len = depths.length;

      for (var i = 0; i < len; i++) {
        if (depths[i] && depths[i][name] != null) {
          return depths[i][name];
        }
      }
    },
    lambda: function lambda(current, context) {
      return typeof current === 'function' ? current.call(context) : current;
    },
    escapeExpression: Utils.escapeExpression,
    invokePartial: invokePartialWrapper,
    fn: function fn(i) {
      var ret = templateSpec[i];
      ret.decorator = templateSpec[i + '_d'];
      return ret;
    },
    programs: [],
    program: function program(i, data, declaredBlockParams, blockParams, depths) {
      var programWrapper = this.programs[i],
          fn = this.fn(i);

      if (data || depths || blockParams || declaredBlockParams) {
        programWrapper = wrapProgram(this, i, fn, data, declaredBlockParams, blockParams, depths);
      } else if (!programWrapper) {
        programWrapper = this.programs[i] = wrapProgram(this, i, fn);
      }

      return programWrapper;
    },
    data: function data(value, depth) {
      while (value && depth--) {
        value = value._parent;
      }

      return value;
    },
    merge: function merge(param, common) {
      var obj = param || common;

      if (param && common && param !== common) {
        obj = Utils.extend({}, common, param);
      }

      return obj;
    },
    // An empty object to use as replacement for null-contexts
    nullContext: Object.seal({}),
    noop: env.VM.noop,
    compilerInfo: templateSpec.compiler
  };

  function ret(context) {
    var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
    var data = options.data;

    ret._setup(options);

    if (!options.partial && templateSpec.useData) {
      data = initData(context, data);
    }

    var depths = undefined,
        blockParams = templateSpec.useBlockParams ? [] : undefined;

    if (templateSpec.useDepths) {
      if (options.depths) {
        depths = context != options.depths[0] ? [context].concat(options.depths) : options.depths;
      } else {
        depths = [context];
      }
    }

    function main(context
    /*, options*/
    ) {
      return '' + templateSpec.main(container, context, container.helpers, container.partials, data, blockParams, depths);
    }

    main = executeDecorators(templateSpec.main, main, container, options.depths || [], data, blockParams);
    return main(context, options);
  }

  ret.isTop = true;

  ret._setup = function (options) {
    if (!options.partial) {
      container.helpers = container.merge(options.helpers, env.helpers);

      if (templateSpec.usePartial) {
        container.partials = container.merge(options.partials, env.partials);
      }

      if (templateSpec.usePartial || templateSpec.useDecorators) {
        container.decorators = container.merge(options.decorators, env.decorators);
      }
    } else {
      container.helpers = options.helpers;
      container.partials = options.partials;
      container.decorators = options.decorators;
    }
  };

  ret._child = function (i, data, blockParams, depths) {
    if (templateSpec.useBlockParams && !blockParams) {
      throw new _exception2['default']('must pass block params');
    }

    if (templateSpec.useDepths && !depths) {
      throw new _exception2['default']('must pass parent depths');
    }

    return wrapProgram(container, i, templateSpec[i], data, 0, blockParams, depths);
  };

  return ret;
}

function wrapProgram(container, i, fn, data, declaredBlockParams, blockParams, depths) {
  function prog(context) {
    var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];
    var currentDepths = depths;

    if (depths && context != depths[0] && !(context === container.nullContext && depths[0] === null)) {
      currentDepths = [context].concat(depths);
    }

    return fn(container, context, container.helpers, container.partials, options.data || data, blockParams && [options.blockParams].concat(blockParams), currentDepths);
  }

  prog = executeDecorators(fn, prog, container, depths, data, blockParams);
  prog.program = i;
  prog.depth = depths ? depths.length : 0;
  prog.blockParams = declaredBlockParams || 0;
  return prog;
}

function resolvePartial(partial, context, options) {
  if (!partial) {
    if (options.name === '@partial-block') {
      partial = options.data['partial-block'];
    } else {
      partial = options.partials[options.name];
    }
  } else if (!partial.call && !options.name) {
    // This is a dynamic partial that returned a string
    options.name = partial;
    partial = options.partials[partial];
  }

  return partial;
}

function invokePartial(partial, context, options) {
  // Use the current closure context to save the partial-block if this partial
  var currentPartialBlock = options.data && options.data['partial-block'];
  options.partial = true;

  if (options.ids) {
    options.data.contextPath = options.ids[0] || options.data.contextPath;
  }

  var partialBlock = undefined;

  if (options.fn && options.fn !== noop) {
    (function () {
      options.data = _base.createFrame(options.data); // Wrapper function to get access to currentPartialBlock from the closure

      var fn = options.fn;

      partialBlock = options.data['partial-block'] = function partialBlockWrapper(context) {
        var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1]; // Restore the partial-block from the closure for the execution of the block
        // i.e. the part inside the block of the partial call.

        options.data = _base.createFrame(options.data);
        options.data['partial-block'] = currentPartialBlock;
        return fn(context, options);
      };

      if (fn.partials) {
        options.partials = Utils.extend({}, options.partials, fn.partials);
      }
    })();
  }

  if (partial === undefined && partialBlock) {
    partial = partialBlock;
  }

  if (partial === undefined) {
    throw new _exception2['default']('The partial ' + options.name + ' could not be found');
  } else if (partial instanceof Function) {
    return partial(context, options);
  }
}

function noop() {
  return '';
}

function initData(context, data) {
  if (!data || !('root' in data)) {
    data = data ? _base.createFrame(data) : {};
    data.root = context;
  }

  return data;
}

function executeDecorators(fn, prog, container, depths, data, blockParams) {
  if (fn.decorator) {
    var props = {};
    prog = fn.decorator(prog, props, container, depths && depths[0], data, blockParams, depths);
    Utils.extend(prog, props);
  }

  return prog;
}

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/safe-string.js":
/*!********************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/safe-string.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Build out our basic SafeString type


exports.__esModule = true;

function SafeString(string) {
  this.string = string;
}

SafeString.prototype.toString = SafeString.prototype.toHTML = function () {
  return '' + this.string;
};

exports['default'] = SafeString;
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/handlebars/dist/cjs/handlebars/utils.js":
/*!**************************************************************!*\
  !*** ./node_modules/handlebars/dist/cjs/handlebars/utils.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.extend = extend;
exports.indexOf = indexOf;
exports.escapeExpression = escapeExpression;
exports.isEmpty = isEmpty;
exports.createFrame = createFrame;
exports.blockParams = blockParams;
exports.appendContextPath = appendContextPath;
var escape = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#x27;',
  '`': '&#x60;',
  '=': '&#x3D;'
};
var badChars = /[&<>"'`=]/g,
    possible = /[&<>"'`=]/;

function escapeChar(chr) {
  return escape[chr];
}

function extend(obj
/* , ...source */
) {
  for (var i = 1; i < arguments.length; i++) {
    for (var key in arguments[i]) {
      if (Object.prototype.hasOwnProperty.call(arguments[i], key)) {
        obj[key] = arguments[i][key];
      }
    }
  }

  return obj;
}

var toString = Object.prototype.toString;
exports.toString = toString; // Sourced from lodash
// https://github.com/bestiejs/lodash/blob/master/LICENSE.txt

/* eslint-disable func-style */

var isFunction = function isFunction(value) {
  return typeof value === 'function';
}; // fallback for older versions of Chrome and Safari

/* istanbul ignore next */


if (isFunction(/x/)) {
  exports.isFunction = isFunction = function (value) {
    return typeof value === 'function' && toString.call(value) === '[object Function]';
  };
}

exports.isFunction = isFunction;
/* eslint-enable func-style */

/* istanbul ignore next */

var isArray = Array.isArray || function (value) {
  return value && typeof value === 'object' ? toString.call(value) === '[object Array]' : false;
};

exports.isArray = isArray; // Older IE versions do not directly support indexOf so we must implement our own, sadly.

function indexOf(array, value) {
  for (var i = 0, len = array.length; i < len; i++) {
    if (array[i] === value) {
      return i;
    }
  }

  return -1;
}

function escapeExpression(string) {
  if (typeof string !== 'string') {
    // don't escape SafeStrings, since they're already safe
    if (string && string.toHTML) {
      return string.toHTML();
    } else if (string == null) {
      return '';
    } else if (!string) {
      return string + '';
    } // Force a string conversion as this will be done by the append regardless and
    // the regex test will do this transparently behind the scenes, causing issues if
    // an object's to string has escaped characters in it.


    string = '' + string;
  }

  if (!possible.test(string)) {
    return string;
  }

  return string.replace(badChars, escapeChar);
}

function isEmpty(value) {
  if (!value && value !== 0) {
    return true;
  } else if (isArray(value) && value.length === 0) {
    return true;
  } else {
    return false;
  }
}

function createFrame(object) {
  var frame = extend({}, object);
  frame._parent = object;
  return frame;
}

function blockParams(params, ids) {
  params.path = ids;
  return params;
}

function appendContextPath(contextPath, id) {
  return (contextPath ? contextPath + '.' : '') + id;
}

/***/ }),

/***/ "./node_modules/handlebars/runtime.js":
/*!********************************************!*\
  !*** ./node_modules/handlebars/runtime.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Create a simple path alias to allow browserify to resolve
// the runtime on a supported path.
module.exports = __webpack_require__(/*! ./dist/cjs/handlebars.runtime */ "./node_modules/handlebars/dist/cjs/handlebars.runtime.js")['default'];

/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g; // This works in non-strict mode

g = function () {
  return this;
}();

try {
  // This works if eval is allowed (see CSP)
  g = g || new Function("return this")();
} catch (e) {
  // This works if the window reference is available
  if (typeof window === "object") g = window;
} // g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}


module.exports = g;

/***/ }),

/***/ "./node_modules/whatwg-fetch/fetch.js":
/*!********************************************!*\
  !*** ./node_modules/whatwg-fetch/fetch.js ***!
  \********************************************/
/*! exports provided: Headers, Request, Response, DOMException, fetch */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Headers", function() { return Headers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Request", function() { return Request; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Response", function() { return Response; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DOMException", function() { return DOMException; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fetch", function() { return fetch; });
var support = {
  searchParams: 'URLSearchParams' in self,
  iterable: 'Symbol' in self && 'iterator' in Symbol,
  blob: 'FileReader' in self && 'Blob' in self && function () {
    try {
      new Blob();
      return true;
    } catch (e) {
      return false;
    }
  }(),
  formData: 'FormData' in self,
  arrayBuffer: 'ArrayBuffer' in self
};

function isDataView(obj) {
  return obj && DataView.prototype.isPrototypeOf(obj);
}

if (support.arrayBuffer) {
  var viewClasses = ['[object Int8Array]', '[object Uint8Array]', '[object Uint8ClampedArray]', '[object Int16Array]', '[object Uint16Array]', '[object Int32Array]', '[object Uint32Array]', '[object Float32Array]', '[object Float64Array]'];

  var isArrayBufferView = ArrayBuffer.isView || function (obj) {
    return obj && viewClasses.indexOf(Object.prototype.toString.call(obj)) > -1;
  };
}

function normalizeName(name) {
  if (typeof name !== 'string') {
    name = String(name);
  }

  if (/[^a-z0-9\-#$%&'*+.^_`|~]/i.test(name)) {
    throw new TypeError('Invalid character in header field name');
  }

  return name.toLowerCase();
}

function normalizeValue(value) {
  if (typeof value !== 'string') {
    value = String(value);
  }

  return value;
} // Build a destructive iterator for the value list


function iteratorFor(items) {
  var iterator = {
    next: function () {
      var value = items.shift();
      return {
        done: value === undefined,
        value: value
      };
    }
  };

  if (support.iterable) {
    iterator[Symbol.iterator] = function () {
      return iterator;
    };
  }

  return iterator;
}

function Headers(headers) {
  this.map = {};

  if (headers instanceof Headers) {
    headers.forEach(function (value, name) {
      this.append(name, value);
    }, this);
  } else if (Array.isArray(headers)) {
    headers.forEach(function (header) {
      this.append(header[0], header[1]);
    }, this);
  } else if (headers) {
    Object.getOwnPropertyNames(headers).forEach(function (name) {
      this.append(name, headers[name]);
    }, this);
  }
}

Headers.prototype.append = function (name, value) {
  name = normalizeName(name);
  value = normalizeValue(value);
  var oldValue = this.map[name];
  this.map[name] = oldValue ? oldValue + ', ' + value : value;
};

Headers.prototype['delete'] = function (name) {
  delete this.map[normalizeName(name)];
};

Headers.prototype.get = function (name) {
  name = normalizeName(name);
  return this.has(name) ? this.map[name] : null;
};

Headers.prototype.has = function (name) {
  return this.map.hasOwnProperty(normalizeName(name));
};

Headers.prototype.set = function (name, value) {
  this.map[normalizeName(name)] = normalizeValue(value);
};

Headers.prototype.forEach = function (callback, thisArg) {
  for (var name in this.map) {
    if (this.map.hasOwnProperty(name)) {
      callback.call(thisArg, this.map[name], name, this);
    }
  }
};

Headers.prototype.keys = function () {
  var items = [];
  this.forEach(function (value, name) {
    items.push(name);
  });
  return iteratorFor(items);
};

Headers.prototype.values = function () {
  var items = [];
  this.forEach(function (value) {
    items.push(value);
  });
  return iteratorFor(items);
};

Headers.prototype.entries = function () {
  var items = [];
  this.forEach(function (value, name) {
    items.push([name, value]);
  });
  return iteratorFor(items);
};

if (support.iterable) {
  Headers.prototype[Symbol.iterator] = Headers.prototype.entries;
}

function consumed(body) {
  if (body.bodyUsed) {
    return Promise.reject(new TypeError('Already read'));
  }

  body.bodyUsed = true;
}

function fileReaderReady(reader) {
  return new Promise(function (resolve, reject) {
    reader.onload = function () {
      resolve(reader.result);
    };

    reader.onerror = function () {
      reject(reader.error);
    };
  });
}

function readBlobAsArrayBuffer(blob) {
  var reader = new FileReader();
  var promise = fileReaderReady(reader);
  reader.readAsArrayBuffer(blob);
  return promise;
}

function readBlobAsText(blob) {
  var reader = new FileReader();
  var promise = fileReaderReady(reader);
  reader.readAsText(blob);
  return promise;
}

function readArrayBufferAsText(buf) {
  var view = new Uint8Array(buf);
  var chars = new Array(view.length);

  for (var i = 0; i < view.length; i++) {
    chars[i] = String.fromCharCode(view[i]);
  }

  return chars.join('');
}

function bufferClone(buf) {
  if (buf.slice) {
    return buf.slice(0);
  } else {
    var view = new Uint8Array(buf.byteLength);
    view.set(new Uint8Array(buf));
    return view.buffer;
  }
}

function Body() {
  this.bodyUsed = false;

  this._initBody = function (body) {
    this._bodyInit = body;

    if (!body) {
      this._bodyText = '';
    } else if (typeof body === 'string') {
      this._bodyText = body;
    } else if (support.blob && Blob.prototype.isPrototypeOf(body)) {
      this._bodyBlob = body;
    } else if (support.formData && FormData.prototype.isPrototypeOf(body)) {
      this._bodyFormData = body;
    } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
      this._bodyText = body.toString();
    } else if (support.arrayBuffer && support.blob && isDataView(body)) {
      this._bodyArrayBuffer = bufferClone(body.buffer); // IE 10-11 can't handle a DataView body.

      this._bodyInit = new Blob([this._bodyArrayBuffer]);
    } else if (support.arrayBuffer && (ArrayBuffer.prototype.isPrototypeOf(body) || isArrayBufferView(body))) {
      this._bodyArrayBuffer = bufferClone(body);
    } else {
      this._bodyText = body = Object.prototype.toString.call(body);
    }

    if (!this.headers.get('content-type')) {
      if (typeof body === 'string') {
        this.headers.set('content-type', 'text/plain;charset=UTF-8');
      } else if (this._bodyBlob && this._bodyBlob.type) {
        this.headers.set('content-type', this._bodyBlob.type);
      } else if (support.searchParams && URLSearchParams.prototype.isPrototypeOf(body)) {
        this.headers.set('content-type', 'application/x-www-form-urlencoded;charset=UTF-8');
      }
    }
  };

  if (support.blob) {
    this.blob = function () {
      var rejected = consumed(this);

      if (rejected) {
        return rejected;
      }

      if (this._bodyBlob) {
        return Promise.resolve(this._bodyBlob);
      } else if (this._bodyArrayBuffer) {
        return Promise.resolve(new Blob([this._bodyArrayBuffer]));
      } else if (this._bodyFormData) {
        throw new Error('could not read FormData body as blob');
      } else {
        return Promise.resolve(new Blob([this._bodyText]));
      }
    };

    this.arrayBuffer = function () {
      if (this._bodyArrayBuffer) {
        return consumed(this) || Promise.resolve(this._bodyArrayBuffer);
      } else {
        return this.blob().then(readBlobAsArrayBuffer);
      }
    };
  }

  this.text = function () {
    var rejected = consumed(this);

    if (rejected) {
      return rejected;
    }

    if (this._bodyBlob) {
      return readBlobAsText(this._bodyBlob);
    } else if (this._bodyArrayBuffer) {
      return Promise.resolve(readArrayBufferAsText(this._bodyArrayBuffer));
    } else if (this._bodyFormData) {
      throw new Error('could not read FormData body as text');
    } else {
      return Promise.resolve(this._bodyText);
    }
  };

  if (support.formData) {
    this.formData = function () {
      return this.text().then(decode);
    };
  }

  this.json = function () {
    return this.text().then(JSON.parse);
  };

  return this;
} // HTTP methods whose capitalization should be normalized


var methods = ['DELETE', 'GET', 'HEAD', 'OPTIONS', 'POST', 'PUT'];

function normalizeMethod(method) {
  var upcased = method.toUpperCase();
  return methods.indexOf(upcased) > -1 ? upcased : method;
}

function Request(input, options) {
  options = options || {};
  var body = options.body;

  if (input instanceof Request) {
    if (input.bodyUsed) {
      throw new TypeError('Already read');
    }

    this.url = input.url;
    this.credentials = input.credentials;

    if (!options.headers) {
      this.headers = new Headers(input.headers);
    }

    this.method = input.method;
    this.mode = input.mode;
    this.signal = input.signal;

    if (!body && input._bodyInit != null) {
      body = input._bodyInit;
      input.bodyUsed = true;
    }
  } else {
    this.url = String(input);
  }

  this.credentials = options.credentials || this.credentials || 'same-origin';

  if (options.headers || !this.headers) {
    this.headers = new Headers(options.headers);
  }

  this.method = normalizeMethod(options.method || this.method || 'GET');
  this.mode = options.mode || this.mode || null;
  this.signal = options.signal || this.signal;
  this.referrer = null;

  if ((this.method === 'GET' || this.method === 'HEAD') && body) {
    throw new TypeError('Body not allowed for GET or HEAD requests');
  }

  this._initBody(body);
}

Request.prototype.clone = function () {
  return new Request(this, {
    body: this._bodyInit
  });
};

function decode(body) {
  var form = new FormData();
  body.trim().split('&').forEach(function (bytes) {
    if (bytes) {
      var split = bytes.split('=');
      var name = split.shift().replace(/\+/g, ' ');
      var value = split.join('=').replace(/\+/g, ' ');
      form.append(decodeURIComponent(name), decodeURIComponent(value));
    }
  });
  return form;
}

function parseHeaders(rawHeaders) {
  var headers = new Headers(); // Replace instances of \r\n and \n followed by at least one space or horizontal tab with a space
  // https://tools.ietf.org/html/rfc7230#section-3.2

  var preProcessedHeaders = rawHeaders.replace(/\r?\n[\t ]+/g, ' ');
  preProcessedHeaders.split(/\r?\n/).forEach(function (line) {
    var parts = line.split(':');
    var key = parts.shift().trim();

    if (key) {
      var value = parts.join(':').trim();
      headers.append(key, value);
    }
  });
  return headers;
}

Body.call(Request.prototype);
function Response(bodyInit, options) {
  if (!options) {
    options = {};
  }

  this.type = 'default';
  this.status = options.status === undefined ? 200 : options.status;
  this.ok = this.status >= 200 && this.status < 300;
  this.statusText = 'statusText' in options ? options.statusText : 'OK';
  this.headers = new Headers(options.headers);
  this.url = options.url || '';

  this._initBody(bodyInit);
}
Body.call(Response.prototype);

Response.prototype.clone = function () {
  return new Response(this._bodyInit, {
    status: this.status,
    statusText: this.statusText,
    headers: new Headers(this.headers),
    url: this.url
  });
};

Response.error = function () {
  var response = new Response(null, {
    status: 0,
    statusText: ''
  });
  response.type = 'error';
  return response;
};

var redirectStatuses = [301, 302, 303, 307, 308];

Response.redirect = function (url, status) {
  if (redirectStatuses.indexOf(status) === -1) {
    throw new RangeError('Invalid status code');
  }

  return new Response(null, {
    status: status,
    headers: {
      location: url
    }
  });
};

var DOMException = self.DOMException;

try {
  new DOMException();
} catch (err) {
  DOMException = function (message, name) {
    this.message = message;
    this.name = name;
    var error = Error(message);
    this.stack = error.stack;
  };

  DOMException.prototype = Object.create(Error.prototype);
  DOMException.prototype.constructor = DOMException;
}

function fetch(input, init) {
  return new Promise(function (resolve, reject) {
    var request = new Request(input, init);

    if (request.signal && request.signal.aborted) {
      return reject(new DOMException('Aborted', 'AbortError'));
    }

    var xhr = new XMLHttpRequest();

    function abortXhr() {
      xhr.abort();
    }

    xhr.onload = function () {
      var options = {
        status: xhr.status,
        statusText: xhr.statusText,
        headers: parseHeaders(xhr.getAllResponseHeaders() || '')
      };
      options.url = 'responseURL' in xhr ? xhr.responseURL : options.headers.get('X-Request-URL');
      var body = 'response' in xhr ? xhr.response : xhr.responseText;
      resolve(new Response(body, options));
    };

    xhr.onerror = function () {
      reject(new TypeError('Network request failed'));
    };

    xhr.ontimeout = function () {
      reject(new TypeError('Network request failed'));
    };

    xhr.onabort = function () {
      reject(new DOMException('Aborted', 'AbortError'));
    };

    xhr.open(request.method, request.url, true);

    if (request.credentials === 'include') {
      xhr.withCredentials = true;
    } else if (request.credentials === 'omit') {
      xhr.withCredentials = false;
    }

    if ('responseType' in xhr && support.blob) {
      xhr.responseType = 'blob';
    }

    request.headers.forEach(function (value, name) {
      xhr.setRequestHeader(name, value);
    });

    if (request.signal) {
      request.signal.addEventListener('abort', abortXhr);

      xhr.onreadystatechange = function () {
        // DONE (success or failure)
        if (xhr.readyState === 4) {
          request.signal.removeEventListener('abort', abortXhr);
        }
      };
    }

    xhr.send(typeof request._bodyInit === 'undefined' ? null : request._bodyInit);
  });
}
fetch.polyfill = true;

if (!self.fetch) {
  self.fetch = fetch;
  self.Headers = Headers;
  self.Request = Request;
  self.Response = Response;
}

/***/ }),

/***/ "./src/public/js/core/data-manager.js":
/*!********************************************!*\
  !*** ./src/public/js/core/data-manager.js ***!
  \********************************************/
/*! exports provided: DataManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataManager", function() { return DataManager; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var DataManager =
/*#__PURE__*/
function () {
  function DataManager() {
    _classCallCheck(this, DataManager);
  }

  _createClass(DataManager, null, [{
    key: "storeBanks",
    value: function storeBanks(collection) {
      localStorage.setItem("swyp-businesses", JSON.stringify(collection));
    }
  }, {
    key: "getBanks",
    value: function getBanks() {
      return JSON.parse(localStorage.getItem("swyp-businesses"));
    }
  }, {
    key: "findBank",
    value: function findBank(slug) {
      var businesses = JSON.parse(localStorage.getItem("swyp-businesses"));
      return businesses.find(function (biz) {
        return biz.slug === slug;
      });
    }
  }, {
    key: "storeForms",
    value: function storeForms(collection) {
      localStorage.setItem("swyp-forms", JSON.stringify(collection));
    }
  }, {
    key: "getForms",
    value: function getForms() {
      return JSON.parse(localStorage.getItem("swyp-forms"));
    }
  }, {
    key: "findForm",
    value: function findForm(slug) {
      try {
        var forms = JSON.parse(localStorage.getItem("swyp-forms"));
        return forms.find(function (form) {
          return form.slug === slug;
        });
      } catch (error) {
        return null;
      }
    }
  }]);

  return DataManager;
}();

/***/ }),

/***/ "./src/public/js/core/http.js":
/*!************************************!*\
  !*** ./src/public/js/core/http.js ***!
  \************************************/
/*! exports provided: Http */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Http", function() { return Http; });
/* harmony import */ var whatwg_fetch__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! whatwg-fetch */ "./node_modules/whatwg-fetch/fetch.js");

var baseUrl = "https://business-backend-service.herokuapp.com/api/v1/"; // const baseUrl = "http://localhost:4000/api/v1/";

var Http = {
  get: function get(url) {
    return fetch("".concat(baseUrl).concat(url)).then(function (response) {
      return processResponse(response);
    });
  },
  post: function post(url, data) {
    return fetch("".concat(baseUrl).concat(url), {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify(data)
    }).then(function (response) {
      return processResponse(response);
    });
  },
  put: function put(url, data) {
    return fetch("".concat(baseUrl).concat(url), {
      method: "PUT",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify(data)
    }).then(function (response) {
      return processResponse(response);
    });
  },
  upLoadForm: function upLoadForm(url, formData) {
    return fetch("".concat(baseUrl).concat(url), {
      method: "POST",
      body: formData
    }).then(function (response) {
      return processResponse(response);
    });
  }
};

var processResponse = function processResponse(response) {
  if (response.ok) {
    return response.json();
  }

  var error = new Error("NetworkError");
  error.detials = response.json();
  throw error;
};

/***/ }),

/***/ "./src/public/js/core/index.js":
/*!*************************************!*\
  !*** ./src/public/js/core/index.js ***!
  \*************************************/
/*! exports provided: DataManager, Http */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _data_manager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./data-manager */ "./src/public/js/core/data-manager.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "DataManager", function() { return _data_manager__WEBPACK_IMPORTED_MODULE_0__["DataManager"]; });

/* harmony import */ var _http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./http */ "./src/public/js/core/http.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Http", function() { return _http__WEBPACK_IMPORTED_MODULE_1__["Http"]; });




/***/ }),

/***/ "./src/public/js/core/response-canvas/callback.js":
/*!********************************************************!*\
  !*** ./src/public/js/core/response-canvas/callback.js ***!
  \********************************************************/
/*! exports provided: Callback */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Callback", function() { return Callback; });
/* harmony import */ var _notifier__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./notifier */ "./src/public/js/core/response-canvas/notifier.js");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helper */ "./src/public/js/core/response-canvas/helper.js");
/* harmony import */ var _state__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./state */ "./src/public/js/core/response-canvas/state.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }




var Callback =
/*#__PURE__*/
function () {
  function Callback() {
    _classCallCheck(this, Callback);
  }

  _createClass(Callback, null, [{
    key: "intersectTargetAbove",
    // if TOP edge of target crosses threshold,
    // bottom must be > 0 which means it is on "screen" (shifted by offset)
    value: function intersectTargetAbove(entries) {
      _helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].updateDirection();
      entries.forEach(function (entry) {
        var isIntersecting = entry.isIntersecting,
            boundingClientRect = entry.boundingClientRect,
            target = entry.target; // bottom is how far bottom edge of target element is from top of viewport

        var bottom = boundingClientRect.bottom,
            height = boundingClientRect.height;
        var bottomAdjusted = bottom - _state__WEBPACK_IMPORTED_MODULE_2__["State"].offsetMargin;
        var targetIndex = _helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].getIndex(target);
        var targetState = _state__WEBPACK_IMPORTED_MODULE_2__["State"].targetStates[targetIndex];

        if (bottomAdjusted >= -_state__WEBPACK_IMPORTED_MODULE_2__["State"].ZERO_MOE) {
          if (isIntersecting && _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction === "down" && targetState.mode !== "enter") {
            _notifier__WEBPACK_IMPORTED_MODULE_0__["Notifier"].notifyTargetEnter(target, _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction);
          } else if (!isIntersecting && _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction === "up" && targetState.mode === "enter") {
            _notifier__WEBPACK_IMPORTED_MODULE_0__["Notifier"].notifyTargetExit(target, _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction);
          } else if (!isIntersecting && bottomAdjusted >= height && _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction === "down" && targetState.mode === "enter") {
            _notifier__WEBPACK_IMPORTED_MODULE_0__["Notifier"].notifyTargetExit(target, _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction);
          }
        }
      });
    }
  }, {
    key: "intersectTargetBelow",
    value: function intersectTargetBelow(entries) {
      _helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].updateDirection();
      entries.forEach(function (entry) {
        var isIntersecting = entry.isIntersecting,
            boundingClientRect = entry.boundingClientRect,
            target = entry.target;
        var bottom = boundingClientRect.bottom,
            height = boundingClientRect.height;
        var bottomAdjusted = bottom - _state__WEBPACK_IMPORTED_MODULE_2__["State"].offsetMargin;
        var targetIndex = _helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].getIndex(target);
        var targetState = _state__WEBPACK_IMPORTED_MODULE_2__["State"].targetStates[targetIndex];

        if (bottomAdjusted >= -_state__WEBPACK_IMPORTED_MODULE_2__["State"].ZERO_MOE && bottomAdjusted < height && isIntersecting && _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction === "up" && targetState.mode !== "enter") {
          _notifier__WEBPACK_IMPORTED_MODULE_0__["Notifier"].notifyTargetEnter(target, _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction);
        } else if (bottomAdjusted <= _state__WEBPACK_IMPORTED_MODULE_2__["State"].ZERO_MOE && !isIntersecting && _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction === "down" && targetState.mode === "enter") {
          _notifier__WEBPACK_IMPORTED_MODULE_0__["Notifier"].notifyTargetExit(target, _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction);
        }
      });
    }
    /*
    if there is a scroll event where a target never intersects (therefore
    skipping an enter/exit trigger), use this fallback to detect if it is
    in view
    */

  }, {
    key: "intersectViewportAbove",
    value: function intersectViewportAbove(entries) {
      _helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].updateDirection();
      entries.forEach(function (entry) {
        var isIntersecting = entry.isIntersecting,
            target = entry.target;
        var index = _helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].getIndex(target);
        var targetState = _state__WEBPACK_IMPORTED_MODULE_2__["State"].targetStates[index];

        if (isIntersecting && _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction === "down" && targetState.state !== "enter" && targetState.direction !== "down") {
          _notifier__WEBPACK_IMPORTED_MODULE_0__["Notifier"].notifyTargetEnter(target, "down");
          _notifier__WEBPACK_IMPORTED_MODULE_0__["Notifier"].notifyTargetExit(target, "down");
        }
      });
    }
  }, {
    key: "intersectViewportBelow",
    value: function intersectViewportBelow(entries) {
      _helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].updateDirection();
      entries.forEach(function (entry) {
        var isIntersecting = entry.isIntersecting,
            target = entry.target;
        var index = _helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].getIndex(target);
        var targetState = _state__WEBPACK_IMPORTED_MODULE_2__["State"].targetStates[index];

        if (isIntersecting && _state__WEBPACK_IMPORTED_MODULE_2__["State"].direction === "up" && targetState.state !== "enter" && targetState.direction !== "up") {
          _notifier__WEBPACK_IMPORTED_MODULE_0__["Notifier"].notifyTargetEnter(target, "up");
          _notifier__WEBPACK_IMPORTED_MODULE_0__["Notifier"].notifyTargetExit(target, "up");
        }
      });
    }
  }]);

  return Callback;
}();

/***/ }),

/***/ "./src/public/js/core/response-canvas/canvas.js":
/*!******************************************************!*\
  !*** ./src/public/js/core/response-canvas/canvas.js ***!
  \******************************************************/
/*! exports provided: Canvas */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Canvas", function() { return Canvas; });
/* harmony import */ var _observer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./observer */ "./src/public/js/core/response-canvas/observer.js");
/* harmony import */ var _debugger__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./debugger */ "./src/public/js/core/response-canvas/debugger.js");
/* harmony import */ var _state__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./state */ "./src/public/js/core/response-canvas/state.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }




var Canvas =
/*#__PURE__*/
function () {
  function Canvas() {
    _classCallCheck(this, Canvas);
  }

  _createClass(Canvas, null, [{
    key: "unmountObservers",
    value: function unmountObservers() {
      _state__WEBPACK_IMPORTED_MODULE_2__["State"].unregisterObservers();
      return this;
    }
  }, {
    key: "setUp",
    value: function setUp(_ref) {
      var targets = _ref.targets,
          canvasClass = _ref.canvasClass,
          _ref$offset = _ref.offset,
          offset = _ref$offset === void 0 ? 0.20 : _ref$offset,
          _ref$debug = _ref.debug,
          debug = _ref$debug === void 0 ? false : _ref$debug;
      _state__WEBPACK_IMPORTED_MODULE_2__["State"].canvasClass = canvasClass;
      _state__WEBPACK_IMPORTED_MODULE_2__["State"].offsetValue = offset;
      _state__WEBPACK_IMPORTED_MODULE_2__["State"].targets = targets;
      _state__WEBPACK_IMPORTED_MODULE_2__["State"].debugMode = debug;
      setUpDebugMode();
      _state__WEBPACK_IMPORTED_MODULE_2__["State"].addIndexDateToTargets();
      _state__WEBPACK_IMPORTED_MODULE_2__["State"].setTargetStates();
      initialize();
      return this;
    }
  }, {
    key: "onTargetEnter",
    value: function onTargetEnter(cb) {
      _state__WEBPACK_IMPORTED_MODULE_2__["State"].registerTargetEnter(cb);
      return this;
    }
  }, {
    key: "onTargetExit",
    value: function onTargetExit(cb) {
      _state__WEBPACK_IMPORTED_MODULE_2__["State"].registerTargetExit(cb);
      return this;
    }
  }]);

  return Canvas;
}();

function initialize() {
  // Warning order of initialization is important
  _state__WEBPACK_IMPORTED_MODULE_2__["State"].setViewPortHeight();
  _state__WEBPACK_IMPORTED_MODULE_2__["State"].setPageHeight();
  _state__WEBPACK_IMPORTED_MODULE_2__["State"].setOffsetMargin();
  _state__WEBPACK_IMPORTED_MODULE_2__["State"].setTargetsOffsetHeight();
  _state__WEBPACK_IMPORTED_MODULE_2__["State"].setTargetsOffsetTop();
  _observer__WEBPACK_IMPORTED_MODULE_0__["Observer"].registerAll();

  if (_state__WEBPACK_IMPORTED_MODULE_2__["State"].debugMode) {
    _debugger__WEBPACK_IMPORTED_MODULE_1__["Debugger"].updateOffset(_state__WEBPACK_IMPORTED_MODULE_2__["State"].offsetMargin);
  }
}

function setUpDebugMode() {
  var offsetValue = _state__WEBPACK_IMPORTED_MODULE_2__["State"].offsetValue,
      targets = _state__WEBPACK_IMPORTED_MODULE_2__["State"].targets,
      debugMode = _state__WEBPACK_IMPORTED_MODULE_2__["State"].debugMode;
  if (debugMode) _debugger__WEBPACK_IMPORTED_MODULE_1__["Debugger"].setup({
    offsetValue: offsetValue,
    targets: targets
  });
}

/***/ }),

/***/ "./src/public/js/core/response-canvas/debugger.js":
/*!********************************************************!*\
  !*** ./src/public/js/core/response-canvas/debugger.js ***!
  \********************************************************/
/*! exports provided: Debugger */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Debugger", function() { return Debugger; });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var id = "1";
var Debugger =
/*#__PURE__*/
function () {
  function Debugger() {
    _classCallCheck(this, Debugger);
  }

  _createClass(Debugger, null, [{
    key: "setup",
    value: function setup(_ref) {
      var offsetValue = _ref.offsetValue,
          targets = _ref.targets;
      var targetClass = targets[0].getAttribute("class");
      setupOffset({
        id: id,
        offsetValue: offsetValue,
        targetClass: targetClass
      });
    }
  }, {
    key: "updateOffset",
    value: function updateOffset(offsetMargin) {
      var idVal = getOffsetId();
      var el = document.querySelector("#".concat(idVal));
      el.style.top = "".concat(offsetMargin, "px");
    }
  }, {
    key: "notifyStep",
    value: function notifyStep(state) {
      var idVal = getStepId();
      var elA = document.querySelector("#".concat(idVal, "_above"));
      var elB = document.querySelector("#".concat(idVal, "_below"));
      var display = state === "enter" ? "block" : "none";
      if (elA) elA.style.display = display;
      if (elB) elB.style.display = display;
    }
  }]);

  return Debugger;
}();

function getStepId(i) {
  return "scrollama__debug-step--".concat(id, "-").concat(i);
}

function getOffsetId() {
  return "scrollama__debug-offset--".concat(id);
}

function setupOffset(_ref2) {
  var offsetValue = _ref2.offsetValue,
      targetClass = _ref2.targetClass;
  var el = document.createElement("div");
  el.setAttribute("id", getOffsetId());
  el.setAttribute("class", "scrollama__debug-offset");
  el.style.position = "fixed";
  el.style.left = "0";
  el.style.width = "100%";
  el.style.height = "0px";
  el.style.borderTop = "2px dashed black";
  el.style.zIndex = "9999";
  var text = document.createElement("p");
  text.innerText = "\".".concat(targetClass, "\" trigger: ").concat(offsetValue);
  text.style.fontSize = "12px";
  text.style.fontFamily = "monospace";
  text.style.color = "black";
  text.style.margin = "0";
  text.style.padding = "6px";
  el.appendChild(text);
  document.body.appendChild(el);
}

/***/ }),

/***/ "./src/public/js/core/response-canvas/helper.js":
/*!******************************************************!*\
  !*** ./src/public/js/core/response-canvas/helper.js ***!
  \******************************************************/
/*! exports provided: Helper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Helper", function() { return Helper; });
/* harmony import */ var _state__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./state */ "./src/public/js/core/response-canvas/state.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }


var Helper =
/*#__PURE__*/
function () {
  function Helper() {
    _classCallCheck(this, Helper);
  }

  _createClass(Helper, null, [{
    key: "getPageHeight",
    value: function getPageHeight() {
      var html = document.documentElement;
      var body = document.body;
      return Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
    }
  }, {
    key: "getDistanceToTop",
    value: function getDistanceToTop(elem) {
      var distance = 0;

      if (elem.offsetParent) {
        do {
          distance += elem.offsetTop;
          elem = elem.offsetParent;
        } while (elem);
      }

      return distance < 0 ? 0 : distance;
    }
  }, {
    key: "getIndex",
    value: function getIndex(element) {
      return +element.getAttribute("data-intersect-index");
    }
  }, {
    key: "updateDirection",
    value: function updateDirection() {
      var className = _state__WEBPACK_IMPORTED_MODULE_0__["State"].canvasClass;
      var scrolledOffset = document.querySelector(".".concat(className)) ? document.querySelector(".".concat(className)).scrollTop : window.pageYOffset;

      if (scrolledOffset > _state__WEBPACK_IMPORTED_MODULE_0__["State"].previousYOffset) {
        _state__WEBPACK_IMPORTED_MODULE_0__["State"].direction = "down";
      } else if (scrolledOffset < _state__WEBPACK_IMPORTED_MODULE_0__["State"].previousYOffset) {
        _state__WEBPACK_IMPORTED_MODULE_0__["State"].direction = "up";
      }

      _state__WEBPACK_IMPORTED_MODULE_0__["State"].previousYOffset = scrolledOffset;
    }
  }]);

  return Helper;
}();

/***/ }),

/***/ "./src/public/js/core/response-canvas/index.js":
/*!*****************************************************!*\
  !*** ./src/public/js/core/response-canvas/index.js ***!
  \*****************************************************/
/*! exports provided: Canvas */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _canvas__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./canvas */ "./src/public/js/core/response-canvas/canvas.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Canvas", function() { return _canvas__WEBPACK_IMPORTED_MODULE_0__["Canvas"]; });



/***/ }),

/***/ "./src/public/js/core/response-canvas/notifier.js":
/*!********************************************************!*\
  !*** ./src/public/js/core/response-canvas/notifier.js ***!
  \********************************************************/
/*! exports provided: Notifier */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Notifier", function() { return Notifier; });
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./helper */ "./src/public/js/core/response-canvas/helper.js");
/* harmony import */ var _state__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./state */ "./src/public/js/core/response-canvas/state.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



var Notifier = function Notifier() {
  _classCallCheck(this, Notifier);
};

_defineProperty(Notifier, "notifyTargetEnter", function (element, direction) {
  var check = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
  var index = _helper__WEBPACK_IMPORTED_MODULE_0__["Helper"].getIndex(element);
  var response = {
    element: element,
    index: index,
    direction: direction
  };
  _state__WEBPACK_IMPORTED_MODULE_1__["State"].updateTargetStateDirection(index, direction);
  _state__WEBPACK_IMPORTED_MODULE_1__["State"].updateTargetStateMode(index, "enter");

  if (check && direction === "down") {
    notifyOthers(index, "above");
  }

  if (check && direction === "up") {
    notifyOthers(index, "below");
  }

  _state__WEBPACK_IMPORTED_MODULE_1__["State"].callTargetEnter(response);
});

_defineProperty(Notifier, "notifyTargetExit", function (element, direction) {
  var index = _helper__WEBPACK_IMPORTED_MODULE_0__["Helper"].getIndex(element);
  var response = {
    element: element,
    index: index,
    direction: direction
  };
  _state__WEBPACK_IMPORTED_MODULE_1__["State"].updateTargetStateDirection(index, direction);
  _state__WEBPACK_IMPORTED_MODULE_1__["State"].updateTargetStateMode(index, "exit");
  _state__WEBPACK_IMPORTED_MODULE_1__["State"].callTargetExit(response);
});

function notifyOthers(index, location) {
  var targets = _state__WEBPACK_IMPORTED_MODULE_1__["State"].targets,
      targetStates = _state__WEBPACK_IMPORTED_MODULE_1__["State"].targetStates;

  if (location === "above") {
    // check if targets below were skipped and notified first
    for (var i = 0; i < index; i++) {
      var targetState = targetStates[i];
      if (targetState.mode === "enter") Notifier.notifyTargetEnter(targets[i], "down");

      if (targetState.direction === "up") {
        Notifier.notifyTargetEnter(targets[i], "down", false);
        Notifier.notifyTargetExit(targets[i], "down");
      }
    }
  } else if (location === "below") {
    // check if targets above were skipped and notified first
    var len = targetStates.length;

    for (var _i = len - 1; _i > index; _i--) {
      var _targetState = targetStates[_i];
      if (_targetState.state === "enter") Notifier.notifyTargetExit(targets[_i], "up");

      if (_targetState.direction === "down") {
        Notifier.notifyTargetEnter(targets[_i], "up", false);
        Notifier.notifyTargetExit(targets[_i], "up");
      }
    }
  }
}

/***/ }),

/***/ "./src/public/js/core/response-canvas/observer.js":
/*!********************************************************!*\
  !*** ./src/public/js/core/response-canvas/observer.js ***!
  \********************************************************/
/*! exports provided: Observer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Observer", function() { return Observer; });
/* harmony import */ var _state__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./state */ "./src/public/js/core/response-canvas/state.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }


var Observer =
/*#__PURE__*/
function () {
  function Observer() {
    _classCallCheck(this, Observer);
  }

  _createClass(Observer, null, [{
    key: "registerAll",
    value: function registerAll() {
      _state__WEBPACK_IMPORTED_MODULE_0__["State"].registerObserverOnViewPortAbove();
      _state__WEBPACK_IMPORTED_MODULE_0__["State"].registerObserverOnViewPortBelow();
      _state__WEBPACK_IMPORTED_MODULE_0__["State"].registerObserversAbove();
      _state__WEBPACK_IMPORTED_MODULE_0__["State"].registerObserversBelow();
    }
  }]);

  return Observer;
}();

/***/ }),

/***/ "./src/public/js/core/response-canvas/state.js":
/*!*****************************************************!*\
  !*** ./src/public/js/core/response-canvas/state.js ***!
  \*****************************************************/
/*! exports provided: State */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "State", function() { return State; });
/* harmony import */ var _callback__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./callback */ "./src/public/js/core/response-canvas/callback.js");
/* harmony import */ var _helper__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helper */ "./src/public/js/core/response-canvas/helper.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var stateValues = {
  observers: {
    above: [],
    below: [],
    viewportAbove: [],
    viewportBelow: []
  },
  callbacks: {
    targetEnter: null,
    targetExit: null
  },
  targetsOffsetHeight: [],
  targetsOffsetTop: [],
  previousYOffset: -1,
  canvasClass: null,
  viewPortHeight: 0,
  targetStates: [],
  debugMode: false,
  offsetMargin: 0,
  direction: null,
  offsetValue: 0,
  pageHeight: 0,
  ZERO_MOE: 1,
  targets: []
};
var State =
/*#__PURE__*/
function () {
  function State() {
    _classCallCheck(this, State);
  }

  _createClass(State, null, [{
    key: "addIndexDateToTargets",
    value: function addIndexDateToTargets() {
      stateValues.targets.forEach(function (el, index) {
        return el.setAttribute("data-intersect-index", index);
      });
    }
  }, {
    key: "setViewPortHeight",
    value: function setViewPortHeight() {
      stateValues.viewPortHeight = window.innerHeight;
    }
  }, {
    key: "setPageHeight",
    value: function setPageHeight() {
      stateValues.pageHeight = _helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].getPageHeight();
    }
  }, {
    key: "setOffsetMargin",
    value: function setOffsetMargin() {
      var offsetValue = stateValues.offsetValue,
          viewPortHeight = stateValues.viewPortHeight;
      stateValues.offsetMargin = offsetValue * viewPortHeight;
    }
  }, {
    key: "setTargetStates",
    value: function setTargetStates() {
      stateValues.targetStates = stateValues.targets.map(function () {
        return {
          direction: null,
          state: null
        };
      });
    }
  }, {
    key: "setTargetsOffsetTop",
    value: function setTargetsOffsetTop() {
      var targets = stateValues.targets;
      if (!targets[0]) throw new Error("Canvas doesn't have target element to monitor");
      stateValues.targetsOffsetTop = targets.map(_helper__WEBPACK_IMPORTED_MODULE_1__["Helper"].getDistanceToTop);
    }
  }, {
    key: "setTargetsOffsetHeight",
    value: function setTargetsOffsetHeight() {
      var targets = stateValues.targets;
      if (!targets[0]) throw new Error("Canvas doesn't have target element to monitor");
      stateValues.targetsOffsetHeight = targets.map(function (el) {
        return el.offsetHeight;
      });
    }
  }, {
    key: "registerTargetEnter",
    value: function registerTargetEnter(cb) {
      if (typeof cb !== "function") throw new Error("Argument passed is not a function");
      stateValues.callbacks.targetEnter = cb;
    }
  }, {
    key: "registerTargetExit",
    value: function registerTargetExit(cb) {
      if (typeof cb !== "function") throw new Error("Argument passed is not a function");
      stateValues.callbacks.targetExit = cb;
    }
  }, {
    key: "callTargetExit",
    value: function callTargetExit(response) {
      var callbacks = stateValues.callbacks,
          targetStates = stateValues.targetStates;

      if (callbacks.targetExit && typeof callbacks.targetExit === "function") {
        callbacks.targetExit(response, targetStates);
      }
    }
  }, {
    key: "callTargetEnter",
    value: function callTargetEnter(response) {
      var callbacks = stateValues.callbacks,
          targetStates = stateValues.targetStates;

      if (callbacks.targetEnter && typeof callbacks.targetEnter === "function") {
        callbacks.targetEnter(response, targetStates);
      }
    }
  }, {
    key: "registerObserversAbove",
    value: function registerObserversAbove() {
      var targetsOffsetHeight = stateValues.targetsOffsetHeight,
          viewPortHeight = stateValues.viewPortHeight,
          offsetMargin = stateValues.offsetMargin;
      var observers = stateValues.observers,
          targets = stateValues.targets;
      if (observers.above[0]) observers.above.forEach(function (observer) {
        return observer.disconnect();
      });
      observers.above = targets.map(function (target, index) {
        var marginTop = targetsOffsetHeight[index];
        var marginBottom = -viewPortHeight + offsetMargin;
        var rootMargin = "".concat(marginTop, "px 0px ").concat(marginBottom, "px 0px");
        var options = {
          root: null,
          rootMargin: rootMargin,
          threshold: 0
        };
        var observer = new IntersectionObserver(_callback__WEBPACK_IMPORTED_MODULE_0__["Callback"].intersectTargetAbove, options);
        observer.observe(target);
        return observer;
      });
    }
  }, {
    key: "registerObserversBelow",
    value: function registerObserversBelow() {
      var targetsOffsetHeight = stateValues.targetsOffsetHeight,
          viewPortHeight = stateValues.viewPortHeight,
          offsetMargin = stateValues.offsetMargin;
      var observers = stateValues.observers,
          targets = stateValues.targets,
          pageHeight = stateValues.pageHeight;
      if (observers.below[0]) observers.below.forEach(function (observer) {
        return observer.disconnect();
      });
      observers.below = targets.map(function (target, index) {
        var marginTop = -offsetMargin;
        var marginBottom = pageHeight - viewPortHeight + targetsOffsetHeight[index] + offsetMargin;
        var rootMargin = "".concat(marginTop, "px 0px ").concat(marginBottom, "px 0px");
        var options = {
          root: null,
          rootMargin: rootMargin,
          threshold: 0
        };
        var observer = new IntersectionObserver(_callback__WEBPACK_IMPORTED_MODULE_0__["Callback"].intersectTargetBelow, options);
        observer.observe(target);
        return observer;
      });
    }
  }, {
    key: "registerObserverOnViewPortAbove",
    value: function registerObserverOnViewPortAbove() {
      var targetsOffsetHeight = stateValues.targetsOffsetHeight,
          viewPortHeight = stateValues.viewPortHeight,
          offsetMargin = stateValues.offsetMargin;
      var observers = stateValues.observers,
          targets = stateValues.targets;
      if (observers.viewportAbove[0]) observers.viewportAbove.forEach(function (observer) {
        return observer.disconnect();
      });
      observers.viewportAbove = targets.map(function (target, index) {
        var marginTop = targetsOffsetHeight[index];
        var marginBottom = -(viewPortHeight - offsetMargin + targetsOffsetHeight[index]);
        var rootMargin = "".concat(marginTop, "px 0px ").concat(marginBottom, "px 0px");
        var options = {
          root: null,
          rootMargin: rootMargin,
          threshold: 0
        };
        var observer = new IntersectionObserver(_callback__WEBPACK_IMPORTED_MODULE_0__["Callback"].intersectViewportAbove, options);
        observer.observe(target);
        return observer;
      });
    }
  }, {
    key: "registerObserverOnViewPortBelow",
    value: function registerObserverOnViewPortBelow() {
      var targetsOffsetHeight = stateValues.targetsOffsetHeight,
          pageHeight = stateValues.pageHeight,
          offsetMargin = stateValues.offsetMargin;
      var observers = stateValues.observers,
          targets = stateValues.targets,
          targetsOffsetTop = stateValues.targetsOffsetTop;
      if (observers.viewportBelow[0]) ;
      observers.viewportBelow.forEach(function (observer) {
        return observer.disconnect();
      });
      observers.viewportBelow = targets.map(function (target, index) {
        var marginTop = -(offsetMargin + targetsOffsetHeight[index]);
        var marginBottom = pageHeight - targetsOffsetTop[index] - targetsOffsetHeight[index] - offsetMargin;
        var rootMargin = "".concat(marginTop, "px 0px ").concat(marginBottom, "px 0px");
        var options = {
          root: null,
          rootMargin: rootMargin,
          threshold: 0
        };
        var observer = new IntersectionObserver(_callback__WEBPACK_IMPORTED_MODULE_0__["Callback"].intersectViewportBelow, options);
        observer.observe(target);
        return observer;
      });
    }
  }, {
    key: "unregisterObservers",
    value: function unregisterObservers() {
      var observers = stateValues.observers;
      if (observers.above[0]) observers.above.forEach(function (observer) {
        return observer.disconnect();
      });
      if (observers.below[0]) observers.below.forEach(function (observer) {
        return observer.disconnect();
      });
      if (observers.viewportAbove[0]) observers.viewportAbove.forEach(function (observer) {
        return observer.disconnect();
      });
      if (observers.viewportBelow[0]) observers.viewportBelow.forEach(function (observer) {
        return observer.disconnect();
      });
    }
  }, {
    key: "updateTargetStateMode",
    value: function updateTargetStateMode(index, mode) {
      if (!stateValues.targetStates[index]) throw new Error("The element with the provided index has no state data");
      stateValues.targetStates[index].mode = mode;
    }
  }, {
    key: "updateTargetStateDirection",
    value: function updateTargetStateDirection(index, direction) {
      if (!stateValues.targetStates[index]) throw new Error("The element with the provided index has no state data");
      stateValues.targetStates[index].direction = direction;
    }
  }, {
    key: "targets",
    get: function get() {
      return stateValues.targets;
    },
    set: function set(nodeList) {
      var list = [];

      if (nodeList instanceof NodeList) {
        [].forEach.call(nodeList, function (node) {
          return list.push(node);
        });
        stateValues.targets = list;
      }
    }
  }, {
    key: "canvasClass",
    get: function get() {
      return stateValues.canvasClass;
    },
    set: function set() {
      var string = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "canvas";
      stateValues.canvasClass = string;
    }
  }, {
    key: "ZERO_MOE",
    get: function get() {
      return stateValues.ZERO_MOE;
    }
  }, {
    key: "debugMode",
    get: function get() {
      return stateValues.debugMode;
    },
    set: function set(bool) {
      if (![true, false].includes(bool)) throw new Error("Argument ".concat(bool, " is not a valid value"));
      stateValues.debugMode = bool;
    }
  }, {
    key: "direction",
    get: function get() {
      return stateValues.direction;
    },
    set: function set(value) {
      if (!["up", "down"].includes(value)) throw new Error("Argument ".concat(value, " is not a valid direction"));
      stateValues.direction = value;
    }
  }, {
    key: "previousYOffset",
    get: function get() {
      return stateValues.previousYOffset;
    },
    set: function set(numb) {
      if (isNaN(numb)) throw new Error("Argument ".concat(numb, " is not a valid value"));
      stateValues.previousYOffset = numb;
    }
  }, {
    key: "viewPortHeight",
    get: function get() {
      return stateValues.viewPortHeight;
    }
  }, {
    key: "pageHeight",
    get: function get() {
      return stateValues.pageHeight;
    }
  }, {
    key: "offsetValue",
    get: function get() {
      return stateValues.offsetValue;
    },
    set: function set(numb) {
      if (isNaN(numb)) throw new Error("Argument ".concat(numb, " is not a valid value"));
      stateValues.offsetValue = Math.min(Math.max(0, numb), 1);
    }
  }, {
    key: "offsetMargin",
    get: function get() {
      return stateValues.offsetMargin;
    }
  }, {
    key: "targetStates",
    get: function get() {
      return stateValues.targetStates;
    }
  }, {
    key: "targetsOffsetTop",
    get: function get() {
      return stateValues.targetsOffsetTop;
    }
  }]);

  return State;
}();

/***/ }),

/***/ "./src/public/js/response-canvas/index.js":
/*!************************************************!*\
  !*** ./src/public/js/response-canvas/index.js ***!
  \************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _response_canvas__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./response-canvas */ "./src/public/js/response-canvas/response-canvas.js");

new _response_canvas__WEBPACK_IMPORTED_MODULE_0__["ResponseCanvas"](document.querySelector("body"));

/***/ }),

/***/ "./src/public/js/response-canvas/media-manager/picture.js":
/*!****************************************************************!*\
  !*** ./src/public/js/response-canvas/media-manager/picture.js ***!
  \****************************************************************/
/*! exports provided: PictureManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PictureManager", function() { return PictureManager; });
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./util */ "./src/public/js/response-canvas/media-manager/util.js");
function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


var PictureManager =
/*#__PURE__*/
function () {
  function PictureManager(componenntUI, id) {
    var _this = this;

    _classCallCheck(this, PictureManager);

    _defineProperty(this, "handleVideoCanPlay", function (ev) {
      _this.takePictureBtn.style.opacity = 1;

      if (!_this.streaming) {
        _this.height = _this.video.videoWidth / (_this.video.videoWidth / _this.video.videoHeight);
        _this.tak;

        _this.canvas.setAttribute("height", _this.height);

        _this.video.setAttribute("height", _this.height);

        _this.canvas.setAttribute("width", _this.width);

        _this.video.setAttribute("width", _this.width);

        _this.streaming = false;
      }
    });

    _defineProperty(this, "takePicture", function () {
      var context = _this.canvas.getContext("2d");

      if (_this.width && _this.height) {
        _this.canvas.height = _this.height;
        _this.canvas.width = _this.width;
        context.drawImage(_this.video, 0, 0, _this.width, _this.height);

        var data = _this.canvas.toDataURL("image/png");

        _this.photo.setAttribute("src", data);

        _this.pictureTaken = true;

        _this.photoContainer.classList.remove("des-content");

        _this.retakeButton.classList.remove("des-content");

        _this.uploadBtn.classList.remove("des-content");

        _this.videoContainer.classList.add("des-content");

        var videotrack = _this.stream.getVideoTracks()[0]; // if only one media track


        videotrack.stop();
      } else {
        _this.clearPhoto();
      }
    });

    _defineProperty(this, "startAgain", function () {
      _this.clearPhoto();

      _this.videoContainer.classList.remove("des-content");

      _this.photoContainer.classList.add("des-content");

      _this.retakeButton.classList.add("des-content");

      _this.uploadBtn.classList.add("des-content");
    });

    this.uploadBtn = document.querySelector(".uploadButton[id=\"".concat(id, "\"]"));
    this.retakeButton = document.querySelector(".resetButton[id=\"".concat(id, "\"]"));
    this.photoContainer = componenntUI.querySelector(".picture__output");
    this.videoContainer = componenntUI.querySelector(".picture__feed");
    this.video = componenntUI.getElementById("livefeed");
    this.takePictureBtn = componenntUI.getElementById("capture");
    this.canvas = componenntUI.getElementById("canvas");
    this.photo = componenntUI.getElementById("photo");
    this.streaming = false;
    this.pictureTaken = false;
    this.width = 340;
    this.questionId = id;
    this.height = 0;
    this.stream = null;
    this.iniate();
  }

  _createClass(PictureManager, [{
    key: "iniate",
    value: function iniate() {
      var _this2 = this;

      this.retakeButton.addEventListener("click", this.startAgain);

      var settings = _objectSpread({}, _util__WEBPACK_IMPORTED_MODULE_0__["constraints"]);

      delete settings.audio; // register events

      this.takePictureBtn.addEventListener("click", this.takePicture, false); // request stream data from media devices on auser system

      navigator.mediaDevices.getUserMedia(settings).then(function (stream) {
        _this2.video.srcObject = stream;

        _this2.video.play();

        _this2.stream = stream;
        localStorage.setItem("picture_stream", _this2.stream);
        console.log(_this2.stream);
      })["catch"](function (err) {
        return alert("Sorry we can't access your camera");
      });
      this.video.addEventListener("canplay", this.handleVideoCanPlay); // reset picture canvas

      this.clearPhoto();
    }
    /**
     * configure window elements when video start receiving data from media devices
     */

  }, {
    key: "clearPhoto",

    /**
     * Prepare the canvas for another picture
     */
    value: function clearPhoto() {
      var context = this.canvas.getContext("2d");
      context.fillStyle = "#AAA";
      context.fillRect(0, 0, this.canvas.width, this.canvas.height);
      var data = this.canvas.toDataURL("image/png");
      this.photo.setAttribute("src", data);
    }
  }, {
    key: "getAsset",

    /**
     * return asset that was recorded
     */
    value: function getAsset() {
      if (this.pictureTaken) {
        var data = this.canvas.toDataURL("image/png");
        var blobBin = atob(data.split(',')[1]);
        var array = [];

        for (var i = 0; i < blobBin.length; i++) {
          array.push(blobBin.charCodeAt(i));
        }

        return new Blob([new Uint8Array(array)], {
          type: 'image/png'
        });
      }

      return null;
    }
  }]);

  return PictureManager;
}();

/***/ }),

/***/ "./src/public/js/response-canvas/media-manager/util.js":
/*!*************************************************************!*\
  !*** ./src/public/js/response-canvas/media-manager/util.js ***!
  \*************************************************************/
/*! exports provided: constraints */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "constraints", function() { return constraints; });
var constraints = {
  video: {
    width: {
      ideal: 600
    },
    height: {
      ideal: 600
    },
    facingMode: "user"
  },
  audio: true
};

/***/ }),

/***/ "./src/public/js/response-canvas/media-manager/video.js":
/*!**************************************************************!*\
  !*** ./src/public/js/response-canvas/media-manager/video.js ***!
  \**************************************************************/
/*! exports provided: VideoManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoManager", function() { return VideoManager; });
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./util */ "./src/public/js/response-canvas/media-manager/util.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


var VideoManager =
/*#__PURE__*/
function () {
  function VideoManager(componentUI, id) {
    var _this = this;

    _classCallCheck(this, VideoManager);

    _defineProperty(this, "handleMediaSourceOpen", function () {
      _this.sourceBuffer = _this.mediaSource.addSourceBuffer('video/webm; codecs="vp8"');
    });

    _defineProperty(this, "handleRecording", function () {
      var text = _this.recordButton.textContent;

      if (text === "Start Recording") {
        _this.startRecording();
      } else if (text === "Record Again") {
        _this.recordAgain();
      } else {
        _this.stopRecording();
      }
    });

    _defineProperty(this, "playRecordedVideo", function () {
      var buffer = new Blob(_this.recordedBlob, {
        type: "video/webm"
      });
      _this.recordedVideo.src = null;
      _this.recordedVideo.srcObject = null;
      _this.recordedVideo.src = window.URL.createObjectURL(buffer);
      _this.recordedVideo.controls = true;

      _this.recordedVideo.play();
    });

    _defineProperty(this, "handleDataAvailable", function (event) {
      console.log(event);

      if (event.data && event.data.size > 0) {
        _this.recordedBlob.push(event.data);
      }
    });

    this.uploadBtn = document.querySelector(".uploadButton[id=\"".concat(id, "\"]"));
    this.recordedVideo = componentUI.querySelector("video#playback");
    this.recordButton = componentUI.querySelector("button#record");
    this.errorMsgElement = componentUI.querySelector("span#errorMsg");
    this.liveVideo = componentUI.querySelector("video#live");
    this.secondsSpan = componentUI.querySelector('.seconds');
    this.minutesSpan = componentUI.querySelector('.minutes');
    this.hoursSpan = componentUI.querySelector('.hours');
    this.mediaSource = new MediaSource();
    this.componentUI = componentUI;
    this.timeInterval = null;
    this.mediaRecorder = null;
    this.recordedBlob = null;
    this.sourceBuffer = null;
    this.stream = null;
    this.clonestream = null;
    this.init();
  }
  /**
   * Ask permission to user media and set up camera
   * start streaming media data from users devices
   */


  _createClass(VideoManager, [{
    key: "init",
    value: function init() {
      var _this2 = this;

      this.mediaSource.addEventListener("sourceopen", this.handleMediaSourceOpen);
      this.recordButton.addEventListener("click", this.handleRecording);

      try {
        navigator.mediaDevices.getUserMedia(_util__WEBPACK_IMPORTED_MODULE_0__["constraints"]).then(function (stream) {
          console.log(stream);
          _this2.stream = stream;
          _this2.liveVideo.srcObject = stream;
          _this2.clonestream = stream;
          _this2.recordButton.disabled = false;
        });
      } catch (ex) {
        console.log("Exception happened requesting access to user camera ".concat(ex));
      }
    }
    /**
     * process media data from user devices
     * @param { MediaStream } stream
     */

  }, {
    key: "recordAgain",

    /**
     * prepare environment to record another video
     */
    value: function recordAgain() {
      var _this3 = this;

      this.mediaSource = new MediaSource(); //    this.recordedVideo.classList.add("des-content");
      //    this.recordedVideo.src = null;
      //    this.recordedVideo.srcObject = null;

      try {
        navigator.mediaDevices.getUserMedia(_util__WEBPACK_IMPORTED_MODULE_0__["constraints"]).then(function (stream) {
          console.log(stream);
          _this3.stream = stream;
          _this3.liveVideo.srcObject = stream;
          _this3.clonestream = stream;
          _this3.recordButton.disabled = false;

          _this3.liveVideo.classList.remove("des-content");

          _this3.uploadBtn.classList.add("des-content");

          _this3.secondsSpan.innerHTML = "00";
          _this3.minutesSpan.innerHTML = "0";
          _this3.hoursSpan.innerHTML = "0";

          _this3.startRecording();
        });
      } catch (ex) {
        console.log("Exception happened requesting access to user camera ".concat(ex));
      }
    }
    /**
     * Save the data coming from users media devices camera/mic
     */

  }, {
    key: "startRecording",
    value: function startRecording() {
      this.recordedBlob = [];
      var options = {
        mimeType: "video/webm;codec=ßvp9"
      };

      if (!MediaRecorder.isTypeSupported(options.mimeType)) {
        options = {
          mimeType: "video/webm;codec=vp8"
        };

        if (!MediaRecorder.isTypeSupported(options.mimeType)) {
          options = {
            mimeType: "video/webm"
          };
        }
      }

      try {
        this.mediaRecorder = new MediaRecorder(this.stream, options);
        console.log(this.mediaRecorder, this.stream);
        this.recordButton.textContent = "Stop Recording";
        this.mediaRecorder.start(10); // collect 10ms of data;

        this.mediaRecorder.ondataavailable = this.handleDataAvailable;
        this.startCountDown();
      } catch (ex) {
        console.log("Exception while recording video ".concat(JSON.stringify(ex)));
      }
    }
    /**
     * Save raw data from user's devices to memory
     */

  }, {
    key: "stopRecording",

    /**
     * stop collecting media data from user's device
     */
    value: function stopRecording() {
      this.mediaRecorder.stop();
      this.recordButton.textContent = "Record Again";
      this.recordedVideo.classList.remove("des-content");
      this.uploadBtn.classList.remove("des-content");
      this.liveVideo.classList.add("des-content");
      this.playRecordedVideo();
      this.clearTimer();
      var videotrack = this.stream.getVideoTracks()[0]; // if only one media track

      var audiotrack = this.stream.getAudioTracks()[0]; // if only one media track

      videotrack.stop();
      audiotrack.stop();
    }
    /**
     * display elapsed time 
     */

  }, {
    key: "startCountDown",
    value: function startCountDown() {
      var _this4 = this;

      var startTime = new Date();
      this.timeInterval = setInterval(function () {
        var time = new Date() - startTime;
        var seconds = Math.floor(time / 1000 % 60);
        var munites = Math.floor(time / 1000 / 60 % 60);
        var hours = Math.floor(time / (1000 * 60 * 60) % 24);
        var days = Math.floor(time / (1000 * 60 * 60 * 24));
        _this4.secondsSpan.innerHTML = ('0' + seconds).slice(-2);
        _this4.minutesSpan.innerHTML = munites;
        _this4.hoursSpan.innerHTML = hours;
      }, 1000);
    }
  }, {
    key: "clearTimer",
    value: function clearTimer() {
      clearInterval(this.timeInterval);
    }
    /**
     * return asset that was recorded
     */

  }, {
    key: "getAsset",
    value: function getAsset() {
      if (this.recordedBlob.length > 0) return new Blob(this.recordedBlob, {
        type: "video/webm"
      });
      return null;
    }
  }]);

  return VideoManager;
}();

/***/ }),

/***/ "./src/public/js/response-canvas/page-event-handlers.js":
/*!**************************************************************!*\
  !*** ./src/public/js/response-canvas/page-event-handlers.js ***!
  \**************************************************************/
/*! exports provided: EventHandler */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventHandler", function() { return EventHandler; });
/* harmony import */ var _templates_response_canvas_uploaded_indicator_hbs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../templates/response-canvas/uploaded-indicator.hbs */ "./src/public/js/templates/response-canvas/uploaded-indicator.hbs");
/* harmony import */ var _templates_response_canvas_uploaded_indicator_hbs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_uploaded_indicator_hbs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _templates_response_canvas_upload_indicator_hbs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../templates/response-canvas/upload-indicator.hbs */ "./src/public/js/templates/response-canvas/upload-indicator.hbs");
/* harmony import */ var _templates_response_canvas_upload_indicator_hbs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_upload_indicator_hbs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _templates_response_canvas_validation_msg_hbs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../templates/response-canvas/validation-msg.hbs */ "./src/public/js/templates/response-canvas/validation-msg.hbs");
/* harmony import */ var _templates_response_canvas_validation_msg_hbs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_validation_msg_hbs__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _templates_response_canvas_record_video_hbs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../templates/response-canvas/record-video.hbs */ "./src/public/js/templates/response-canvas/record-video.hbs");
/* harmony import */ var _templates_response_canvas_record_video_hbs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_record_video_hbs__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _templates_response_canvas_take_picture_hbs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../templates/response-canvas/take-picture.hbs */ "./src/public/js/templates/response-canvas/take-picture.hbs");
/* harmony import */ var _templates_response_canvas_take_picture_hbs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_take_picture_hbs__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _templates_response_canvas_header_hbs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../templates/response-canvas/header.hbs */ "./src/public/js/templates/response-canvas/header.hbs");
/* harmony import */ var _templates_response_canvas_header_hbs__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_header_hbs__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _templates_response_canvas_footer_hbs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../templates/response-canvas/footer.hbs */ "./src/public/js/templates/response-canvas/footer.hbs");
/* harmony import */ var _templates_response_canvas_footer_hbs__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_footer_hbs__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _media_manager_picture__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./media-manager/picture */ "./src/public/js/response-canvas/media-manager/picture.js");
/* harmony import */ var _media_manager_video__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./media-manager/video */ "./src/public/js/response-canvas/media-manager/video.js");
/* harmony import */ var _page_utils__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./page-utils */ "./src/public/js/response-canvas/page-utils.js");
/* harmony import */ var _validator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./validator */ "./src/public/js/response-canvas/validator.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../utils */ "./src/public/js/utils/index.js");
/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../core */ "./src/public/js/core/index.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }














var EventHandler =
/*#__PURE__*/
function () {
  function EventHandler(pageView, questions, _formData) {
    var _this = this;

    _classCallCheck(this, EventHandler);

    _defineProperty(this, "handleInput", function (event) {
      var _event$target = event.target,
          id = _event$target.id,
          value = _event$target.value;

      var nextButtonWrapper = _this.pageView.querySelector("div.nextButtonWrapper[id=\"".concat(id, "\"]"));

      if (nextButtonWrapper) {
        if (value) {
          nextButtonWrapper.classList.remove("inactiveNextButtonWrapper");
        } else {
          nextButtonWrapper.classList.add("inactiveNextButtonWrapper");
        }
      }

      _this.addAnswer(id, value);
    });

    _defineProperty(this, "handleFileDrop", function (e) {
      e.preventDefault();
      var data = e.target.dataset;

      _this.handleStaticAssetUpload(e.dataTransfer.files[0], data.qType, dataqId);
    });

    _defineProperty(this, "expandDropDownList", function (event) {
      var id = event.target.dataset.qId;

      var node = _this.pageView.querySelector("div.dropdownOptions[data-q-id=\"".concat(id, "\"]"));

      if (node.classList.contains("inactiveWrapper")) {
        node.classList.remove("inactiveWrapper");
        node.classList.add("activeWrapper");
      }
    });

    _defineProperty(this, "filterDropdownOptions", function (event) {
      var _event$target2 = event.target,
          id = _event$target2.id,
          value = _event$target2.value;

      var question = _this.questions.find(function (el) {
        return el.id === id;
      });

      var type = question.type,
          children = question.children;
      var options = [];

      if (type === "branch") {
        options = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionsFromBranches(children, value);
      } else {
        options = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionFromArray(children, value);
      }

      var template = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].buildOptionTemplate(options, id, type);
      var optionsNode = Object(_utils__WEBPACK_IMPORTED_MODULE_11__["parseHtml"])(template);

      var optionContainer = _this.pageView.querySelector("div.optionsBox[data-q-id=\"".concat(id, "\"]"));

      _this.addOptionListeners(optionsNode, id);

      optionContainer.innerHTML = "";
      optionContainer.appendChild(optionsNode);
    });

    _defineProperty(this, "toggleDropDownList", function (event) {
      var id = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      if (!id) {
        id = event.target.dataset.qId;
      }

      var node = _this.pageView.querySelector("div.dropdownOptions[data-q-id=\"".concat(id, "\"]"));

      if (node.classList.contains("inactiveWrapper")) {
        node.classList.remove("inactiveWrapper");
        node.classList.add("activeWrapper");
      } else {
        node.classList.add("inactiveWrapper");
        node.classList.remove("activeWrapper");
      }
    });

    _defineProperty(this, "handleOptionSelection", function (event) {
      var node = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getRootNode(event.target);
      var type = node.dataset.optionType;
      var id = node.dataset.optionId;

      if (id) {
        var allOptions = _this.pageView.querySelectorAll("div[data-option-id=\"".concat(id, "\"]"));

        allOptions.forEach(function (option) {
          var checkIconBox = option.querySelector("div.iconWrapper");

          if (checkIconBox.classList.contains("activIconWrapper")) {
            checkIconBox.classList.add("inactivIconWrapper");
            checkIconBox.classList.remove("activIconWrapper");
          }
        });
        var picked = node.querySelector("div.iconWrapper");
        var pickedTextBox = node.querySelector("div.text");
        picked.classList.remove("inactivIconWrapper");
        picked.classList.add("activIconWrapper");

        _this.addAnswer(id, pickedTextBox.textContent);

        _this.toggleErrorUIFor(id, []);

        _this.toggleSubmitErrorUI(false);

        _this.updateHeader(id);

        _this.updateFooter();

        if (type === "dropdown") {
          var input = _this.pageView.querySelector("input[data-q-id=\"".concat(id, "\"]"));

          input.value = pickedTextBox.textContent;

          _this.toggleDropDownList(null, id);
        }

        _this.goToNextQuestion(id, "down");

        _this.updateFocus(id);
      }
    });

    _defineProperty(this, "askMediaPermission", function (ev) {
      var id = ev.currentTarget.id;
      var qtype = ev.currentTarget.dataset.qtype;

      var componentParent = _this.pageView.querySelector("div.mediaRecorder[id=\"".concat(id, "\"]"));

      var component = componentParent.querySelector("div.answerWrapper");
      component.innerHTML = "";

      if (qtype === "video") {
        var newNodeTree = Object(_utils__WEBPACK_IMPORTED_MODULE_11__["parseHtml"])(_templates_response_canvas_record_video_hbs__WEBPACK_IMPORTED_MODULE_3___default()());
        var videoManager = new _media_manager_video__WEBPACK_IMPORTED_MODULE_8__["VideoManager"](newNodeTree, id); //        videoManager = recordVideo.stop();    

        component.appendChild(newNodeTree);

        _this.videoManagers.set(id, videoManager);
      } else if (qtype === "picture") {
        var _newNodeTree = Object(_utils__WEBPACK_IMPORTED_MODULE_11__["parseHtml"])(_templates_response_canvas_take_picture_hbs__WEBPACK_IMPORTED_MODULE_4___default()());

        var pictureManager = new _media_manager_picture__WEBPACK_IMPORTED_MODULE_7__["PictureManager"](_newNodeTree, id);
        component.appendChild(_newNodeTree);

        _this.pictureManagers.set(id, pictureManager);
      }
    });

    _defineProperty(this, "goToNextQuestion", function (currentQuestionId) {
      var direction = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "down";
      // Todo **************************************
      // logic should change if user is scrolling up
      var nextQuestion = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getNextQuestion(_this.questions, currentQuestionId);

      var canvas = _this.pageView.querySelector(".canvas");

      var currentQuestionUI = _this.pageView.querySelector(".elementWrapper[data-q-id=\"".concat(currentQuestionId, "\"]"));

      var distanceToScroll = 237;

      if (nextQuestion) {
        var nextQuestionUI = _this.pageView.querySelector(".elementWrapper[data-q-id=\"".concat(nextQuestion.id, "\"]"));

        var distanceToCurrentQ = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getDistanceToTop(currentQuestionUI);
        var distanceToNextQ = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getDistanceToTop(nextQuestionUI);
        distanceToScroll = distanceToNextQ - distanceToCurrentQ;
      }

      if (direction === "down") {
        canvas.scrollBy(0, distanceToScroll);
      } else if (direction === "up") {
        canvas.scrollBy(0, -distanceToScroll);
      }
    });

    _defineProperty(this, "handleStaticAssetUpload", function (file, type, id) {
      console.log(file);

      var uploadIndicator = _this.pageView.querySelector("div.uploadstatus[data-q-id=\"".concat(id, "\"]"));

      var templateOption = {
        imgUrl: "/img/uploading.svg",
        labelText: "Uploading...",
        id: id
      };
      var template = _templates_response_canvas_upload_indicator_hbs__WEBPACK_IMPORTED_MODULE_1___default()(templateOption);
      uploadIndicator.innerHTML = "";
      uploadIndicator.appendChild(Object(_utils__WEBPACK_IMPORTED_MODULE_11__["parseHtml"])(template));

      _this.uploadFile(file, type, id).then(function (res) {
        templateOption = {
          imgUrl: res.assetUrl,
          labelText: "Change File",
          id: id
        };
        template = _templates_response_canvas_uploaded_indicator_hbs__WEBPACK_IMPORTED_MODULE_0___default()(templateOption);
        uploadIndicator.innerHTML = "";
        uploadIndicator.appendChild(Object(_utils__WEBPACK_IMPORTED_MODULE_11__["parseHtml"])(template));

        _this.addAnswer(id, res.assetUrl);

        _this.processAnswer(null, id, res.assetUrl);
      })["catch"](function (err) {
        alert("".concat(type, " upload faild please try again."));
      });
    });

    _defineProperty(this, "handleGeneratedAssetUpload", function (ev) {
      var span = ev.currentTarget.querySelector(".nextButtonText");
      span.innerHTML = "Uploading...";
      var id = ev.currentTarget.id;
      var qtype = ev.currentTarget.dataset.qtype;
      var assetManager = null;
      var name = null;
      var errorMessage = null;

      if (qtype === "video") {
        assetManager = _this.videoManagers.get(id);
        errorMessage = "You have not recorded any video yet.";
        name = "video.webm";
      } else if (qtype === "picture") {
        assetManager = _this.pictureManagers.get(id);
        name = "image.png";
        errorMessage = "You have not taken any picture yet.";
      }

      if (assetManager) {
        var asset = assetManager.getAsset();

        if (asset) {
          _this.uploadFile(asset, qtype, name).then(function (res) {
            span.innerHTML = "Upload";

            _this.addAnswer(id, res.assetUrl);

            _this.processAnswer(null, id, res.assetUrl);
          })["catch"](function (err) {
            console.log(err);
            alert("".concat(qtype, " upload faild please try again."));
          });
        } else {
          alert(errorMessage);
        }
      }
    });

    _defineProperty(this, "uploadFile", function (file, type) {
      var name = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

      var _PageUtils$getDefault = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getDefaultRespondant(),
          firstname = _PageUtils$getDefault.firstname,
          lastname = _PageUtils$getDefault.lastname;

      type = String("".concat(type, "s")).toLowerCase();
      var fullname = "".concat(firstname).concat(lastname);
      var bankslug = _this.pageView.dataset.bankslug;
      var url = "upload/".concat(type, "/").concat(bankslug, "/").concat(fullname, "_").concat(type);
      var formData = new FormData();

      if (name) {
        formData.append("asset", file, name);
      } else {
        formData.append("asset", file);
      }

      return _core__WEBPACK_IMPORTED_MODULE_12__["Http"].upLoadForm(url, formData);
    });

    _defineProperty(this, "processAnswer", function (event, id, value) {
      if (!id) {
        // process answer only when a user press enter while interacting with an input element
        var target = event.target;
        if (event.shiftKey && event.key === "Enter") return;
        if (event.key && event.key !== "Enter") return;

        if (event.key && event.key === "Enter") {
          target = event.target;
        } // ensure that the event target is an input element


        if (target.nodeName === "SPAN" || target.nodeName === "BUTTON") {
          target = _this.pageView.querySelector(".input[data-q-id=\"".concat(target.id, "\"]"));
        }

        value = target.value;
        id = target.id;
      }

      var hasError = _this.validateAnswer(value, id);

      if (!hasError) {
        _this.updateHeader(id);

        _this.updateFooter();

        _this.goToNextQuestion(id, "down", 340);
      }

      _this.updateFocus(id);
    });

    _defineProperty(this, "validateAnswer", function (answer, questionId) {
      var question = _this.questions.find(function (q) {
        return q.id === questionId;
      });

      var validationRules = question.validationRules,
          type = question.type;
      if (type === "statement") return;
      var result = _validator__WEBPACK_IMPORTED_MODULE_10__["Validator"].validate(validationRules, type, answer);
      var hasError = result.hasError,
          messages = result.messages;

      _this.toggleErrorUIFor(questionId, messages, hasError);

      _this.toggleSubmitErrorUI(hasError);

      return result.hasError;
    });

    this.questions = questions;
    this.formData = _formData;
    this.pageView = pageView;
    this.pictureManagers = new Map();
    this.videoManagers = new Map();
    this.answers = [];
  }
  /**
   * collect text entered into an input element by a user
   * @param {InputEvent} event the input event
   */


  _createClass(EventHandler, [{
    key: "highlightDropArea",

    /**
     * Give a visual cue to user when they drag a object around an area where files
     * can be dropped for uploading
     * @param {DragEvent} event
     */
    value: function highlightDropArea(event) {
      event.target.classList.add("highlightArea");
    }
    /**
     * Give a visual cue to user when they drag a object out of an area where files
     * can be dropped for uploading
     *@param {DragEvent} event
     */

  }, {
    key: "unhighlightDropArea",
    value: function unhighlightDropArea(event) {
      event.target.classList.remove("highlightArea");
    }
    /**
     * Handle object drop event in a region that allow drag and drop
     * @param {DropEvent} e
     */

  }, {
    key: "addOptionListeners",

    /**
     *  Register an event listener that monitors when a user clicks an option in a multi
     * option interaction element
     * @param {documentFragment} node Document Object containing input elements for user
     * to interact with
     * @param {string} id  id of the interaction element whose option the user clicked
     * on
     */
    value: function addOptionListeners(node, id) {
      var _this2 = this;

      var options = node.querySelectorAll("div[data-option-id=\"".concat(id, "\"]"));

      if (options.length) {
        options.forEach(function (option) {
          return option.onclick = _this2.handleOptionSelection;
        });
      }
    }
    /**
     * Handle user option selection, add check icon on the select option
     * @param {mouseEvent} event the clicked event
     */

  }, {
    key: "addAnswer",

    /**
     * add users answer to a question to the list of answers
     * @param {string} questionId
     * @param {string} answerText text entered by user or file url from s3 bucket
     */
    value: function addAnswer(questionId, answerText) {
      var question = this.questions.find(function (el) {
        return el.id === questionId;
      });
      var answer = {
        questionType: question.type,
        question: question.name,
        questionId: question.id,
        answer: answerText
      };
      var existingIndex = this.answers.findIndex(function (res) {
        return res.question === answer.question;
      });

      if (existingIndex >= 0) {
        this.answers[existingIndex] = answer;
      } else {
        this.answers.push(answer);
      }
    }
    /**
     * Show/Hid UI with error messages for a question with wrong answer
     * @param {string} questionId id of question whose answer failed validation
     * @param {array} messages messages to display to user
     */

  }, {
    key: "toggleErrorUIFor",
    value: function toggleErrorUIFor(questionId) {
      var messages = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
      var msgContainer = this.pageView.querySelector("[data-validation-msg-for=\"".concat(questionId, "\"]"));

      if (messages.length) {
        msgContainer.classList.remove("inactiveValidation");
        var msgString = _templates_response_canvas_validation_msg_hbs__WEBPACK_IMPORTED_MODULE_2___default()({
          messages: messages
        });
        var oldNode = msgContainer.firstChild;
        var msgNode = Object(_utils__WEBPACK_IMPORTED_MODULE_11__["parseHtml"])(msgString);
        msgContainer.replaceChild(msgNode, oldNode);
      } else {
        if (!msgContainer.classList.contains("inactiveValidation")) {
          msgContainer.classList.add("inactiveValidation");
        }
      }
    }
    /**
     * Show error message to user if any question's  answer does not pass all validation rules
     * @param {bool} haveError
     */

  }, {
    key: "toggleSubmitErrorUI",
    value: function toggleSubmitErrorUI(haveError) {
      var msgContainer = this.pageView.querySelector("#submit-error-msg");

      if (haveError) {
        msgContainer.classList.remove("inactiveValidation");
        var messages = [//          "Scroll up to fix problems with some of your answers",
        "Scroll up to fix problems with some of your answers"];
        var msgString = _templates_response_canvas_validation_msg_hbs__WEBPACK_IMPORTED_MODULE_2___default()({
          messages: messages
        });
        var oldNode = msgContainer.firstChild;
        var msgNode = Object(_utils__WEBPACK_IMPORTED_MODULE_11__["parseHtml"])(msgString);
        msgContainer.replaceChild(msgNode, oldNode);
      } else {
        if (!msgContainer.classList.contains("inactiveValidation")) {
          msgContainer.classList.add("inactiveValidation");
        }
      }
    }
    /**
     * update the section information on the canvas header
     * @param {number} currentQuestionId
     */

  }, {
    key: "updateHeader",
    value: function updateHeader(currentQuestionId) {
      var sectionData = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getNextSectionData(this.formData.elements, currentQuestionId);

      if (sectionData) {
        var headerString = _templates_response_canvas_header_hbs__WEBPACK_IMPORTED_MODULE_5___default()(sectionData);
        var newHeaderNode = Object(_utils__WEBPACK_IMPORTED_MODULE_11__["parseHtml"])(headerString);
        var oldHeaderNode = this.pageView.querySelector("section.canvas__headerSection");
        this.pageView.replaceChild(newHeaderNode, oldHeaderNode);
      }
    }
    /**
    * Replace old copy of footer node with a new copy that
    * reflect the current state of the application
    */

  }, {
    key: "updateFooter",
    value: function updateFooter() {
      this.toggleFooterForMobile(false);
      var total = this.questions.filter(function (question) {
        return question.type !== "statement";
      }).length;
      var totalAnswered = this.answers.length;
      var footerString = _templates_response_canvas_footer_hbs__WEBPACK_IMPORTED_MODULE_6___default()({
        total: total,
        totalAnswered: totalAnswered
      });
      var newFooterNode = Object(_utils__WEBPACK_IMPORTED_MODULE_11__["parseHtml"])(footerString);
      this.addNavigationListeners(newFooterNode);
      var oldFooterNode = this.pageView.querySelector("section.canvas__footerSection");
      this.pageView.replaceChild(newFooterNode, oldFooterNode);
    }
    /**
     * Register click event handler on navigation button
     *  @param {documentFragment} node Document Object containing input elements for user
     * to interact with
     */

  }, {
    key: "addNavigationListeners",
    value: function addNavigationListeners(node) {
      var _this3 = this;

      var upBtn = node.querySelector("button[id=\"navigateUP\"]");
      var downBtn = node.querySelector("button[id=\"navigateDown\"]");

      if (upBtn && downBtn) {
        upBtn.onclick = function () {
          var questionUI = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getCurrentQuestionUI();

          _this3.goToNextQuestion(questionUI.dataset.qId, "up");
        };

        downBtn.onclick = function () {
          var questionUI = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getCurrentQuestionUI();

          _this3.goToNextQuestion(questionUI.dataset.qId, "down");
        };
      }
    }
    /**
    * Bring a new question into user's focus
    * @param {string} currentQuestionId id of the question user has just answered
    * @param direction direction to scroll to get to the next question
    */

  }, {
    key: "updateFocus",

    /**
     * Put focus on the next question for user to answer
     * @param {string} questionId id of the question a user just finished answering
     */
    value: function updateFocus(questionId) {
      var firstQuestion = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      var nextQuestion = _page_utils__WEBPACK_IMPORTED_MODULE_9__["PageUtils"].getNextQuestion(this.questions, questionId);
      if (!nextQuestion && firstQuestion === false) return;
      var id = questionId;

      if (!firstQuestion) {
        id = nextQuestion.id;
      }

      var input = this.pageView.querySelector(".input[data-q-id=\"".concat(id, "\"]"));

      if (input) {
        input.focus();
        this.toggleFooterForMobile(true);
      }
    }
    /**
     * handle uploading static assets like passport and signature
     */

  }, {
    key: "toggleFooterForMobile",

    /**
     * show/hid progress on the footer depending on the kind of question
     * a user is answering while using a mobile phone
     * @param {Boolean} show
     */
    value: function toggleFooterForMobile() {
      var show = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      var footer = this.pageView.querySelector("section.canvas__footerSection");

      if (show && !footer.classList.contains("des-content")) {
        footer.classList.add("des-content");
      }

      if (!show && footer.classList.contains("des-content")) {
        footer.classList.remove("des-content");
      }
    }
    /**
     * Observes when users click the ok button on each input based interaction elements
     * @param {MouseEvent} event
     * to signify that they have finished answering the question
     */

  }, {
    key: "getAnswers",

    /**
     * return all the answer given by a use4r
     */
    value: function getAnswers() {
      return this.answers;
    }
  }]);

  return EventHandler;
}();

/***/ }),

/***/ "./src/public/js/response-canvas/page-ui-builder.js":
/*!**********************************************************!*\
  !*** ./src/public/js/response-canvas/page-ui-builder.js ***!
  \**********************************************************/
/*! exports provided: PageUIBuilder */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageUIBuilder", function() { return PageUIBuilder; });
/* harmony import */ var _templates_response_canvas_multi_choice_question_hbs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../templates/response-canvas/multi-choice-question.hbs */ "./src/public/js/templates/response-canvas/multi-choice-question.hbs");
/* harmony import */ var _templates_response_canvas_multi_choice_question_hbs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_multi_choice_question_hbs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _templates_response_canvas_upload_indicator_hbs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../templates/response-canvas/upload-indicator.hbs */ "./src/public/js/templates/response-canvas/upload-indicator.hbs");
/* harmony import */ var _templates_response_canvas_upload_indicator_hbs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_upload_indicator_hbs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _templates_response_canvas_media_permission_hbs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../templates/response-canvas/media-permission.hbs */ "./src/public/js/templates/response-canvas/media-permission.hbs");
/* harmony import */ var _templates_response_canvas_media_permission_hbs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_media_permission_hbs__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _templates_response_canvas_dropdown_question_hbs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../templates/response-canvas/dropdown-question.hbs */ "./src/public/js/templates/response-canvas/dropdown-question.hbs");
/* harmony import */ var _templates_response_canvas_dropdown_question_hbs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_dropdown_question_hbs__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _templates_response_canvas_simple_question_hbs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../templates/response-canvas/simple-question.hbs */ "./src/public/js/templates/response-canvas/simple-question.hbs");
/* harmony import */ var _templates_response_canvas_simple_question_hbs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_simple_question_hbs__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _templates_response_canvas_statement_hbs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../templates/response-canvas/statement.hbs */ "./src/public/js/templates/response-canvas/statement.hbs");
/* harmony import */ var _templates_response_canvas_statement_hbs__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_statement_hbs__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _templates_response_canvas_long_question_hbs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../templates/response-canvas/long-question.hbs */ "./src/public/js/templates/response-canvas/long-question.hbs");
/* harmony import */ var _templates_response_canvas_long_question_hbs__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_long_question_hbs__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _templates_response_canvas_picture_hbs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../templates/response-canvas/picture.hbs */ "./src/public/js/templates/response-canvas/picture.hbs");
/* harmony import */ var _templates_response_canvas_picture_hbs__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_picture_hbs__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _page_utils__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./page-utils */ "./src/public/js/response-canvas/page-utils.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }










/**
 * transform form data into interactive UI
 */

var PageUIBuilder = function PageUIBuilder() {
  _classCallCheck(this, PageUIBuilder);
};
/**
  * Get the right input type property
  * @param {string} el 
  */

_defineProperty(PageUIBuilder, "buildUIFor", function (questionData) {
  var validationRules = questionData.validationRules;
  var option = {
    isRequired: confirmRequirement(validationRules),
    placeholder: generatePlaceholder(questionData),
    type: generateType(questionData),
    description: questionData.description,
    position: questionData.qPosition,
    children: questionData.children,
    question: questionData.name,
    uploadIndicator: "",
    id: questionData.id,
    optionsString: ""
  };
  var options = null;

  switch (questionData.type) {
    case "introduction":
      return null;

    case "section":
      return null;

    case "statement":
      return _templates_response_canvas_statement_hbs__WEBPACK_IMPORTED_MODULE_5___default()(option);

    case "picture":
      option.text = "Turn on Camera";
      return _templates_response_canvas_media_permission_hbs__WEBPACK_IMPORTED_MODULE_2___default()(option);

    case "video":
      option.text = "Start Recording";
      return _templates_response_canvas_media_permission_hbs__WEBPACK_IMPORTED_MODULE_2___default()(option);

    case "signature":
    case "passport":
      option.labelText = "Or Click to upload";
      option.uploadIndicator = _templates_response_canvas_upload_indicator_hbs__WEBPACK_IMPORTED_MODULE_1___default()(option);
      return _templates_response_canvas_picture_hbs__WEBPACK_IMPORTED_MODULE_7___default()(option);

    case "multichoice":
      options = _page_utils__WEBPACK_IMPORTED_MODULE_8__["PageUtils"].buildOptionFromArray(option.children);
      option.optionsString = _page_utils__WEBPACK_IMPORTED_MODULE_8__["PageUtils"].buildOptionTemplate(options, option.id, option.type);
      return _templates_response_canvas_multi_choice_question_hbs__WEBPACK_IMPORTED_MODULE_0___default()(option);

    case "dropdown":
      options = _page_utils__WEBPACK_IMPORTED_MODULE_8__["PageUtils"].buildOptionFromArray(option.children);
      option.optionsString = _page_utils__WEBPACK_IMPORTED_MODULE_8__["PageUtils"].buildOptionTemplate(options, option.id, option.type);
      return _templates_response_canvas_dropdown_question_hbs__WEBPACK_IMPORTED_MODULE_3___default()(option);

    case "branch":
      options = _page_utils__WEBPACK_IMPORTED_MODULE_8__["PageUtils"].buildOptionsFromBranches(option.children);
      option.optionsString = _page_utils__WEBPACK_IMPORTED_MODULE_8__["PageUtils"].buildOptionTemplate(options, option.id, option.type);
      return _templates_response_canvas_dropdown_question_hbs__WEBPACK_IMPORTED_MODULE_3___default()(option);

    case "creditcards":
      options = generateCardOptions();
      option.optionsString = _page_utils__WEBPACK_IMPORTED_MODULE_8__["PageUtils"].buildOptionTemplate(options, option.id, option.type);
      return _templates_response_canvas_multi_choice_question_hbs__WEBPACK_IMPORTED_MODULE_0___default()(option);

    case "yesorno":
      options = generateYesOrNoOptions();
      option.optionsString = _page_utils__WEBPACK_IMPORTED_MODULE_8__["PageUtils"].buildOptionTemplate(options, option.id, option.type);
      return _templates_response_canvas_multi_choice_question_hbs__WEBPACK_IMPORTED_MODULE_0___default()(option);

    case "gender":
      options = generateGenderOptions();
      option.optionsString = _page_utils__WEBPACK_IMPORTED_MODULE_8__["PageUtils"].buildOptionTemplate(options, option.id, option.type);
      return _templates_response_canvas_multi_choice_question_hbs__WEBPACK_IMPORTED_MODULE_0___default()(option);

    case "longtext":
      return _templates_response_canvas_long_question_hbs__WEBPACK_IMPORTED_MODULE_6___default()(option);

    default:
      return _templates_response_canvas_simple_question_hbs__WEBPACK_IMPORTED_MODULE_4___default()(option);
  }
});

var generateType = function generateType(el) {
  switch (el.type) {
    case "shorttext":
    case "firstname":
    case "lastname":
    case "address":
      return "text";

    case "date":
    case "dob":
      return "date";

    case "tel":
      return "tel";

    case "email":
      return "email";

    case "mobile":
    case "tel":
    case "bvn":
      return "number";

    default:
      return el.type;
  }
};
/**
 * Return the right input placeholver property
 * @param {obje3ct} el 
 */


var generatePlaceholder = function generatePlaceholder(el) {
  switch (el.type) {
    case "address":
      return "Like 16 Karimu Ikotun VI, Lagos";

    case "mobile":
      return "Like 08136868448";

    case "tel":
      return "Like 01729011";

    case "email":
      return "Like jendoe@cool.com";

    case "bvn":
      return "Like 22123803000";

    case "dropdown":
      return "Start typing to filter options";

    default:
      return "Enter Your Answer Here";
  }
};
/**
 * return array of options to display for card questions
 */


var generateCardOptions = function generateCardOptions() {
  return [{
    label: "A",
    text: "Master"
  }, {
    label: "B",
    text: "Valve"
  }, {
    label: "C",
    text: "Visa"
  }];
};
/**
 * return array of options to display for yes/no questions
 */


var generateYesOrNoOptions = function generateYesOrNoOptions() {
  return [{
    label: "Y",
    text: "Yes"
  }, {
    label: "N",
    text: "No"
  }];
};
/**
 * return array of options to display for yes/no questions
 */


var generateGenderOptions = function generateGenderOptions() {
  return [{
    label: "M",
    text: "Male"
  }, {
    label: "F",
    text: "Female"
  }];
};
/**
 * check if a question has required as part of its validation rules
 * @param {object} validationRules 
 */


var confirmRequirement = function confirmRequirement(validationRules) {
  var requiredRule = validationRules.find(function (rule) {
    return rule.name === "required";
  });
  return requiredRule !== undefined;
};

/***/ }),

/***/ "./src/public/js/response-canvas/page-utils.js":
/*!*****************************************************!*\
  !*** ./src/public/js/response-canvas/page-utils.js ***!
  \*****************************************************/
/*! exports provided: PageUtils */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageUtils", function() { return PageUtils; });
/* harmony import */ var _templates_response_canvas_dropdown_options_hbs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../templates/response-canvas/dropdown-options.hbs */ "./src/public/js/templates/response-canvas/dropdown-options.hbs");
/* harmony import */ var _templates_response_canvas_dropdown_options_hbs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_dropdown_options_hbs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _templates_response_canvas_option_hbs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../templates/response-canvas/option.hbs */ "./src/public/js/templates/response-canvas/option.hbs");
/* harmony import */ var _templates_response_canvas_option_hbs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_option_hbs__WEBPACK_IMPORTED_MODULE_1__);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



/**
 * used to for building label for question with options
 */

var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var PageUtils =
/*#__PURE__*/
function () {
  function PageUtils() {
    _classCallCheck(this, PageUtils);
  }

  _createClass(PageUtils, null, [{
    key: "getQuestions",

    /**
     * Extract out form element that need users interaction
     * @param {array} formInputs form elements
     */
    value: function getQuestions(formInputs) {
      return formInputs.filter(function (element) {
        return element.type !== "section" && element.type !== "introduction";
      }).map(function (question, index) {
        question.qPosition = index + 1;
        return question;
      });
    }
    /**
     * check if a form has intro section
     * @param {*} formInputs form elements
     */

  }, {
    key: "containsIntro",
    value: function containsIntro(formInputs) {
      var intro = formInputs.find(function (el) {
        return el.type === "introduction";
      });
      if (intro) return true;
      return false;
    }
    /**
     * pick the ids of all form elements that needs user's interaction
     * @param {*} questions 
     */

  }, {
    key: "pickIds",
    value: function pickIds(questions) {
      return questions.map(function (el) {
        return el.id;
      });
    }
    /**
    * get the intro section of a form
    * @param {*} formInputs 
    */

  }, {
    key: "getIntroData",
    value: function getIntroData(formInputs) {
      return formInputs.find(function (el) {
        return el.type === "introduction";
      });
    }
    /**
     * extract the first section data to display on the page header
     */

  }, {
    key: "getFirstSection",
    value: function getFirstSection(formQuestions) {
      return formQuestions.slice(0, 3).find(function (question) {
        return question.type === "section";
      });
    }
    /**
     * Extract the next section data to display on the page header
     *  @param {*} formInputs 
     */

  }, {
    key: "getNextSection",
    value: function getNextSection(formQuestions, currentQuestionId) {
      var currentQuestionIndex = formQuestions.findIndex(function (question) {
        return question.id === currentQuestionId;
      });
      var nextQuestion = formQuestions[currentQuestionIndex + 1];
      return (nextQuestion && nextQuestion.type) === "section" ? nextQuestion : null;
    }
    /**
      * return the parent node for document fragment acting as a question option
      * that recieved a click event
      * @param {DocumentNode} node 
      */

  }, {
    key: "getRootNode",
    value: function getRootNode(node) {
      while (node.dataset.optionId === undefined && node.nodeName !== "SECTION") {
        node = node.parentElement;
      }

      return node;
    }
    /**
     * transform array of text to arry of option object
     * @param {array} array array with text to be transformed into option object
     * @param {string} filterText value by which array element would be filted
     */

  }, {
    key: "buildOptionFromArray",
    value: function buildOptionFromArray(array) {
      var filterText = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

      if (filterText) {
        array = array.filter(function (item) {
          return item.toLowerCase().indexOf(filterText.toLowerCase()) !== -1;
        });
      }

      return array.map(function (el, index) {
        return {
          label: alphabet[index],
          text: el
        };
      });
    }
  }, {
    key: "buildOptionsFromBranches",

    /**
     * Transform the data in a branch list to option objects
     * @param {Array} branches list of a bank branch
     * @param {string} filterText value by which array element would be filted
     */
    value: function buildOptionsFromBranches(branches) {
      var filterText = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      branches = branches.map(function (branch) {
        return "".concat(branch.name, " | ").concat(branch.address);
      });

      if (filterText) {
        branches = branches.filter(function (branch) {
          return branch.toLowerCase().indexOf(filterText.toLowerCase()) !== -1;
        });
      }

      return branches.map(function (text, index) {
        return {
          label: alphabet[index],
          text: text
        };
      });
    }
    /**
      * Build UI for each multi choice question's option
      * @param {array} options option object
      * @param {string} questionId id of question
      * @param {boolean} questionType type of question e.g dropdown/card etc.
      */

  }, {
    key: "buildOptionTemplate",
    value: function buildOptionTemplate() {
      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      var questionId = arguments.length > 1 ? arguments[1] : undefined;
      var questionType = arguments.length > 2 ? arguments[2] : undefined;

      if (questionType === "dropdown" || questionType === "branch") {
        return options.map(function (_ref) {
          var text = _ref.text;
          return _templates_response_canvas_dropdown_options_hbs__WEBPACK_IMPORTED_MODULE_0___default()({
            text: text,
            id: questionId
          });
        }).join("");
      }

      return options.map(function (_ref2) {
        var label = _ref2.label,
            text = _ref2.text;
        return _templates_response_canvas_option_hbs__WEBPACK_IMPORTED_MODULE_1___default()({
          label: label,
          text: text,
          id: questionId
        });
      }).join("");
    }
    /**
     * Return the next data to be be displayed on the page header
     */

  }, {
    key: "getNextSectionData",
    value: function getNextSectionData(formQuestions, currentQuestionId) {
      var currentQuestionIndex = formQuestions.findIndex(function (question) {
        return question.id === currentQuestionId;
      });
      var nextQuestion = formQuestions[currentQuestionIndex + 1];
      return (nextQuestion && nextQuestion.type) === "section" ? nextQuestion : null;
    }
    /**
     * return the next question in the series
     * @param {array} formQuestions 
     * @param {string} currentQuestionId 
     */

  }, {
    key: "getNextQuestion",
    value: function getNextQuestion(formQuestions, currentQuestionId) {
      var currentQuestionIndex = formQuestions.findIndex(function (question) {
        return question.id === currentQuestionId;
      });
      return formQuestions[currentQuestionIndex + 1];
    }
    /**
     * get the distance between the top of the canvas to an element
     * @param {DocumentNode} elem 
     */

  }, {
    key: "getDistanceToTop",
    value: function getDistanceToTop(elem) {
      var distance = 0;

      if (elem.offsetParent) {
        do {
          distance += elem.offsetTop;
          elem = elem.offsetParent;
        } while (elem);
      }

      return distance < 0 ? 0 : distance;
    }
    /**
     * get the branch a user want their answers sent to
     * @param {array} answers all the answers a user has given
     */

  }, {
    key: "getBranchData",
    value: function getBranchData(answers) {
      var branch = answers.find(function (answer) {
        return answer.questionType === "branch";
      });
      return branch ? removeAddresspart(branch.answer) : "HQ";
    }
    /**
     * return the node of the current question a user is answering
     */

  }, {
    key: "getCurrentQuestionUI",
    value: function getCurrentQuestionUI() {
      return Array.from(document.querySelectorAll(".elementWrapper")).find(function (node) {
        return !node.classList.contains("dull");
      });
    }
    /**
     * since we are not collect users data this is our default form respondant value for now
     */

  }, {
    key: "getDefaultRespondant",
    value: function getDefaultRespondant() {
      return {
        id: "5b51e1706fa9c80029476871",
        firstname: "ThankGod",
        lastname: "Ossaija",
        phone: "08136868448"
      };
    }
  }]);

  return PageUtils;
}();

var removeAddresspart = function removeAddresspart(branchAnswer) {
  var index = branchAnswer.indexOf(" ");
  return branchAnswer.substring(0, index);
};

/***/ }),

/***/ "./src/public/js/response-canvas/response-canvas.js":
/*!**********************************************************!*\
  !*** ./src/public/js/response-canvas/response-canvas.js ***!
  \**********************************************************/
/*! exports provided: ResponseCanvas */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ResponseCanvas", function() { return ResponseCanvas; });
/* harmony import */ var _templates_response_canvas_submit_hbs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../templates/response-canvas/submit.hbs */ "./src/public/js/templates/response-canvas/submit.hbs");
/* harmony import */ var _templates_response_canvas_submit_hbs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_submit_hbs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _templates_response_canvas_header_hbs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../templates/response-canvas/header.hbs */ "./src/public/js/templates/response-canvas/header.hbs");
/* harmony import */ var _templates_response_canvas_header_hbs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_header_hbs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _templates_response_canvas_footer_hbs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../templates/response-canvas/footer.hbs */ "./src/public/js/templates/response-canvas/footer.hbs");
/* harmony import */ var _templates_response_canvas_footer_hbs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_footer_hbs__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _templates_response_canvas_canvas_hbs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../templates/response-canvas/canvas.hbs */ "./src/public/js/templates/response-canvas/canvas.hbs");
/* harmony import */ var _templates_response_canvas_canvas_hbs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_canvas_hbs__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _templates_response_canvas_intro_hbs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../templates/response-canvas/intro.hbs */ "./src/public/js/templates/response-canvas/intro.hbs");
/* harmony import */ var _templates_response_canvas_intro_hbs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_templates_response_canvas_intro_hbs__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../utils */ "./src/public/js/utils/index.js");
/* harmony import */ var _page_event_handlers__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./page-event-handlers */ "./src/public/js/response-canvas/page-event-handlers.js");
/* harmony import */ var _page_ui_builder__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./page-ui-builder */ "./src/public/js/response-canvas/page-ui-builder.js");
/* harmony import */ var _core_response_canvas__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../core/response-canvas */ "./src/public/js/core/response-canvas/index.js");
/* harmony import */ var _core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../core */ "./src/public/js/core/index.js");
/* harmony import */ var _page_utils__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./page-utils */ "./src/public/js/response-canvas/page-utils.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }












/**
 * Manages the response page where user are given oppurtunity to provide data
 * to banks by interacting with various UI elements
 */

var ResponseCanvas =
/*#__PURE__*/
function () {
  function ResponseCanvas(_container) {
    var _this = this;

    _classCallCheck(this, ResponseCanvas);

    _defineProperty(this, "handleGettingSetarted", function () {
      _this.startFormFilling = true;

      _this.presentView();
    });

    _defineProperty(this, "targetEnterCanvas", function (res) {
      var element = res.element;
      var container = element.querySelector("section");
      element.classList.remove("dull");
      container.classList.remove("inactiveElement");
      container.classList.add("activeElement");
    });

    _defineProperty(this, "targetExitCanvas", function (res) {
      var element = res.element;
      var container = element.querySelector("section");
      element.classList.add("dull");
      container.classList.remove("activeElement");
      container.classList.add("inactiveElement");
    });

    _defineProperty(this, "processResponse", function () {
      var haveError = false;

      var answers = _this.eventHandler.getAnswers();

      _this.questions.forEach(function (question) {
        var response = answers.find(function (r) {
          return r.questionId === question.id;
        });
        var answer = response ? response.answer : "";

        var notValid = _this.eventHandler.validateAnswer(answer, question.id);

        if (notValid) haveError = true;
      });

      if (!haveError) {
        var params = {
          user: _page_utils__WEBPACK_IMPORTED_MODULE_10__["PageUtils"].getDefaultRespondant(),
          branch: _page_utils__WEBPACK_IMPORTED_MODULE_10__["PageUtils"].getBranchData(answers),
          content: answers,
          form: _this.formData.id
        };
        _core__WEBPACK_IMPORTED_MODULE_9__["Http"].post("responses", params).then(function (data) {
          var _this$container$datas = _this.container.dataset,
              formslug = _this$container$datas.formslug,
              bankslug = _this$container$datas.bankslug;
          location.replace("".concat(location.origin, "/thankyou?bank=").concat(bankslug, "&form=").concat(formslug));
        })["catch"](function (err) {
          alert("Sorry we are experiencing some issues handling your response");
        });
      } else {
        _this.eventHandler.toggleSubmitErrorUI(haveError);
      }
    });

    this.startFormFilling = false;
    this.container = _container;
    this.fetchForm().then(function (form) {
      _this.questions = _page_utils__WEBPACK_IMPORTED_MODULE_10__["PageUtils"].getQuestions(form.elements);
      _this.eventHandler = new _page_event_handlers__WEBPACK_IMPORTED_MODULE_6__["EventHandler"](_container, _this.questions, form);
      _this.formData = form;

      _this.presentView();
    });
  }
  /**
   * fetch form created by the bank either locally or from the server
   */


  _createClass(ResponseCanvas, [{
    key: "fetchForm",
    value: function fetchForm() {
      // Todo ***********************************************************************
      // need to investigate with there is an error when data is not on local storage
      var formslug = this.container.dataset.formslug;
      var localFormData = _core__WEBPACK_IMPORTED_MODULE_9__["DataManager"].findForm(formslug);

      if (localFormData) {
        return Promise.resolve(localFormData);
      }

      return this.fetchRemotely();
    }
    /**
     * Go to back end service to fetch form data as it is not available locally
     */

  }, {
    key: "fetchRemotely",
    value: function fetchRemotely() {
      var data = this.container.dataset;
      var bankslug = data.bankslug,
          formslug = data.formslug;
      var parent = Object(_utils__WEBPACK_IMPORTED_MODULE_5__["capitalizeFirstLatter"])(data.formtypeparent);
      var formType = Object(_utils__WEBPACK_IMPORTED_MODULE_5__["capitalizeFirstLatter"])(data.formtype);
      var url = "forms/".concat(bankslug, "/").concat(parent, "/").concat(formType, "/").concat(formslug);
      return _core__WEBPACK_IMPORTED_MODULE_9__["Http"].get(url)["catch"](function (err) {
        alert("Sorry we are experiencing issues fetching content from our servers");
      });
    }
    /**
     * Ansamble various ui element and add them to the document at once
     */

  }, {
    key: "presentView",
    value: function presentView() {
      var nodeString = "";
      var canvasReady = false;

      if (!this.startFormFilling && _page_utils__WEBPACK_IMPORTED_MODULE_10__["PageUtils"].containsIntro(this.formData.elements)) {
        nodeString += this.buildIntroUI();
      } else {
        nodeString += this.buildCanvasUI();
        canvasReady = true;
      }

      var questionIDs = _page_utils__WEBPACK_IMPORTED_MODULE_10__["PageUtils"].pickIds(this.questions);
      var node = Object(_utils__WEBPACK_IMPORTED_MODULE_5__["parseHtml"])(nodeString);
      this.addEventListeners(node, questionIDs);
      this.container.replaceChild(node, this.container.firstChild);

      if (canvasReady) {
        this.initiateCanvas();
      }
    }
    /**
     * Build the intro section ui if its part of a form
     */

  }, {
    key: "buildIntroUI",
    value: function buildIntroUI() {
      var name = this.formData.name;
      var introData = _page_utils__WEBPACK_IMPORTED_MODULE_10__["PageUtils"].getIntroData(this.formData.elements);
      return _templates_response_canvas_intro_hbs__WEBPACK_IMPORTED_MODULE_4___default()({
        name: name,
        components: introData.children
      });
    }
    /**
     * Translate the various template that makes up the canvas area
     * into a a single string that can be converted into a node
     */

  }, {
    key: "buildCanvasUI",
    value: function buildCanvasUI() {
      var total = this.questions.filter(function (question) {
        return question.type !== "statement";
      }).length;
      var sectionData = _page_utils__WEBPACK_IMPORTED_MODULE_10__["PageUtils"].getFirstSection(this.formData.elements);
      var submitSectionString = _templates_response_canvas_submit_hbs__WEBPACK_IMPORTED_MODULE_0___default()();
      var totalAnswered = this.eventHandler.getAnswers().length;
      var footerString = _templates_response_canvas_footer_hbs__WEBPACK_IMPORTED_MODULE_2___default()({
        total: total,
        totalAnswered: totalAnswered
      });
      var headerString = _templates_response_canvas_header_hbs__WEBPACK_IMPORTED_MODULE_1___default()(sectionData);
      var canvasString = _templates_response_canvas_canvas_hbs__WEBPACK_IMPORTED_MODULE_3___default()({
        questions: "".concat(this.buildQuestionsUI()),
        submitSection: submitSectionString
      });
      return "".concat(headerString).concat(canvasString).concat(footerString);
    }
    /**
     * Build a suitable ui for each question data to allow users
     * Answer the question being asked
     */

  }, {
    key: "buildQuestionsUI",
    value: function buildQuestionsUI() {
      return this.questions.map(function (question) {
        return _page_ui_builder__WEBPACK_IMPORTED_MODULE_7__["PageUIBuilder"].buildUIFor(question);
      }).join("");
    }
    /**
     * Add event listeners to items of interest in the document
     * @param {documentFragment} node Document Object containing input elements for user
     * to interact with
     * @param {array} elementIds array of string containing unique id for input
     * interaction item
     */

  }, {
    key: "addEventListeners",
    value: function addEventListeners(node) {
      var _this2 = this;

      var elementIds = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
      this.addGetStartedListener(node);
      this.addNavigationListeners(node);
      elementIds.forEach(function (id) {
        _this2.addInputListener(node, id);

        _this2.addOptionListeners(node, id);

        _this2.addDragDropListeners(node, id);

        _this2.addMediaPermissionHandler(node, id);
      });
      this.addOnSubmitListener(node);
    }
    /**
     * Observe when user click get started button and show response canvas
     * @param {documentFragment} node Document Object containing input elements for user
     * to interact with
     */

  }, {
    key: "addGetStartedListener",
    value: function addGetStartedListener(node) {
      var getStartedBtn = node.querySelector("#getStarted");

      if (getStartedBtn) {
        getStartedBtn.addEventListener("click", this.handleGettingSetarted, false);
      }
    }
    /**
     * Register click event handler on navigation button
     *  @param {documentFragment} node Document Object containing input elements for user
     * to interact with
     */

  }, {
    key: "addNavigationListeners",
    value: function addNavigationListeners(node) {
      var _this3 = this;

      var upBtn = node.querySelector("button[id=\"navigateUP\"]");
      var downBtn = node.querySelector("button[id=\"navigateDown\"]");

      if (upBtn && downBtn) {
        upBtn.onclick = function () {
          var questionUI = _page_utils__WEBPACK_IMPORTED_MODULE_10__["PageUtils"].getCurrentQuestionIdFromUI();

          _this3.eventHandler.goToNextQuestion(questionUI.dataset.qId, "up");
        };

        downBtn.onclick = function () {
          var questionUI = _page_utils__WEBPACK_IMPORTED_MODULE_10__["PageUtils"].getCurrentQuestionIdFromUI();

          _this3.eventHandler.goToNextQuestion(questionUI.dataset.qId, "down");
        };
      }
    }
    /**
     * Register an event listener that monitors when a user start interacting with an input
     * interection eleemnt
     *  @param {documentFragment} node Document Object containing input elements for user
     * to interact with
     * @param {string} id  id of the interaction element whose input element is to be
     * observed
     */

  }, {
    key: "addInputListener",
    value: function addInputListener(node, id) {
      var _this4 = this;

      //this functions breaks single responsibility principle. fix it
      var btn = node.querySelector("button.nextButton[id=\"".concat(id, "\"]"));
      var input = node.querySelector(".input[data-q-id=\"".concat(id, "\"]"));

      if (input) {
        if (input.dataset.qType === "dropdown") {
          var dropDownIcon = node.querySelector("span[data-q-id=\"".concat(id, "\"]"));
          dropDownIcon.onclick = this.eventHandler.toggleDropDownList;
          input.addEventListener("input", this.eventHandler.expandDropDownList, false);
          input.oninput = this.eventHandler.filterDropdownOptions;
        } else if (input.type === "file") {
          var type = input.dataset.qType;
          var _id = input.dataset.qId;

          input.oninput = function (e) {
            _this4.eventHandler.handleStaticAssetUpload(e.target.files[0], type, _id);
          };
        } else {
          input.oninput = this.eventHandler.handleInput;
          input.onkeyup = this.eventHandler.processAnswer;
        }
      }

      if (btn) {
        if (btn.classList.contains("statement")) {
          btn.onclick = function (e) {
            return _this4.eventHandler.goToNextQuestion(e.target.id, "down");
          };
        } else {
          btn.onclick = this.eventHandler.processAnswer;
        }
      }
    }
    /**
     * Register event hanndler for button elements on media component UI
     *  @param {documentFragment} node Document Object containing input elements for user
     * @param {string} id  id of the question whose askPermissionBtn we are interested in
     */

  }, {
    key: "addMediaPermissionHandler",
    value: function addMediaPermissionHandler(node, id) {
      var permissionBtn = node.querySelector("button.askPermissionBtn[id=\"".concat(id, "\"]"));
      var uploadBtn = node.querySelector("button.uploadButton[id=\"".concat(id, "\"]"));

      if (permissionBtn) {
        permissionBtn.onclick = this.eventHandler.askMediaPermission;
      }

      if (uploadBtn) {
        uploadBtn.onclick = this.eventHandler.handleGeneratedAssetUpload;
      }
    }
    /**
     *  Register an event listener that monitors when a user clicks an option in a multi
     * option interaction element
     * @param {documentFragment} node Document Object containing input elements for user
     * to interact with
     * @param {string} id  id of the interaction element whose option the user clicked
     * on
     */

  }, {
    key: "addOptionListeners",
    value: function addOptionListeners(node, id) {
      var _this5 = this;

      var options = node.querySelectorAll("div[data-option-id=\"".concat(id, "\"]"));

      if (options.length) {
        options.forEach(function (option) {
          return option.onclick = _this5.eventHandler.handleOptionSelection;
        });
      }
    }
    /**
     * Register an click handler that processes users response
     *@param {documentFragment} node Document Object containing input elements for user
     * to interact with
     */

  }, {
    key: "addOnSubmitListener",
    value: function addOnSubmitListener(node) {
      var btn = node.querySelector("button[id=\"submitresponse\"]");
      if (btn) btn.onclick = this.processResponse;
    }
    /**
     * Register event to handle user drag and drop action
     */

  }, {
    key: "addDragDropListeners",
    value: function addDragDropListeners(node, id) {
      var _this6 = this;

      var dropArea = node.querySelector("div.dropArea[data-q-id=\"".concat(id, "\"]"));

      function preventDefault(e) {
        e.preventDefault();
        e.stopPropagation();
      }

      if (dropArea) {
        ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(function (eventName) {
          dropArea.addEventListener(eventName, preventDefault, false);
        });
        ['dragenter', 'dragover'].forEach(function (eventName) {
          dropArea.addEventListener(eventName, _this6.eventHandler.highlightDropArea, false);
        });
        ['dragleave', 'drop'].forEach(function (eventName) {
          dropArea.addEventListener(eventName, _this6.eventHandler.unhighlightDropArea, false);
        });
        dropArea.addEventListener('drop', this.eventHandler.handleFileDrop, false);
      }
    }
    /**
     * Observe when a user click get started and show and initialize the canvas
     */

  }, {
    key: "initiateCanvas",

    /**
     * set up intersection observers to control appearance of
     * elements entering the canvas area
     */
    value: function initiateCanvas() {
      var _this7 = this;

      var targets = document.querySelectorAll('[data-question="true"]');
      var canvasClass = "canvas";
      _core_response_canvas__WEBPACK_IMPORTED_MODULE_8__["Canvas"].setUp({
        targets: targets,
        canvasClass: canvasClass
      }).onTargetEnter(this.targetEnterCanvas).onTargetExit(this.targetExitCanvas);
      this.canvas = document.querySelector(".".concat(canvasClass)); // temporarily hut the unidentified issues causing intersection
      // observe to enter infinit loop

      this.canvas.scrollBy(0, 50);
      setTimeout(function () {
        _this7.canvas.scrollBy(0, -20); // focus on first question if it is an input element


        _this7.eventHandler.updateFocus(_this7.questions[0].id, true);
      }, 10);
    }
    /**
     * Make question element in the canvas ready to be interacted with
     * @param res passed down from intersection observer
     */

  }]);

  return ResponseCanvas;
}();

/***/ }),

/***/ "./src/public/js/response-canvas/validator.js":
/*!****************************************************!*\
  !*** ./src/public/js/response-canvas/validator.js ***!
  \****************************************************/
/*! exports provided: Validator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Validator", function() { return Validator; });
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils */ "./src/public/js/utils/index.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }


/**
 * object to hold error messages for rules that each value
 * Beign validated fail
 */

var validationError = {
  hasError: false,
  messages: []
};
/**
 * question types that require digit values
 */

var digitTypes = ["tel", "number", "account", "bvn", "mobile"];
/**
 * class with interfaces to validate values users pass to response canvas
 * interactive elements
 */

var Validator =
/*#__PURE__*/
function () {
  function Validator() {
    _classCallCheck(this, Validator);
  }

  _createClass(Validator, null, [{
    key: "validate",

    /**
     * 
     * @param {object} rules // object containing containing rules that value entered must meet
     * @param {string} questionType string of charactered by user
     * @param {string} answer string of charactered entered by user
     */
    value: function validate(rules, questionType, answer) {
      // empty validation error to remove hold over values
      validationError.hasError = false;
      validationError.messages = [];
      rules.forEach(function (rule) {
        var methodName = "validate".concat(Object(_utils__WEBPACK_IMPORTED_MODULE_0__["capitalizeFirstLatter"])(rule.name), "Rule");

        if (Validator.hasOwnProperty(methodName)) {
          Validator[methodName](rule, questionType, answer);
        }
      });
      Validator.validateType(questionType, answer);
      return validationError;
    }
    /**
     * validate that value meets the mininum value required for the value
     * @param {object} rule includes the amount of value that satisfies the minimum
     * characters for answer
     * @param {string} questionType string of charactered by user
     * @param {string} answer string of characters entered by user that needs to be validated
     */

  }, {
    key: "validateMinRule",
    value: function validateMinRule(rule, questionType, answer) {
      if (answer.length < rule.value) {
        buildErrorMessage(questionType, rule);
      }
    }
    /**
     * validate that value entered by users meet the maximum rule required
     * @param {object} rule includes the amount of value that satisfies the maximum
     * characters for answer
     * @param {string} questionType string of charactered by user
     * @param {string} answer  string of characters entered by user that needs to be validated
     */

  }, {
    key: "validateMaxRule",
    value: function validateMaxRule(rule, questionType, answer) {
      if (answer.length > rule.value) {
        buildErrorMessage(questionType, rule);
      }
    }
    /**
     * validate that users answer required questions
     * @param {object} rule includes the amount of answer that satisfies the maximum requirement
     * @param {string} questionType string of charactered by user
     * @param {string} answer string of characters entered by user that needs to be validated
     */

  }, {
    key: "validateRequiredRule",
    value: function validateRequiredRule(rule, questionType, answer) {
      if (!answer) {
        buildErrorMessage(questionType, rule);
      }
    }
    /**
     * validate that users answer is a valid email address
     * @param {object} rule includes the amount of answer that satisfies the maximum requirement
     * @param {string} questionType string of charactered by user
     * @param {string} answer string of characters entered by user that needs to be validated
     */

  }, {
    key: "validateEmailRule",
    value: function validateEmailRule(rule, questionType, answer) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      if (!re.test(String(answer).toLowerCase())) {
        buildErrorMessage(questionType, rule);
      }
    }
    /**
    * validate that users answer is of the right type
    * @param {string} questionType string of charactered by user
    * @param {string} anwer string of characters entered by user that needs to be validated
    */

  }, {
    key: "validateType",
    value: function validateType(questionType, anwer) {
      if (digitTypes.includes(questionType) && isNaN(anwer)) {
        buildErrorMessage(questionType, {
          name: "need-digits"
        });
      }
    }
  }]);

  return Validator;
}();
/**
 * Add suitable error message to be show users
 * @param {string} questionType the type of question user is answering
 * @param {object} rule // object containing containing rules that answer entered must meet
 */

var buildErrorMessage = function buildErrorMessage(questionType, rule) {
  var message = "";

  switch (rule.name) {
    case "need-digits":
      message = "You need to enter digits not letters";
      break;

    case "email":
      message = "You need to enter a valid email address";
      break;

    case "min":
      message = "You need to enter at least ".concat(rule.value, " ").concat(lettersOrDigits(questionType));
      break;

    case "max":
      message = "You can't enter more than ".concat(rule.value, " ").concat(lettersOrDigits(questionType));
      break;

    case "required":
      message = "This question is required";
      break;

    default:
      message = "";
  }

  validationError.messages.push(message);
  validationError.hasError = true;
};
/**
 * figure out whether to include digits or letters to error message
 * @param {string} questionType the type of question user is answering
 */


var lettersOrDigits = function lettersOrDigits(questionType) {
  if (digitTypes.includes(questionType)) {
    return "digits";
  }

  return "letters";
};

/***/ }),

/***/ "./src/public/js/templates/response-canvas/canvas.hbs":
/*!************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/canvas.hbs ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function";

  return "<section class=\"interectionSection\">\n  <div class=\"canvas\">\n    <main>\n      "
    + ((stack1 = ((helper = (helper = helpers.questions || (depth0 != null ? depth0.questions : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"questions","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n    </main>\n    "
    + ((stack1 = ((helper = (helper = helpers.submitSection || (depth0 != null ? depth0.submitSection : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"submitSection","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n  </div>\n</section>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/dropdown-options.hbs":
/*!**********************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/dropdown-options.hbs ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"dropdownOptionWrapper\" data-option-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-option-type=\"dropdown\">\n  <div class=\"optionContents\">\n    <div class=\"optionBox\">\n      <div class=\"dropDownOption\">\n        <div class=\"optionTextWrapper\">\n          <div class=\"optionTextBox\">\n            <div class=\"dropDownOptionText text\">"
    + alias4(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper)))
    + "</div>\n          </div>\n          <div class=\"iconWrapper inactivIconWrapper\">\n            <div class=\"optionIconBox\">\n              <span>\n                <img src=\"/img/picked-green.svg\" alt=\"picked\" />\n              </span>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/dropdown-question.hbs":
/*!***********************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/dropdown-question.hbs ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "              <div class=\"requiredQuestion\">*</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"elementWrapper dull elementWrapperDropdown\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-question=\"true\">\n  <section class=\"inactiveElement\">\n    <div class=\"elementParent\">\n      <div class=\"elementHouse\">\n        <div>\n          <div class=\"elementQuestionWrapper\">\n            <div class=\"elementQuestionContents\">\n              <div class=\"questionDecorationWrapper\">\n                <span class=\"questionDecoration\">\n                  <div class=\"questionPosition\" data-q-position="
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + ">\n                    "
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + "\n                  </div>\n                  <div class=\"questionIcon\">\n                    <span>\n                      <i class=\"fa fa-arrow-right\"></i>\n                    </span>\n                  </div>\n                </span>\n              </div>\n            </div>\n          </div>\n          <div class=\"questionTextWrapper\">\n            <span>\n              <span>"
    + alias4(((helper = (helper = helpers.question || (depth0 != null ? depth0.question : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"question","hash":{},"data":data}) : helper)))
    + "</span>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isRequired : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </span>\n          </div>\n          <p class=\"description\">"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</p>\n        </div>\n        <div>\n          <div class=\"answerWrapper\">\n            <div class=\"answerContent\" tabIndex=\"-1\">\n              <div class=\"answerBox\">\n                <div class=\"answerText\">\n                  <input class=\"answer input\" placeholder=\""
    + alias4(((helper = (helper = helpers.placeholder || (depth0 != null ? depth0.placeholder : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"placeholder","hash":{},"data":data}) : helper)))
    + "\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-q-type=\"dropdown\"\n                    id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" />\n                  <div class=\"answerDecorationWrapper\">\n                    <div class=\"answerDecoration\">\n                      <span data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n                        <i class=\"fa fa-angle-down\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"></i>\n                      </span>\n                    </div>\n                  </div>\n                </div>\n                <div class=\"inactiveWrapper dropdownOptions\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n                  <div class=\"optionsBox\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n                    "
    + ((stack1 = ((helper = (helper = helpers.optionsString || (depth0 != null ? depth0.optionsString : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"optionsString","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n          <div class=\"validationWrapper inactiveValidation\" data-validation-msg-for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n          </div>\n        </div>\n      </div>\n    </div>\n  </section>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/footer.hbs":
/*!************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/footer.hbs ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<section class=\"canvas__footerSection\">\n  <div class=\"canvas__footer\">\n    <div class=\"canvas__footerContent\">\n      <div class=\"canvas__footerProgress\">\n        <p class=\"progress__text\">\n          "
    + alias4(((helper = (helper = helpers.totalAnswered || (depth0 != null ? depth0.totalAnswered : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"totalAnswered","hash":{},"data":data}) : helper)))
    + " of "
    + alias4(((helper = (helper = helpers.total || (depth0 != null ? depth0.total : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total","hash":{},"data":data}) : helper)))
    + " Questions\n        </p>\n        <progress value=\""
    + alias4(((helper = (helper = helpers.totalAnswered || (depth0 != null ? depth0.totalAnswered : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"totalAnswered","hash":{},"data":data}) : helper)))
    + "\" max=\""
    + alias4(((helper = (helper = helpers.total || (depth0 != null ? depth0.total : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total","hash":{},"data":data}) : helper)))
    + "\"></progress>\n      </div>\n      <div class=\"canvas__footerNavigiation\">\n        <button class=\"canvas__btn canvas__btn--primary btn--navigation\" id=\"navigateUP\">\n          <i class=\"fa fa-angle-up\"></i>\n        </button>\n        <button class=\"canvas__btn canvas__btn--primary btn--navigation\" id=\"navigateDown\">\n          <i class=\"fa fa-angle-down\"></i>\n        </button>\n      </div>\n    </div>\n  </div>\n</section>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/header.hbs":
/*!************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/header.hbs ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<section class=\"canvas__headerSection\">\n  <div class=\"canvas__header\">\n    <h1 class=\"canvas__sectionTitle\">"
    + container.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"name","hash":{},"data":data}) : helper)))
    + "</h1>\n  </div>\n</section>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/intro.hbs":
/*!***********************************************************!*\
  !*** ./src/public/js/templates/response-canvas/intro.hbs ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var alias1=container.lambda, alias2=container.escapeExpression;

  return "    <div class=\"intro__requirement\">\n      <div class=\"intro__icon\">\n        <img class=\"intro__iconImage\" src=\"/img/"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + ".svg\" alt=\""
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + " Icon\" />\n      </div>\n      <div class=\"intro__requirementTextWrapper\">\n        <h3 class=\"intro__requirementText\">\n          "
    + alias2(alias1((depth0 != null ? depth0.description : depth0), depth0))
    + "\n        </h3>\n      </div>\n    </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"intro\">\n  <div class=\"intro__info\">\n    <h1 class=\"primary-heading\">"
    + container.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</h1>\n  </div>\n  <div class=\"intro__instruction\">\n    <span class=\"intro__instructionWarning\">\n      You need the following below to fill this form\n    </span>\n    <span class=\"intro__instructionIcon\">\n      <i class=\"far fa-hand-point-down\"></i>\n    </span>\n  </div>\n  <div class=\"intro__requirements\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.components : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "  </div>\n  <div>\n    <div class=\"intro__actionBtnWrapper\">\n      <button class=\"canvas__btn canvas__btn--primary canvas__btn--action\" id=\"getStarted\">\n        I am Ready\n      </button>\n    </div>\n    <span class=\"intro__actonInstruction\">\n      Press <strong>Enter</strong>\n    </span>\n  </div>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/long-question.hbs":
/*!*******************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/long-question.hbs ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "                  <div class=\"requiredQuestion\">*</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"elementWrapper dull\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-question=\"true\">\n  <section class=\"inactiveElement\">\n    <div class=\"elementParent\">\n      <div class=\"elementHouse\">\n        <div>\n          <div>\n            <div class=\"elementQuestionWrapper\">\n              <div class=\"elementQuestionContents\">\n                <div class=\"questionDecorationWrapper\">\n                  <span class=\"questionDecoration\">\n                    <div class=\"questionPosition\">\n                      "
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + "\n                    </div>\n                    <div class=\"questionIcon\">\n                      <span>\n                        <i class=\"fa fa-arrow-right\"></i>\n                      </span>\n                    </div>\n                  </span>\n                </div>\n                <div class=\"questionTextWrapper\">\n                  <label for=\"id=\" "
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"\"> "
    + alias4(((helper = (helper = helpers.question || (depth0 != null ? depth0.question : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"question","hash":{},"data":data}) : helper)))
    + "?</label>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isRequired : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                </div>\n                <p class=\"description\">"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</p>\n              </div>\n            </div>\n          </div>\n          <div>\n            <div class=\"answerWrapper\">\n              <textarea data-q-position=\""
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + "\" placeholder=\""
    + alias4(((helper = (helper = helpers.placeholder || (depth0 != null ? depth0.placeholder : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"placeholder","hash":{},"data":data}) : helper)))
    + "\" class=\"textAreaAnswer input\"\n                id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" required=\""
    + alias4(((helper = (helper = helpers.isRequired || (depth0 != null ? depth0.isRequired : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"isRequired","hash":{},"data":data}) : helper)))
    + "\"></textarea>\n            </div>\n            <p class=\"anwerHint\">\n              <strong>SHIFT</strong> + <strong>ENTER</strong> To make a new line\n            </p>\n            <div class=\"nextButtonWrapper inactiveNextButtonWrapper\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n              <button class=\"nextButton\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n                <span class=\"nextButtonText\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">OK</span>\n              </button>\n              <div class=\"buttonInstruction\">\n                Press <strong>Enter</strong>\n              </div>\n            </div>\n            <div class=\"validationWrapper inactiveValidation\" data-validation-msg-for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </section>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/media-permission.hbs":
/*!**********************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/media-permission.hbs ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "                  <div class=\"requiredQuestion\">*</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"elementWrapper dull elementWrapperMedia\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-question=\"true\">\n  <section class=\"inactiveElement\">\n    <div class=\"elementParent recording-canvas\">\n      <div class=\"elementHouse\">\n        <div>\n          <div>\n            <div class=\"elementQuestionWrapper\">\n              <div class=\"elementQuestionContents\">\n                <div class=\"questionDecorationWrapper\">\n                  <span class=\"questionDecoration\">\n                    <div class=\"questionPosition\">\n                      "
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + "\n                    </div>\n                    <div class=\"questionIcon\">\n                      <span>\n                        <i class=\"fa fa-arrow-right\"></i>\n                      </span>\n                    </div>\n                  </span>\n                </div>\n                <div class=\"questionTextWrapper\">\n                  <label for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"> "
    + alias4(((helper = (helper = helpers.question || (depth0 != null ? depth0.question : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"question","hash":{},"data":data}) : helper)))
    + "</label>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isRequired : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                </div>\n                <p class=\"description\">"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</p>\n              </div>\n            </div>\n          </div>\n          <div class=\"mediaRecorder\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n            <div class=\"answerWrapper\">\n              <button class=\"askPermissionBtn\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-qType=\""
    + alias4(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data}) : helper)))
    + "\">\n                <span class=\"nextButtonText\">"
    + alias4(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper)))
    + "</span>\n              </button>\n            </div>\n            <div class=\"nextButtonWrapper in-mobile\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n              <button class=\"uploadButton des-content\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-qType=\""
    + alias4(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data}) : helper)))
    + "\">\n                <span class=\"nextButtonText\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">Upload</span>\n              </button>\n              <button class=\"resetButton mob-content des-content\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-qType=\""
    + alias4(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data}) : helper)))
    + "\">\n                <span class=\"nextButtonText\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">Re-Take</span>\n              </button>\n            </div>\n            <div class=\"validationWrapper inactiveValidation\" data-validation-msg-for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </section>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/multi-choice-question.hbs":
/*!***************************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/multi-choice-question.hbs ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "              <div class=\"requiredQuestion\">*</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"elementWrapper dull\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-question=\"true\">\n  <section class=\"inactiveElement\">\n    <div class=\"elementParent\">\n      <div class=\"elementHouse\">\n        <div>\n          <div class=\"elementQuestionWrapper\">\n            <div class=\"elementQuestionContents\">\n              <div class=\"questionDecorationWrapper\">\n                <span class=\"questionDecoration\">\n                  <div class=\"questionPosition\" data-q-position="
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + ">\n                    "
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + "\n                  </div>\n                  <div class=\"questionIcon\">\n                    <span>\n                      <i class=\"fa fa-arrow-right\"></i>\n                    </span>\n                  </div>\n                </span>\n              </div>\n            </div>\n          </div>\n          <div class=\"questionTextWrapper\">\n            <span>\n              <span>"
    + alias4(((helper = (helper = helpers.question || (depth0 != null ? depth0.question : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"question","hash":{},"data":data}) : helper)))
    + "</span>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isRequired : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </span>\n          </div>\n          <p class=\"description\">"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</p>\n        </div>\n        <div>\n          <div class=\"answerWrapper\">\n            <div class=\"answerContent\">\n              <div>\n                "
    + ((stack1 = ((helper = (helper = helpers.optionsString || (depth0 != null ? depth0.optionsString : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"optionsString","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n              </div>\n            </div>\n          </div>\n          <div class=\"validationWrapper inactiveValidation\" data-validation-msg-for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n          </div>\n        </div>\n      </div>\n    </div>\n  </section>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/option.hbs":
/*!************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/option.hbs ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"optionRoot\" data-option-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n  <div class=\"optionWrapper\">\n    <div class=\"optionContents\">\n      <div class=\"option\">\n        <div class=\"optionContent\">\n          <div class=\"optionDecoration\" data-input=\"true\">\n            <div class=\"optionLabelBox\">\n              <div class=\"optionLabelDecoration\">\n                <span class=\"optionLabelIcon\">"
    + alias4(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data}) : helper)))
    + "</span>\n              </div>\n            </div>\n            <div class=\"optionTextBox\">\n              <div class=\"optionText text\">"
    + alias4(((helper = (helper = helpers.text || (depth0 != null ? depth0.text : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"text","hash":{},"data":data}) : helper)))
    + "</div>\n            </div>\n            <div class=\"iconWrapper inactivIconWrapper\">\n              <div class=\"optionIconBox\">\n                <span>\n                  <img src=\"/img/picked-green.svg\" alt=\"picked\" />\n                </span>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/picture.hbs":
/*!*************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/picture.hbs ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "                  <div class=\"requiredQuestion\">*</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"elementWrapper dull\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-question=\"true\">\n  <section class=\"inactiveElement\">\n    <div class=\"elementParent\">\n      <div class=\"elementHouse\">\n        <div>\n          <div>\n            <div class=\"elementQuestionWrapper\">\n              <div class=\"elementQuestionContents\">\n                <div class=\"questionDecorationWrapper\">\n                  <span class=\"questionDecoration\">\n                    <div class=\"questionPosition\">\n                      "
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + "\n                    </div>\n                    <div class=\"questionIcon\">\n                      <span>\n                        <i class=\"fa fa-arrow-right\"></i>\n                      </span>\n                    </div>\n                  </span>\n                </div>\n                <div class=\"questionTextWrapper\">\n                  <span>"
    + alias4(((helper = (helper = helpers.question || (depth0 != null ? depth0.question : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"question","hash":{},"data":data}) : helper)))
    + "</span>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isRequired : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                </div>\n                <p class=\"description\">"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</p>\n              </div>\n            </div>\n          </div>\n          <div>\n            <div class=\"answerWrapper\">\n              <div class=\"dropArea\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-q-type=\""
    + alias4(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data}) : helper)))
    + "\">\n                <div class=\"dropAreaContent\">\n                  <input type=\"file\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"logo input\" data-q-type=\""
    + alias4(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data}) : helper)))
    + "\" />\n                  <div class=\"uploadstatus\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n                    "
    + ((stack1 = ((helper = (helper = helpers.uploadIndicator || (depth0 != null ? depth0.uploadIndicator : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"uploadIndicator","hash":{},"data":data}) : helper))) != null ? stack1 : "")
    + "\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"validationWrapper inactiveValidation\" data-validation-msg-for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </section>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/record-video.hbs":
/*!******************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/record-video.hbs ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"video-recorder\">\n  <div id=\"clockdiv\">\n    <div>\n      <span class=\"hours\">0</span>\n    </div>\n    <div>\n      <span class=\"minutes\">0</span>\n    </div>\n    <div>\n      <span class=\"seconds\">00</span>\n    </div>\n  </div>\n  <div class=\"video__players\">\n    <video id=\"live\" class=\"video__player\" playsinline autoplay muted></video>\n    <video id=\"playback\" class=\"video__player des-content\" playsinline></video>\n  </div>\n  <div class=\"video__controls\">\n    <button id=\"record\" class=\"video__control\">Start Recording</button>\n  </div>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/simple-question.hbs":
/*!*********************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/simple-question.hbs ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "                  <div class=\"requiredQuestion\">*</div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"elementWrapper dull\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-question=\"true\">\n  <section class=\"inactiveElement\">\n    <div class=\"elementParent\">\n      <div class=\"elementHouse\">\n        <div>\n          <div>\n            <div class=\"elementQuestionWrapper\">\n              <div class=\"elementQuestionContents\">\n                <div class=\"questionDecorationWrapper\">\n                  <span class=\"questionDecoration\">\n                    <div class=\"questionPosition\">\n                      "
    + alias4(((helper = (helper = helpers.position || (depth0 != null ? depth0.position : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"position","hash":{},"data":data}) : helper)))
    + "\n                    </div>\n                    <div class=\"questionIcon\">\n                      <span>\n                        <i class=\"fa fa-arrow-right\"></i>\n                      </span>\n                    </div>\n                  </span>\n                </div>\n                <div class=\"questionTextWrapper\">\n                  <label for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"> "
    + alias4(((helper = (helper = helpers.question || (depth0 != null ? depth0.question : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"question","hash":{},"data":data}) : helper)))
    + "</label>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.isRequired : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                </div>\n                <p class=\"description\">"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</p>\n              </div>\n            </div>\n          </div>\n          <div>\n            <div class=\"answerWrapper\">\n              <input placeholder=\""
    + alias4(((helper = (helper = helpers.placeholder || (depth0 != null ? depth0.placeholder : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"placeholder","hash":{},"data":data}) : helper)))
    + "\" type=\""
    + alias4(((helper = (helper = helpers.type || (depth0 != null ? depth0.type : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type","hash":{},"data":data}) : helper)))
    + "\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"answer input\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"\n                required=\""
    + alias4(((helper = (helper = helpers.isRequired || (depth0 != null ? depth0.isRequired : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"isRequired","hash":{},"data":data}) : helper)))
    + "\" />\n            </div>\n            <div class=\"nextButtonWrapper inactiveNextButtonWrapper\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n              <button class=\"nextButton\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n                <span class=\"nextButtonText\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">OK</span>\n              </button>\n              <div class=\"buttonInstruction\">\n                Press <strong>Enter</strong>\n              </div>\n            </div>\n            <div class=\"validationWrapper inactiveValidation\" data-validation-msg-for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </section>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/statement.hbs":
/*!***************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/statement.hbs ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),depth0,{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    return "                      <li>"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</li>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"elementWrapper dull\" data-q-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-question=\"true\">\n  <section class=\"inactiveElement\">\n    <div class=\"activeElementParent\">\n      <div class=\"elementParent\">\n        <div class=\"elementHouse\">\n          <div>\n            <div>\n              <div class=\"elementQuestionWrapper\">\n                <div class=\"elementQuestionContents\">\n                  <div class=\"questionDecorationWrapper\">\n                    <span class=\"questionDecoration\">\n                      <div class=\"questionIcon\">\n                        <span>\n                          <i class=\"fa fa-tasks\"></i>\n                        </span>\n                      </div>\n                    </span>\n                  </div>\n                  <div class=\"questionTextWrapper\">\n                    <span>\n                      <strong>"
    + alias4(((helper = (helper = helpers.question || (depth0 != null ? depth0.question : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"question","hash":{},"data":data}) : helper)))
    + "</strong>\n                    </span>\n                    <ul>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.children : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                    </ul>\n                  </div>\n                </div>\n              </div>\n            </div>\n            <div class=\"childHouse\">\n              <div class=\"nextButtonWrapper\">\n                <button class=\"nextButton statement\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n                  <span class=\"nextButtonText\" id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\"></span>\n                </button>\n                <div class=\"buttonInstruction\">\n                  Press <strong>Enter</strong>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </section>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/submit.hbs":
/*!************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/submit.hbs ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<div class=\"elementWrapper dull\" data-q-id=\""
    + container.escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data}) : helper)))
    + "\" data-question=\"true\">\n  <section class=\"submitSection inactiveElement\">\n    <div class=\"submit\">\n      <div class=\"itemsWrapper\">\n        <div class=\"items\">\n          <div class=\"contentWrapper\">\n            <div class=\"contents\">\n              <button class=\"submitButton\" id=\"submitresponse\">\n                <div class=\"sc-gqjmRU jhOZlx\">Submit</div>\n              </button>\n              <div class=\"sc-chPdSV bmzmvX\" color=\"#4FB0AE\">\n                <div class=\"sc-kgoBCf kvyvll\">\n                  <div class=\"sc-gqjmRU mOlEV des-content\">press <strong>ENTER</strong></div>\n                </div>\n              </div>\n            </div>\n            <div class=\"validationWrapper inactiveValidation\" id=\"submit-error-msg\">\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </section>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/take-picture.hbs":
/*!******************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/take-picture.hbs ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"picture\">\n  <div class=\"picture__feed\">\n    <video class=\"picture__live\" id=\"livefeed\">Video stream</video>\n    <button class=\"picture__capture\" id=\"capture\">Take Photo</button>\n  </div>\n  <canvas class=\"picture__canvas\" id=\"canvas\"></canvas>\n  <div class=\"picture__output des-content\">\n    <img alt=\"photo from camera\" class=\"picture__photo\" id=\"photo\">\n  </div>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/upload-indicator.hbs":
/*!**********************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/upload-indicator.hbs ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper;

  return "  <div class=\"uploadingImgWrapper\">\n    <img src=\""
    + container.escapeExpression(((helper = (helper = helpers.imgUrl || (depth0 != null ? depth0.imgUrl : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"imgUrl","hash":{},"data":data}) : helper)))
    + "\" alt=\"upload status\" class=\"img\" />\n  </div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "  <p class=\"uploadInstruction\">Drag and drop</p>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.imgUrl : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "  <label for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"inputLabel\">\n    "
    + alias4(((helper = (helper = helpers.labelText || (depth0 != null ? depth0.labelText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelText","hash":{},"data":data}) : helper)))
    + "\n  </label>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/uploaded-indicator.hbs":
/*!************************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/uploaded-indicator.hbs ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper;

  return "  <div class=\"uploadedImgWrapper\">\n    <img src=\""
    + container.escapeExpression(((helper = (helper = helpers.imgUrl || (depth0 != null ? depth0.imgUrl : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"imgUrl","hash":{},"data":data}) : helper)))
    + "\" alt=\"upload status\" class=\"uploadedImg\" />\n  </div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "  <p class=\"uploadInstruction\">Drag and drop</p>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.imgUrl : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "  <label for=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"inputLabel\">\n    "
    + alias4(((helper = (helper = helpers.labelText || (depth0 != null ? depth0.labelText : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"labelText","hash":{},"data":data}) : helper)))
    + "\n  </label>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/templates/response-canvas/validation-msg.hbs":
/*!********************************************************************!*\
  !*** ./src/public/js/templates/response-canvas/validation-msg.hbs ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "    <p>\n      <i class=\"fa fa-arrow-right\"></i>\n      "
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "\n    </p>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"validation\">\n  <div class=\"validationText\">\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.messages : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "  </div>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/public/js/utils/helpers.js":
/*!****************************************!*\
  !*** ./src/public/js/utils/helpers.js ***!
  \****************************************/
/*! exports provided: capitalizeFirstLatter, chunkData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "capitalizeFirstLatter", function() { return capitalizeFirstLatter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "chunkData", function() { return chunkData; });
var capitalizeFirstLatter = function capitalizeFirstLatter(s) {
  return s[0].toUpperCase() + s.slice(1);
};
var chunkData = function chunkData(arr) {
  var size = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 3;
  var result = [];
  var currentChunk = [];
  arr.forEach(function (item) {
    if (currentChunk.length === size) {
      result.push(currentChunk);
      currentChunk = [];
    }

    currentChunk.push(item);
  });
  result.push(currentChunk);
  return result;
};

/***/ }),

/***/ "./src/public/js/utils/index.js":
/*!**************************************!*\
  !*** ./src/public/js/utils/index.js ***!
  \**************************************/
/*! exports provided: parseHtml, capitalizeFirstLatter, chunkData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _parse_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./parse-html */ "./src/public/js/utils/parse-html.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "parseHtml", function() { return _parse_html__WEBPACK_IMPORTED_MODULE_0__["parseHtml"]; });

/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./helpers */ "./src/public/js/utils/helpers.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "capitalizeFirstLatter", function() { return _helpers__WEBPACK_IMPORTED_MODULE_1__["capitalizeFirstLatter"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "chunkData", function() { return _helpers__WEBPACK_IMPORTED_MODULE_1__["chunkData"]; });




/***/ }),

/***/ "./src/public/js/utils/parse-html.js":
/*!*******************************************!*\
  !*** ./src/public/js/utils/parse-html.js ***!
  \*******************************************/
/*! exports provided: parseHtml */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseHtml", function() { return parseHtml; });
var contextRange = document.createRange();
contextRange.setStart(document.body, 0);
var parseHtml = function parseHtml(str) {
  return contextRange.createContextualFragment(str);
};

/***/ })

/******/ });
//# sourceMappingURL=response-canvas.js.map