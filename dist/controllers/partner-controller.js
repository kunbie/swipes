"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
exports.PartnerController = {
    get router() {
        const router = express_1.Router();
        router
            .get("/:bankSlug", this.each)
            .get("/:bankSlug/formgroup/:formTypeParent/:formType/forms", this.forms)
            .get("/:bankSlug/:formTypeParent/:formType/:form", this.fillForm);
        return router;
    },
    each(req, res) {
        const bankSlug = req.params.bankSlug;
        res.render("bankhome", {
            scripts: '<script src="/js/bankhome.js"></script>',
            layout: "banks",
            bodyClass: "bank-home",
            bankSlug
        });
    },
    forms(req, res) {
        const { bankSlug, formType } = req.params;
        res.render("forms", {
            scripts: '<script src="/js/form.js"></script>',
            bodyClass: "form-page",
            layout: "banks",
            formType,
            bankSlug
        });
    },
    fillForm(req, res) {
        const { bankSlug, formTypeParent, formType, form } = req.params;
        res.render("fillform", {
            scripts: '<script src="/js/response-canvas.js"></script>',
            layout: "response-canvas",
            formTypeParent,
            formType,
            bankSlug,
            form
        });
    }
};
//# sourceMappingURL=partner-controller.js.map