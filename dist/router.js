"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const controllers_1 = require("./controllers");
const middleware_1 = require("./middleware");
const express_1 = __importStar(require("express"));
const compression_1 = __importDefault(require("compression"));
const body_parser_1 = __importDefault(require("body-parser"));
const zlib_1 = __importDefault(require("zlib"));
const path_1 = __importDefault(require("path"));
const compressor = compression_1.default({
    flush: zlib_1.default.Z_PARTIAL_FLUSH
});
const staticOptions = {
    maxAge: 1
};
const AppRouter = express_1.Router();
AppRouter.use(body_parser_1.default.json())
    .use(body_parser_1.default.urlencoded({ extended: false }))
    .use(compressor)
    .use(middleware_1.validator)
    .use(express_1.default.static(path_1.default.join(__dirname, "public"), staticOptions))
    .use("/", controllers_1.PageController.router)
    .use("/", controllers_1.PartnerController.router)
    .use(middleware_1.errorHandler);
exports.default = AppRouter;
//# sourceMappingURL=router.js.map