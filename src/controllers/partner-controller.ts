import { Request, Response, Router } from "express";

export const PartnerController = {
  get router() {
    const router = Router();
    router
      .get("/:bankSlug", this.each)
      .get("/:bankSlug/formgroup/:formTypeParent/:formType/forms", this.forms)
      .get("/:bankSlug/:formTypeParent/:formType/:form", this.fillForm);

    return router;
  },

  each(req: Request, res: Response) {
    const bankSlug = req.params.bankSlug;
    res.render("bankhome", {
      scripts: '<script src="/js/bankhome.js"></script>',
      layout: "banks",
      bodyClass: "bank-home",
      bankSlug
    });
  },

  forms(req: Request, res: Response) {
    const { bankSlug, formType } = req.params;
    res.render("forms", {
      scripts: '<script src="/js/form.js"></script>',
      bodyClass: "form-page",
      layout: "banks",
      formType,
      bankSlug
    });
  },

  fillForm(req: Request, res: Response) {
    const { bankSlug, formTypeParent, formType, form } = req.params;
    res.render("fillform", {
      scripts: '<script src="/js/response-canvas.js"></script>',
      layout: "response-canvas",
      formTypeParent,
      formType,
      bankSlug,
      form
    });
  }
};
