export class DataManager {
  static storeBanks(collection) {
    localStorage.setItem("swyp-businesses", JSON.stringify(collection));
  }

  static getBanks() {
    return JSON.parse(localStorage.getItem("swyp-businesses"));
  }

  static findBank(slug) {
    const businesses = JSON.parse(localStorage.getItem("swyp-businesses"));
    return businesses.find(biz => biz.slug === slug);
  }

  static storeForms(collection) {
    localStorage.setItem("swyp-forms", JSON.stringify(collection));
  }

  static getForms() {
    return JSON.parse(localStorage.getItem("swyp-forms"));
  }

  static findForm(slug) {
    try {
      const forms = JSON.parse(localStorage.getItem("swyp-forms"));
      return forms.find(form => form.slug === slug);
    } catch (error) {
      return null;
    }
  }
}
