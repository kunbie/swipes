import "whatwg-fetch";

const baseUrl = "https://business-backend-service.herokuapp.com/api/v1/";
// const baseUrl = "http://localhost:4000/api/v1/";

export const Http = {
  get(url) {
    return fetch(`${baseUrl}${url}`).then(response =>
      processResponse(response)
    );
  },

  post(url, data) {
    return fetch(`${baseUrl}${url}`, {
      method: "POST",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify(data)
    }).then(response => processResponse(response));
  },

  put(url, data) {
    return fetch(`${baseUrl}${url}`, {
      method: "PUT",
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify(data)
    }).then(response => processResponse(response));
  },

  upLoadForm(url, formData) {
    return fetch(`${baseUrl}${url}`, {
      method: "POST",
      body: formData
    }).then(response => processResponse(response));
  }
};

const processResponse = response => {
  if (response.ok) {
    return response.json();
  }
  const error = new Error("NetworkError");
  error.detials = response.json();
  throw error;
};