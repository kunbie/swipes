import mobileViewTemplate from "../templates/bankhome-mobileview.hbs";
import desktopViewTemplate from "../templates/bankhome-desktopview.hbs";
import {Http, DataManager} from "../core";
import {parseHtml, chunkData} from "../utils";

export default class PartnerController {
  constructor(container) {
    this._container = container;
    // Since users can come to this page direct we need to fetch partner details
    // again from the backend service is the data is not already in local storage
    // and update local storage
    this.description = this._container.querySelector("#description");
    this.heading = this._container.querySelector("#heading");
    this.logoMbi = document.querySelector("#banklogo-mobi");
    this.logoDes = document.querySelector("#banklogo-des");
    this.info = DataManager.findBank(this.heading.dataset.slug);
    if(!this.info) {
      // Tell user we don't have form for the request partner
      return alert(`We don't have forms from ${this.heading.dataset.slug}`);
    }
    this.description.insertBefore(parseHtml(this.info.description), this.description.firstChild);
    this.heading.insertBefore(parseHtml(this.info.name), this.heading.firstChild);
    this._mobileView = this._container.querySelector(".mob-tab-content-section");
    this._desktopView = this._container.querySelector(".des-tab-content-section");
    this.logoMbi.src = this.info.logo;
    this.logoDes.src = this.info.logo;
    this._registerEvents();
    this._fetchFormTypes();
  }

  /**
   * Fetch all form types for a business
   */
  _fetchFormTypes() {
    Http.get("workspaces")
      .then(formTypes => {
        const {slug} = this.info;
        const individaulChunks = chunkData(formTypes.Individual);
        const corporateChunks = chunkData(formTypes.Individual);
        const desktopViewTemplateString = desktopViewTemplate(
          {individaulChunks, corporateChunks, slug}
        );
        const mobileViewTemplateString = mobileViewTemplate({formTypes, slug: this.info.slug});

        const mobileViewNodes = parseHtml(mobileViewTemplateString);
        const desktopViewNodes = parseHtml(desktopViewTemplateString);
        this._mobileView.insertBefore(mobileViewNodes, this._mobileView.firstChild);
        this._desktopView.insertBefore(desktopViewNodes, this._desktopView.firstChild);
      })
      .catch(err => {
        alert(
          "Sorry we are experiencing issues fetching content from our servers"
        );
      });
  }

  /**
   * Register event listeners on selected elements
   */
  _registerEvents() {
    const tabBtns = this._container.querySelectorAll('[data-tab-btn="true"]');
    Array.from(tabBtns).forEach(tbn => tbn.addEventListener("click", this.handleBtnClicks, false));
  }

  /**
   * Handle click events on tab heads
   */
  handleBtnClicks = e => {
    this._toggleActiveTabBtn(e.target);
    this._toggleActiveTabContent(e.target);
  }

  /**
   * Add and remove active css class from tab heads
   * @param {document node} el document node that was clicked
   */
  _toggleActiveTabBtn(el) {
    const elData = el.dataset;
    if(elData.device === "des") {
      const allActiveTabs = this._container.querySelectorAll(".des-tab-head__btn--active");
      Array.from(allActiveTabs).forEach(tab => tab.classList.remove("des-tab-head__btn--active"));
      el.classList.add("des-tab-head__btn--active")
    }
    else if(elData.device === "mob") {
      const allActiveTabs = this._container.querySelectorAll(".mob-tab-head--active");
      Array.from(allActiveTabs).forEach(tab => {
        tab.classList.remove("mob-tab-head--active")
      });
      el.parentNode.classList.add("mob-tab-head--active")
    }
  }

  /**
   * Change tab content to corespond to the clicked tab head
   * @param {document node} el document node that was clicked
   */
  _toggleActiveTabContent(el) {
    const elData = el.dataset;
    let individualTabContent;
    let corporateTabContent;
    if(elData.device === "des") {
      individualTabContent = this._container.querySelector(".des-individaul-tab-content");
      corporateTabContent = this._container.querySelector(".des-corporate-tab-content");
    }else if(elData.device === "mob") {
      individualTabContent = this._container.querySelector(".mob-individaul-tab-content");
      corporateTabContent = this._container.querySelector(".mob-corporate-tab-content");
    }
    if(elData.tabName === "corporate-tab") {
      individualTabContent.classList.add("hide");
      corporateTabContent.classList.add("visible");
    }else if(elData.tabName === "individual-tab") {
      corporateTabContent.classList.remove("visible");
      individualTabContent.classList.remove("hide");
    }
  }
}
