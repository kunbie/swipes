module.exports = validationRules => {
  const stringArray = validationRules.map(rule => {
    return `${rule.name}=${rule.value}`
  })
  return stringArray.join(" ");
}