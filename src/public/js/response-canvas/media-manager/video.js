import { constraints } from "./util";

export class VideoManager {
  constructor(componentUI, id) {
    this.uploadBtn       = document.querySelector(`.uploadButton[id="${id}"]`);
    this.recordedVideo   = componentUI.querySelector("video#playback");
    this.recordButton    = componentUI.querySelector("button#record");
    this.errorMsgElement = componentUI.querySelector("span#errorMsg");
    this.liveVideo       = componentUI.querySelector("video#live");
    this.secondsSpan     = componentUI.querySelector('.seconds');
    this.minutesSpan     = componentUI.querySelector('.minutes');
    this.hoursSpan       = componentUI.querySelector('.hours');
    this.mediaSource     = new MediaSource();
    this.componentUI     = componentUI;
    this.timeInterval    = null;
    this.mediaRecorder   = null;
    this.recordedBlob    = null;
    this.sourceBuffer    = null;
    this.stream          = null;
    this.clonestream     = null;
    this.init();
  }

  /**
   * Ask permission to user media and set up camera
   * start streaming media data from users devices
   */
  init() {
    this.mediaSource.addEventListener("sourceopen", this.handleMediaSourceOpen);
    this.recordButton.addEventListener("click", this.handleRecording);
    try {
      navigator.mediaDevices.getUserMedia(constraints).then(stream => {
          console.log(stream);
        this.stream                = stream;
        this.liveVideo.srcObject   = stream;
          this.clonestream = stream;
        this.recordButton.disabled = false;
      })
    } catch (ex) {
      console.log(`Exception happened requesting access to user camera ${ex}`);
    }
  }

  /**
   * process media data from user devices
   * @param { MediaStream } stream
   */
  handleMediaSourceOpen = ()  => {
    this.sourceBuffer = this.mediaSource.addSourceBuffer('video/webm; codecs="vp8"');
  }

  /**
   * response to user's intent to record video
   */
  handleRecording = () => {
    const text = this.recordButton.textContent;
    if(text === "Start Recording") {
      this.startRecording();
    }else if( text === "Record Again"){
      this.recordAgain();
    } else {
      this.stopRecording();
    }
  }

  /**
   * play back recorded video
   */
  playRecordedVideo = () =>{
    const buffer = new Blob(this.recordedBlob, {type: "video/webm"});
    this.recordedVideo.src       = null;
    this.recordedVideo.srcObject = null;
    this.recordedVideo.src       = window.URL.createObjectURL(buffer);
    this.recordedVideo.controls  = true;
    this.recordedVideo.play();
  }

  /**
   * prepare environment to record another video
   */
  recordAgain() {
      this.mediaSource     = new MediaSource();
//    this.recordedVideo.classList.add("des-content");
//    this.recordedVideo.src = null;
//    this.recordedVideo.srcObject = null;
      try {
      navigator.mediaDevices.getUserMedia(constraints).then(stream => {
          console.log(stream);
        this.stream                = stream;
        this.liveVideo.srcObject   = stream;
          this.clonestream = stream;
        this.recordButton.disabled = false;
           this.liveVideo.classList.remove("des-content");
    this.uploadBtn.classList.add("des-content");

    this.secondsSpan.innerHTML = "00";
    this.minutesSpan.innerHTML = "0";
    this.hoursSpan.innerHTML   = "0";

    this.startRecording();
      })
    } catch (ex) {
      console.log(`Exception happened requesting access to user camera ${ex}`);
    }
  }

  /**
   * Save the data coming from users media devices camera/mic
   */
  startRecording() {
      
    this.recordedBlob = [];
    let options = {mimeType: "video/webm;codec=ßvp9"};
    if(!MediaRecorder.isTypeSupported(options.mimeType)) 
    {
      options = { mimeType: "video/webm;codec=vp8"};
      if(!MediaRecorder.isTypeSupported(options.mimeType)) 
      {
        options = { mimeType: "video/webm"};
      }
    }
    
    try {
      this.mediaRecorder = new MediaRecorder(this.stream, options);
        console.log(this.mediaRecorder, this.stream);
      this.recordButton.textContent = "Stop Recording";
      this.mediaRecorder.start(10) // collect 10ms of data;
        this.mediaRecorder.ondataavailable = this.handleDataAvailable;
      this.startCountDown();
    } catch (ex) {
      console.log(`Exception while recording video ${JSON.stringify(ex)}`);
    }
  }

  /**
   * Save raw data from user's devices to memory
   */
  handleDataAvailable = event => {
      console.log(event)
    if(event.data && event.data.size > 0) {
      this.recordedBlob.push(event.data);
    }
  }

  /**
   * stop collecting media data from user's device
   */
  stopRecording() {
    this.mediaRecorder.stop();
    this.recordButton.textContent = "Record Again";
    this.recordedVideo.classList.remove("des-content");
    this.uploadBtn.classList.remove("des-content");
    this.liveVideo.classList.add("des-content");
      
    this.playRecordedVideo();
    this.clearTimer();
      
    var videotrack = this.stream.getVideoTracks()[0];  // if only one media track
    var audiotrack = this.stream.getAudioTracks()[0];  // if only one media track

      videotrack.stop();
      audiotrack.stop();
      
    
  }

  /**
   * display elapsed time 
   */
  startCountDown() {
    const startTime   = new Date();
    this.timeInterval = setInterval(() => {
      const time  = new Date() - startTime;
      const seconds = Math.floor((time/1000) % 60);
      const munites = Math.floor((time/1000/60) % 60);
      const hours = Math.floor((time/(1000*60*60)) % 24);
      const days = Math.floor(time/(1000*60*60*24));
  
      this.secondsSpan.innerHTML = ('0' + seconds).slice(-2);
      this.minutesSpan.innerHTML = munites;
      this.hoursSpan.innerHTML   = hours;
      
    }, 1000);
  }

  clearTimer() {
    clearInterval(this.timeInterval);
  }

  /**
   * return asset that was recorded
   */
  getAsset() {
    if(this.recordedBlob.length > 0)
      return new Blob(this.recordedBlob, {type: "video/webm"});
    return null;
  }
}
