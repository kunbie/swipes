import submitSectionTemplate from "../templates/response-canvas/submit.hbs";
import headerTemplate from "../templates/response-canvas/header.hbs";
import footerTemplate from "../templates/response-canvas/footer.hbs";
import canvasTemplate from "../templates/response-canvas/canvas.hbs";
import introTemplate from "../templates/response-canvas/intro.hbs";
import { parseHtml, capitalizeFirstLatter } from "../utils";
import {EventHandler} from "./page-event-handlers";
import { PageUIBuilder } from "./page-ui-builder";
import { Canvas } from "../core/response-canvas";
import { Http, DataManager } from "../core";
import {PageUtils} from "./page-utils";

/**
 * Manages the response page where user are given oppurtunity to provide data
 * to banks by interacting with various UI elements
 */
export class ResponseCanvas {
  constructor(container) {
    this.startFormFilling = false;
    this.container = container;

    this.fetchForm().then(form => {
      this.questions = PageUtils.getQuestions(form.elements);
      this.eventHandler = new EventHandler(container, this.questions, form);
      this.formData = form;
      this.presentView();
    });
  }

  /**
   * fetch form created by the bank either locally or from the server
   */
  fetchForm() {
    // Todo ***********************************************************************
    // need to investigate with there is an error when data is not on local storage
    const {formslug} = this.container.dataset;
    const localFormData = DataManager.findForm(formslug);
    if(localFormData) {
      return Promise.resolve(localFormData);
    }
    return this.fetchRemotely();
  }

  /**
   * Go to back end service to fetch form data as it is not available locally
   */
  fetchRemotely()  {
    const data = this.container.dataset;
    const {bankslug, formslug} = data;
    const parent = capitalizeFirstLatter(data.formtypeparent);
    const formType = capitalizeFirstLatter(data.formtype);
    const url = `forms/${bankslug}/${parent}/${formType}/${formslug}`;
    return Http.get(url)
    .catch(err => {
      alert(
        "Sorry we are experiencing issues fetching content from our servers"
      );
    });
  }

  /**
   * Ansamble various ui element and add them to the document at once
   */
  presentView() {
    let nodeString = "";
    let canvasReady = false;
    if(!this.startFormFilling && PageUtils.containsIntro(this.formData.elements)) {
      nodeString += this.buildIntroUI();
    }else {
      nodeString += this.buildCanvasUI();
      canvasReady = true;
    }
    const questionIDs = PageUtils.pickIds(this.questions);

    const node = parseHtml(nodeString);
    this.addEventListeners(node, questionIDs);
    this.container.replaceChild(node, this.container.firstChild);
    if(canvasReady) {
      this.initiateCanvas();
    }
  }

  /**
   * Build the intro section ui if its part of a form
   */
  buildIntroUI() {
    const name = this.formData.name;
    const introData = PageUtils.getIntroData(this.formData.elements);
    return introTemplate({name, components: introData.children});
  }

  /**
   * Translate the various template that makes up the canvas area
   * into a a single string that can be converted into a node
   */
  buildCanvasUI() {
    const total = this.questions
    .filter(question => question.type !== "statement").length;
    const sectionData = PageUtils.getFirstSection(this.formData.elements);
    const submitSectionString = submitSectionTemplate();
    const totalAnswered = this.eventHandler.getAnswers().length;
    const footerString = footerTemplate({total, totalAnswered});
    const headerString = headerTemplate(sectionData);

    const canvasString = canvasTemplate({
      questions: `${this.buildQuestionsUI()}`,
      submitSection: submitSectionString
    })
    return `${headerString}${canvasString}${footerString}`
  }

  /**
   * Build a suitable ui for each question data to allow users
   * Answer the question being asked
   */
  buildQuestionsUI() {
    return this.questions.map(question => PageUIBuilder.buildUIFor(question)).join("");
  }

  /**
   * Add event listeners to items of interest in the document
   * @param {documentFragment} node Document Object containing input elements for user
   * to interact with
   * @param {array} elementIds array of string containing unique id for input
   * interaction item
   */
  addEventListeners(node, elementIds=[]) {
    this.addGetStartedListener(node);
    this.addNavigationListeners(node);
    elementIds.forEach(id => {
      this.addInputListener(node, id);
      this.addOptionListeners(node, id);
      this.addDragDropListeners(node, id);
      this.addMediaPermissionHandler(node, id);
    })
    this.addOnSubmitListener(node);
  }

  /**
   * Observe when user click get started button and show response canvas
   * @param {documentFragment} node Document Object containing input elements for user
   * to interact with
   */
  addGetStartedListener(node) {
    const getStartedBtn = node.querySelector("#getStarted");
    if(getStartedBtn) {
      getStartedBtn.addEventListener("click", this.handleGettingSetarted, false);
    }
  }

  /**
   * Register click event handler on navigation button
   *  @param {documentFragment} node Document Object containing input elements for user
   * to interact with
   */
  addNavigationListeners(node) {
    const upBtn = node.querySelector(`button[id="navigateUP"]`);
    const downBtn = node.querySelector(`button[id="navigateDown"]`);

    if(upBtn && downBtn) {
      upBtn.onclick = () => {
        const questionUI = PageUtils.getCurrentQuestionIdFromUI();
        this.eventHandler.goToNextQuestion(questionUI.dataset.qId, "up");
      };
      downBtn.onclick = () => {
        const questionUI = PageUtils.getCurrentQuestionIdFromUI();
        this.eventHandler.goToNextQuestion(questionUI.dataset.qId, "down")
      };
    }
  }

  /**
   * Register an event listener that monitors when a user start interacting with an input
   * interection eleemnt
   *  @param {documentFragment} node Document Object containing input elements for user
   * to interact with
   * @param {string} id  id of the interaction element whose input element is to be
   * observed
   */
  addInputListener(node, id) {
    //this functions breaks single responsibility principle. fix it
    const btn = node.querySelector(`button.nextButton[id="${id}"]`);
    let input = node.querySelector(`.input[data-q-id="${id}"]`);
    if(input) {
      if(input.dataset.qType === "dropdown") {
        const dropDownIcon = node.querySelector(`span[data-q-id="${id}"]`);
        dropDownIcon.onclick = this.eventHandler.toggleDropDownList;
        input.addEventListener("input", this.eventHandler.expandDropDownList, false);
        input.oninput = this.eventHandler.filterDropdownOptions;
      }else if(input.type === "file"){
        const type = input.dataset.qType;
        const id = input.dataset.qId;
        input.oninput = (e) => {
          this.eventHandler.handleStaticAssetUpload(e.target.files[0], type, id);
        }
      }else {
        input.oninput = this.eventHandler.handleInput;
        input.onkeyup = this.eventHandler.processAnswer;
      }
    }
    if(btn){
      if(btn.classList.contains("statement")) {
        btn.onclick = (e) => this.eventHandler.goToNextQuestion(e.target.id, "down");
      }else {
        btn.onclick = this.eventHandler.processAnswer;
      }
    }
  }
  /**
   * Register event hanndler for button elements on media component UI
   *  @param {documentFragment} node Document Object containing input elements for user
   * @param {string} id  id of the question whose askPermissionBtn we are interested in
   */
  addMediaPermissionHandler(node, id){
    const permissionBtn = node.querySelector(`button.askPermissionBtn[id="${id}"]`);
    const uploadBtn = node.querySelector(`button.uploadButton[id="${id}"]`);
    if(permissionBtn) {
      permissionBtn.onclick = this.eventHandler.askMediaPermission;
    }
    if(uploadBtn) {
      uploadBtn.onclick = this.eventHandler.handleGeneratedAssetUpload;
    }
  }

  /**
   *  Register an event listener that monitors when a user clicks an option in a multi
   * option interaction element
   * @param {documentFragment} node Document Object containing input elements for user
   * to interact with
   * @param {string} id  id of the interaction element whose option the user clicked
   * on
   */
  addOptionListeners(node, id) {
    const options = node.querySelectorAll(`div[data-option-id="${id}"]`);
    if(options.length) {
      options.forEach(option => option.onclick = this.eventHandler.handleOptionSelection);
    }
  }

  /**
   * Register an click handler that processes users response
   *@param {documentFragment} node Document Object containing input elements for user
   * to interact with
   */
  addOnSubmitListener(node) {
    const btn = node.querySelector(`button[id="submitresponse"]`);
    if(btn) btn.onclick = this.processResponse;
  }

  /**
   * Register event to handle user drag and drop action
   */
  addDragDropListeners(node, id) {
    const dropArea = node.querySelector(`div.dropArea[data-q-id="${id}"]`);
    function preventDefault(e) {
      e.preventDefault();
      e.stopPropagation();
    }
    if(dropArea) {
      ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, preventDefault, false);
      });
      ['dragenter', 'dragover'].forEach(eventName => {
        dropArea.addEventListener(eventName, this.eventHandler.highlightDropArea, false);
      });

      ['dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, this.eventHandler.unhighlightDropArea, false);
      });
      dropArea.addEventListener('drop', this.eventHandler.handleFileDrop, false);
    }
  }

  /**
   * Observe when a user click get started and show and initialize the canvas
   */
  handleGettingSetarted = () => {
    this.startFormFilling = true;
    this.presentView();
  };

  /**
   * set up intersection observers to control appearance of
   * elements entering the canvas area
   */
  initiateCanvas() {
    const targets = document.querySelectorAll('[data-question="true"]');
    const canvasClass = "canvas";
    Canvas
      .setUp({targets, canvasClass})
      .onTargetEnter(this.targetEnterCanvas)
      .onTargetExit(this.targetExitCanvas);

    this.canvas = document.querySelector(`.${canvasClass}`);

    // temporarily hut the unidentified issues causing intersection
    // observe to enter infinit loop
    this.canvas.scrollBy(0, 50);
    setTimeout(() => {
      this.canvas.scrollBy(0, -20);
      // focus on first question if it is an input element
      this.eventHandler.updateFocus(this.questions[0].id, true);
    }, 10);
  }

  /**
   * Make question element in the canvas ready to be interacted with
   * @param res passed down from intersection observer
   */
  targetEnterCanvas = res => {
    const {element} = res
    const container = element.querySelector("section");
    element.classList.remove("dull");
    container.classList.remove("inactiveElement");
    container.classList.add("activeElement");
  }

   /**
   * Make question element exiting the canvas unable to recieve interaction
   * @param res passed down from intersection observer
   */
  targetExitCanvas = res => {
    const {element} = res
    const container = element.querySelector("section");
    element.classList.add("dull");
    container.classList.remove("activeElement");
    container.classList.add("inactiveElement");
  }

  /**
  * collect all users response, validate and send it to the backend server for processing
  */
  processResponse = ()  => {
    let haveError = false;
    const answers = this.eventHandler.getAnswers();
    this.questions.forEach(question => {
      const response = answers.find(r => r.questionId === question.id);
      const answer = response ? response.answer : "";
      const notValid = this.eventHandler.validateAnswer(answer, question.id);
      if(notValid)  haveError = true;
    });
    if(!haveError) {

      const params = {
        user: PageUtils.getDefaultRespondant(),
        branch: PageUtils.getBranchData(answers),
        content: answers,
        form: this.formData.id
      };
      Http.post("responses", params)
      .then(data => {
        const {formslug, bankslug} = this.container.dataset;
        location.replace(`${location.origin}/thankyou?bank=${bankslug}&form=${formslug}`);
      })
      .catch(err => {
        alert("Sorry we are experiencing some issues handling your response");
      });
    }else {
      this.eventHandler.toggleSubmitErrorUI(haveError);
    }
  }
}
