import dropdownOptionTemplate from "../templates/response-canvas/dropdown-options.hbs";
import optionTemplate from "../templates/response-canvas/option.hbs";

/**
 * used to for building label for question with options
 */
const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

export class PageUtils {
  /**
   * Extract out form element that need users interaction
   * @param {array} formInputs form elements
   */
  static getQuestions(formInputs){
    return formInputs.filter(
      element => element.type !== "section" && element.type !== "introduction"
    ).map((question, index) => {
      question.qPosition = index + 1;
      return question;
    })
  }

  /**
   * check if a form has intro section
   * @param {*} formInputs form elements
   */
  static containsIntro(formInputs) {
    const intro = formInputs.find(el => el.type === "introduction");
    if(intro)return true;
    return false;
  }

  /**
   * pick the ids of all form elements that needs user's interaction
   * @param {*} questions 
   */
  static pickIds(questions) {
    return questions.map(el => el.id);
  }

  /**
 * get the intro section of a form
 * @param {*} formInputs 
 */
  static getIntroData(formInputs){
    return formInputs.find(el => el.type === "introduction");
  }

  /**
   * extract the first section data to display on the page header
   */
  static getFirstSection(formQuestions){
    return formQuestions.slice(0, 3).find(question => question.type === "section");
  }

  /**
   * Extract the next section data to display on the page header
   *  @param {*} formInputs 
   */
  static getNextSection(formQuestions, currentQuestionId){
    const currentQuestionIndex = formQuestions.findIndex(
      question => question.id === currentQuestionId
    );
    const nextQuestion = formQuestions[currentQuestionIndex + 1];
    return (nextQuestion && nextQuestion.type) === "section" ? nextQuestion : null;
  }

/**
  * return the parent node for document fragment acting as a question option
  * that recieved a click event
  * @param {DocumentNode} node 
  */
  static getRootNode(node) {
    while (node.dataset.optionId === undefined && node.nodeName !== "SECTION") {
      node = node.parentElement;
    }
    return node;
  }

  /**
   * transform array of text to arry of option object
   * @param {array} array array with text to be transformed into option object
   * @param {string} filterText value by which array element would be filted
   */
  static buildOptionFromArray(array, filterText = null){
    if (filterText) {
      array = array
      .filter(item => item.toLowerCase().indexOf(filterText.toLowerCase()) !== -1);
    }
    return array.map((el, index) => {
      return { label: alphabet[index], text: el };
    });
  };

  /**
   * Transform the data in a branch list to option objects
   * @param {Array} branches list of a bank branch
   * @param {string} filterText value by which array element would be filted
   */
  static buildOptionsFromBranches(branches, filterText = null) {
    branches = branches.map(branch => `${branch.name} | ${branch.address}`);
    if(filterText) {
      branches = branches
      .filter(branch => branch.toLowerCase().indexOf(filterText.toLowerCase()) !== -1);
    }
    return branches.map((text, index) => ({label: alphabet[index], text}));
  }

  /**
    * Build UI for each multi choice question's option
    * @param {array} options option object
    * @param {string} questionId id of question
    * @param {boolean} questionType type of question e.g dropdown/card etc.
    */
  static buildOptionTemplate(options = [], questionId, questionType){
    if(questionType === "dropdown" || questionType === "branch") {
      return options
      .map(({text}) => dropdownOptionTemplate({text, id: questionId}))
      .join("");
    }
    return options
      .map(({label, text}) => optionTemplate({label, text, id: questionId}))
      .join("");
  }

  /**
   * Return the next data to be be displayed on the page header
   */
  static getNextSectionData(formQuestions, currentQuestionId){
    const currentQuestionIndex = formQuestions.findIndex(
      question => question.id === currentQuestionId
    );
    const nextQuestion = formQuestions[currentQuestionIndex + 1];
    return (nextQuestion && nextQuestion.type) === "section" ? nextQuestion : null;
  }

  /**
   * return the next question in the series
   * @param {array} formQuestions 
   * @param {string} currentQuestionId 
   */
  static getNextQuestion(formQuestions, currentQuestionId){
    const currentQuestionIndex = formQuestions.findIndex(
      question => question.id === currentQuestionId
    );
    return formQuestions[currentQuestionIndex + 1];
  }

  /**
   * get the distance between the top of the canvas to an element
   * @param {DocumentNode} elem 
   */
  static getDistanceToTop(elem) {
    let distance = 0;
    if (elem.offsetParent) {
      do {
        distance += elem.offsetTop;
        elem = elem.offsetParent;
      } while (elem);
    }
    return distance < 0 ? 0 : distance;
  }

  /**
   * get the branch a user want their answers sent to
   * @param {array} answers all the answers a user has given
   */
  static getBranchData(answers) {
    const branch = answers.find(answer => answer.questionType === "branch");
    return branch ? removeAddresspart(branch.answer) : "HQ";
  }

  /**
   * return the node of the current question a user is answering
   */
  static getCurrentQuestionUI() {
    return Array.from(document.querySelectorAll(".elementWrapper"))
    .find(node =>  !node.classList.contains("dull"))
  }

  /**
   * since we are not collect users data this is our default form respondant value for now
   */
  static getDefaultRespondant(){
    return{
      id: "5b51e1706fa9c80029476871",
      firstname: "ThankGod",
      lastname: "Ossaija",
      phone: "08136868448"
    };
  }
}

const removeAddresspart = branchAnswer => {
  const index = branchAnswer.indexOf(" ");
  return branchAnswer.substring(0, index);
}