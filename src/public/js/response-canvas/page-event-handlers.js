import uploadedIndicatorTemplate from "../templates/response-canvas/uploaded-indicator.hbs";
import uploadingIndicatorTemplate from "../templates/response-canvas/upload-indicator.hbs";
import validationTemplate from "../templates/response-canvas/validation-msg.hbs";
import recordVideo from "../templates/response-canvas/record-video.hbs";
import takePicture from "../templates/response-canvas/take-picture.hbs";
import headerTemplate from "../templates/response-canvas/header.hbs";
import footerTemplate from "../templates/response-canvas/footer.hbs";
import { PictureManager } from "./media-manager/picture";
import { VideoManager } from "./media-manager/video";
import { PageUtils } from "./page-utils";
import {Validator} from "./validator";
import { parseHtml } from "../utils";
import { Http } from "../core";

export class EventHandler {
  constructor(pageView, questions, formData) {
    this.questions       = questions;
    this.formData        = formData;
    this.pageView        = pageView;
    this.pictureManagers = new Map();
    this.videoManagers   = new Map();
    this.answers         = [];
  }

  /**
   * collect text entered into an input element by a user
   * @param {InputEvent} event the input event
   */
  handleInput = event => {
    const {id, value} = event.target;
    const nextButtonWrapper = this.pageView.querySelector(`div.nextButtonWrapper[id="${id}"]`);
    if(nextButtonWrapper) {
      if(value){
        nextButtonWrapper.classList.remove("inactiveNextButtonWrapper")
      }else {
        nextButtonWrapper.classList.add("inactiveNextButtonWrapper")
      }
    }
    this.addAnswer(id, value);
  }

  /**
   * Give a visual cue to user when they drag a object around an area where files
   * can be dropped for uploading
   * @param {DragEvent} event
   */
  highlightDropArea(event) {
    event.target.classList.add("highlightArea");
  }

  /**
   * Give a visual cue to user when they drag a object out of an area where files
   * can be dropped for uploading
   *@param {DragEvent} event
   */
  unhighlightDropArea(event) {
    event.target.classList.remove("highlightArea");
  }

  /**
   * Handle object drop event in a region that allow drag and drop
   * @param {DropEvent} e
   */
  handleFileDrop = e => {
    e.preventDefault();
    const data = e.target.dataset;
    this.handleStaticAssetUpload(e.dataTransfer.files[0], data.qType, dataqId);
  }


  /**
   * show option of a dropdown question type
   * @param {InputEvent} event
   */
  expandDropDownList = (event) => {
    const id = event.target.dataset.qId;
    const node = this.pageView.querySelector(`div.dropdownOptions[data-q-id="${id}"]`);
    if(node.classList.contains("inactiveWrapper")){
      node.classList.remove("inactiveWrapper");
      node.classList.add("activeWrapper");
    }
  }

  /**
   * filter dropdown option by the text entered by user
   */
  filterDropdownOptions = event => {
    const {id, value } = event.target;
    const question = this.questions.find(el => el.id === id);
    const {type, children} = question;
    let options = []
    if(type === "branch") {
      options = PageUtils.buildOptionsFromBranches(children, value);
    }else {
      options = PageUtils.buildOptionFromArray(children, value);
    }
    const template = PageUtils.buildOptionTemplate(options, id, type);
    const optionsNode = parseHtml(template);
    const optionContainer = this.pageView.querySelector(`div.optionsBox[data-q-id="${id}"]`);
    this.addOptionListeners(optionsNode, id);
    optionContainer.innerHTML = "";
    optionContainer.appendChild(optionsNode);
  }

  /**
   * show/hid options for a dropDown question type
   * @param {ClickEvent} event
   * @param {string} id id of the question whose options need toggling
   */
  toggleDropDownList = (event, id = null) => {
    if(!id) {
      id = event.target.dataset.qId;
    }
    const node = this.pageView.querySelector(`div.dropdownOptions[data-q-id="${id}"]`);
    if(node.classList.contains("inactiveWrapper")){
      node.classList.remove("inactiveWrapper");
      node.classList.add("activeWrapper");
    }else {
      node.classList.add("inactiveWrapper");
      node.classList.remove("activeWrapper");
    }
  }

  /**
   *  Register an event listener that monitors when a user clicks an option in a multi
   * option interaction element
   * @param {documentFragment} node Document Object containing input elements for user
   * to interact with
   * @param {string} id  id of the interaction element whose option the user clicked
   * on
   */
  addOptionListeners(node, id) {
    const options = node.querySelectorAll(`div[data-option-id="${id}"]`);
    if(options.length) {
      options.forEach(option => option.onclick = this.handleOptionSelection)
    }
  }


  /**
   * Handle user option selection, add check icon on the select option
   * @param {mouseEvent} event the clicked event
   */
  handleOptionSelection = event => {
    const node = PageUtils.getRootNode(event.target);
    const type = node.dataset.optionType;
    const id = node.dataset.optionId;
    if(id) {
      const allOptions = this.pageView.querySelectorAll(`div[data-option-id="${id}"]`);
      allOptions.forEach(option => {
        const checkIconBox = option.querySelector("div.iconWrapper");
        if(checkIconBox.classList.contains("activIconWrapper")){
          checkIconBox.classList.add("inactivIconWrapper");
          checkIconBox.classList.remove("activIconWrapper");
        }
      })
      const picked = node.querySelector("div.iconWrapper");
      const pickedTextBox  = node.querySelector("div.text");
      picked.classList.remove("inactivIconWrapper");
      picked.classList.add("activIconWrapper");

      this.addAnswer(id, pickedTextBox.textContent);
      this.toggleErrorUIFor(id, []);
      this.toggleSubmitErrorUI(false);
      this.updateHeader(id);
      this.updateFooter();
      if(type === "dropdown") {
        let input = this.pageView.querySelector(`input[data-q-id="${id}"]`);
        input.value = pickedTextBox.textContent;
        this.toggleDropDownList(null, id);
      }
      this.goToNextQuestion(id,"down");
      this.updateFocus(id);
    }
  }

  /**
   * ask user for access to the media devices on their system i.e mic/camera
   */
  askMediaPermission = (ev) => {
    const {id}                = ev.currentTarget;
    const {qtype}             = ev.currentTarget.dataset;
    const componentParent     = this.pageView.querySelector(`div.mediaRecorder[id="${id}"]`);
    const component           = componentParent.querySelector("div.answerWrapper");
    component.innerHTML       = "";
    if(qtype === "video") {
      const newNodeTree = parseHtml(recordVideo());
      const videoManager = new VideoManager(newNodeTree, id);
//        videoManager = recordVideo.stop();    
    component.appendChild(newNodeTree);
    this.videoManagers.set(id, videoManager);
    }else if(qtype === "picture") {
      const newNodeTree = parseHtml(takePicture());
      const pictureManager = new PictureManager(newNodeTree, id);
      component.appendChild(newNodeTree);
      this.pictureManagers.set(id, pictureManager);
    }
  }

  /**
   * add users answer to a question to the list of answers
   * @param {string} questionId
   * @param {string} answerText text entered by user or file url from s3 bucket
   */
  addAnswer(questionId, answerText) {
    const question = this.questions.find(el => el.id === questionId);
    const answer = {
      questionType: question.type,
      question: question.name,
      questionId: question.id,
      answer: answerText
    }
    const existingIndex = this.answers.findIndex(res => res.question === answer.question);
    if(existingIndex >= 0 ) {
      this.answers[existingIndex] = answer;
    }else {
      this.answers.push(answer);
    }
  }

  /**
   * Show/Hid UI with error messages for a question with wrong answer
   * @param {string} questionId id of question whose answer failed validation
   * @param {array} messages messages to display to user
   */
  toggleErrorUIFor(questionId, messages = []){
    const msgContainer = this.pageView.querySelector(`[data-validation-msg-for="${questionId}"]`);
    if(messages.length) {
      msgContainer.classList.remove("inactiveValidation");
      const msgString = validationTemplate({messages});
      const oldNode = msgContainer.firstChild;
      const msgNode = parseHtml(msgString);
      msgContainer.replaceChild(msgNode, oldNode);
    }else {
      if(!msgContainer.classList.contains("inactiveValidation")){
        msgContainer.classList.add("inactiveValidation");
      }
    }
  }

  /**
   * Show error message to user if any question's  answer does not pass all validation rules
   * @param {bool} haveError
   */
  toggleSubmitErrorUI(haveError) {
    const msgContainer = this.pageView.querySelector("#submit-error-msg");
    if(haveError) {
        msgContainer.classList.remove("inactiveValidation");
        const messages = [
//          "Scroll up to fix problems with some of your answers",
          "Scroll up to fix problems with some of your answers"
        ];
        const msgString = validationTemplate({messages});
        const oldNode = msgContainer.firstChild;
        const msgNode = parseHtml(msgString);

        msgContainer.replaceChild(msgNode, oldNode);
    }else {
      if(!msgContainer.classList.contains("inactiveValidation")){
        msgContainer.classList.add("inactiveValidation");
      }
    }
  }

  /**
   * update the section information on the canvas header
   * @param {number} currentQuestionId
   */
  updateHeader(currentQuestionId) {
    const sectionData = PageUtils.getNextSectionData(this.formData.elements, currentQuestionId);
    if(sectionData) {
      const headerString = headerTemplate(sectionData);
      const newHeaderNode = parseHtml(headerString);
      const oldHeaderNode = this.pageView.querySelector("section.canvas__headerSection");
      this.pageView.replaceChild(newHeaderNode, oldHeaderNode);
    }
  }

   /**
   * Replace old copy of footer node with a new copy that
   * reflect the current state of the application
   */
  updateFooter() {
    this.toggleFooterForMobile(false);
    const total = this.questions
      .filter(question => question.type !== "statement").length;
    const totalAnswered = this.answers.length;
    const footerString = footerTemplate({total, totalAnswered});
    const newFooterNode = parseHtml(footerString);
    this.addNavigationListeners(newFooterNode);
    const oldFooterNode = this.pageView.querySelector("section.canvas__footerSection");
    this.pageView.replaceChild(newFooterNode, oldFooterNode);
  }

  /**
   * Register click event handler on navigation button
   *  @param {documentFragment} node Document Object containing input elements for user
   * to interact with
   */
  addNavigationListeners(node) {
    const upBtn = node.querySelector(`button[id="navigateUP"]`);
    const downBtn = node.querySelector(`button[id="navigateDown"]`);
    if(upBtn && downBtn) {
      upBtn.onclick = () => {
        const questionUI = PageUtils.getCurrentQuestionUI();
        this.goToNextQuestion(questionUI.dataset.qId, "up");
      };
      downBtn.onclick = () => {
        const questionUI = PageUtils.getCurrentQuestionUI();
        this.goToNextQuestion(questionUI.dataset.qId, "down")
      };
    }
  }

   /**
   * Bring a new question into user's focus
   * @param {string} currentQuestionId id of the question user has just answered
   * @param direction direction to scroll to get to the next question
   */
  goToNextQuestion = (currentQuestionId, direction = "down") => {

    // Todo **************************************
    // logic should change if user is scrolling up

    const nextQuestion = PageUtils.getNextQuestion(this.questions, currentQuestionId);
    const canvas = this.pageView.querySelector(".canvas");
    const currentQuestionUI = this.pageView.querySelector(
      `.elementWrapper[data-q-id="${currentQuestionId}"]`
    );
    let distanceToScroll = 237;
    if(nextQuestion) {
      const nextQuestionUI = this.pageView.querySelector(
        `.elementWrapper[data-q-id="${nextQuestion.id}"]`
      );
      const distanceToCurrentQ = PageUtils.getDistanceToTop(currentQuestionUI);
      const distanceToNextQ = PageUtils.getDistanceToTop(nextQuestionUI);
      distanceToScroll = distanceToNextQ - distanceToCurrentQ;
    }
    if (direction === "down") {
      canvas.scrollBy(0, distanceToScroll);
    } else if (direction === "up") {
      canvas.scrollBy(0, -distanceToScroll);
    }
  }

  /**
   * Put focus on the next question for user to answer
   * @param {string} questionId id of the question a user just finished answering
   */
  updateFocus(questionId, firstQuestion = false) {
    const nextQuestion = PageUtils.getNextQuestion(this.questions, questionId);
    if(!nextQuestion && firstQuestion === false) return;
    let id = questionId;
    if(!firstQuestion){
      id = nextQuestion.id;
    }
    const input = this.pageView.querySelector(`.input[data-q-id="${id}"]`);
    if(input) {
      input.focus();
      this.toggleFooterForMobile(true);
    }
  }

  /**
   * handle uploading static assets like passport and signature
   */
  handleStaticAssetUpload =(file, type, id) => {
    console.log(file);
    const uploadIndicator = this.pageView.querySelector(`div.uploadstatus[data-q-id="${id}"]`);
    let templateOption = {imgUrl: "/img/uploading.svg", labelText: "Uploading...", id};
    let template = uploadingIndicatorTemplate(templateOption);
    uploadIndicator.innerHTML = "";
    uploadIndicator.appendChild(parseHtml(template));
    this.uploadFile(file, type, id).then(res => {
      templateOption = {imgUrl: res.assetUrl, labelText: "Change File", id};
      template = uploadedIndicatorTemplate(templateOption);
      uploadIndicator.innerHTML = "";
      uploadIndicator.appendChild(parseHtml(template));
      this.addAnswer(id, res.assetUrl);
      this.processAnswer(null, id, res.assetUrl);
    }).catch(err => {
      alert(`${type} upload faild please try again.`);
    })
  }

   /**
   * upload video/picture to aws
   */
  handleGeneratedAssetUpload = (ev) => {
    const span = ev.currentTarget.querySelector(".nextButtonText");
    span.innerHTML = "Uploading...";
    const {id}         = ev.currentTarget;
    const {qtype}      = ev.currentTarget.dataset;
    let assetManager   = null;
    let name           = null;
    let errorMessage   = null;

    if(qtype === "video") {
      assetManager = this.videoManagers.get(id);
      errorMessage = "You have not recorded any video yet."
      name = "video.webm";
    }else if (qtype === "picture") {
      assetManager = this.pictureManagers.get(id);
      name = "image.png";
      errorMessage = "You have not taken any picture yet."
    }
    if(assetManager) {
      const asset = assetManager.getAsset();
      if(asset) {
        this.uploadFile(asset, qtype, name).then(res => {
          span.innerHTML = "Upload";
          this.addAnswer(id, res.assetUrl);
          this.processAnswer(null, id, res.assetUrl);
        }).catch(err => {
          console.log(err);
          alert(`${qtype} upload faild please try again.`);
        });
      }else {
        alert(errorMessage);
      }
    }
  }

  /**
   * upload file to S3 bucket
   * @param {FileObject} file the file to be uploaded
   * @param {string} type the type of file to upload e.g signature/passport
   */
  uploadFile = (file, type, name = null) => {
    const {firstname, lastname} = PageUtils.getDefaultRespondant();
    type = String(`${type}s`).toLowerCase();
    const fullname = `${firstname}${lastname}`;
    const {bankslug} = this.pageView.dataset;
    const url = `upload/${type}/${bankslug}/${fullname}_${type}`;
    const formData = new FormData();
    if(name) {
      formData.append("asset", file, name);
    }else {
      formData.append("asset", file);
    }
    return Http.upLoadForm(url, formData)
  }

  /**
   * show/hid progress on the footer depending on the kind of question
   * a user is answering while using a mobile phone
   * @param {Boolean} show
   */
  toggleFooterForMobile(show = false) {
    const footer = this.pageView.querySelector("section.canvas__footerSection");
    if(show && !footer.classList.contains("des-content")) {
      footer.classList.add("des-content");
    }
    if(!show && footer.classList.contains("des-content")) {
      footer.classList.remove("des-content");
    }
  }



  /**
   * Observes when users click the ok button on each input based interaction elements
   * @param {MouseEvent} event
   * to signify that they have finished answering the question
   */
  processAnswer = (event, id, value) => {
    if(!id) {
      // process answer only when a user press enter while interacting with an input element
      let target = event.target;
      if(event.shiftKey && event.key === "Enter") return;
      if(event.key && event.key !== "Enter") return;
      if((event.key && event.key === "Enter")) {
        target = event.target;
      }
      // ensure that the event target is an input element
      if(target.nodeName === "SPAN" || target.nodeName === "BUTTON") {
        target = this.pageView.querySelector(`.input[data-q-id="${target.id}"]`)
      }
      value = target.value;
      id = target.id;
    }
    const hasError = this.validateAnswer(value, id);
    if(!hasError) {
      this.updateHeader(id);
      this.updateFooter();
      this.goToNextQuestion(id, "down", 340);
    }
    this.updateFocus(id);
  }

   /**
   * Validate user input against the rules specified for the question
   */
  validateAnswer = (answer, questionId) => {
    const question = this.questions.find(q => q.id === questionId);
    const {validationRules, type} = question;
    if(type === "statement")return;
    const result = Validator.validate(validationRules, type, answer);
    const { hasError, messages } = result;

    this.toggleErrorUIFor(questionId, messages, hasError)
    this.toggleSubmitErrorUI(hasError);
    return result.hasError;
  }



  /**
   * return all the answer given by a use4r
   */
  getAnswers() {
    return this.answers;
  }
}
